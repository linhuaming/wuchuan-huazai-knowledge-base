## 物理机使用U盘安装CentOS 8系统

[TOC]

### 一、制作Linux启动盘

##### 1、准备东西

-  1个大于8GU盘（最小1G以上，最好8G）

- 官方系统镜像（CentOS7-1810）

- 光盘刻录软件（UltraISO）



##### 2、利用UItraISO 制作U盘启动引导

- 打开UltraISO软件，选择 “文件” -> “打开” 选择linux系统镜像（iso文件）并 “打开”。

  ![](https://img2018.cnblogs.com/blog/1586336/201901/1586336-20190114103348752-1169421822.png)

  ![](https://img2018.cnblogs.com/blog/1586336/201901/1586336-20190114103356657-1888742085.png)

  

- 插入U盘，选择 “启动” -> “写入硬盘镜像”，硬盘驱动器选择做为系统盘的U盘，其他选项不需要改动

  ![](https://img2018.cnblogs.com/blog/1586336/201901/1586336-20190114103410118-1049883164.png)

  

- 点击 ”写入” ，等待刻录完成即可。

  ![](https://img2018.cnblogs.com/blog/1586336/201901/1586336-20190114103454750-1598916783.png)

  

### 二、使用U盘安装Centos 8

1、插入刚刚制作的系统U盘进入BIOS，更改U盘为第一启动项



2、进入centos安装界面，按tab键进入配置编辑（根据提示是按tab键）



3、修改加载配置

第一步：

```shell
 将配置 vmlinuz initrd=initrd.img inst.stage2=hd......quiet
 改为 vmlinuz initrd=initrd.img linux dd quiet。
 修改之后按回车键，系统会进入磁盘设备列表，查看U盘的设备名。
```

第二步：

```shell
记住设备名（sdb4），然后按ctrl + alt + del 重启，
进入安装界面，按tab编辑配置，
改为vmlinuz initrd=initrd.img inst.stage2=hd:/dev/sdb4 quiet
之后按回车键，会进入系统安装界面了
```



4、进入图形界面如果不出现报错就是成功了，可以正常安装操作系统了，后面的操作就和vm安装centos 一样了

![](https://img2018.cnblogs.com/blog/1586336/201901/1586336-20190114103853024-1843823227.png)



