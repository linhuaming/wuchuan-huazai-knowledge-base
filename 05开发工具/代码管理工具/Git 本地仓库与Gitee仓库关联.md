## Git 本地仓库与Gitee仓库关联

> Git安装过程不做详解，下载安装包后一直Next即可，安装完成后，选择或新建文件夹作为本地仓库，右键点击【Git Bash Here】，Git下载地址：[点击此处](https://git-scm.com/downloads) 



[TOC]

### 一、添加Git 配置

#### 1、添加Git账号和邮箱

	git config --global user.name "码云登录名"

```
git config --global user.email "码云邮箱"
```



#### 2、生成SSH公钥

```
命令：ssh-keygen -t rsa
```

输入命令后根据提示回车三次

<img src="../../img/2021063022390001.png" alt="img" style="zoom:80%;" />



#### 3、查看生成的SSH公钥

```
命令：cat ~/.ssh/id_rsa.pub
```

windows 系统，可以在当前用户下.ssh 目录下查看id_rsa.pub 文件，如下图所示

<img src="../../img/image-20210630224645556.png" alt="image-20210630224645556" style="zoom: 67%;" />



#### 4、添加SSH公钥

登录码云：点击个人头像→【设置】，找到并点击【SSH公钥】，将复制的公钥粘贴进来，标题可随意，点击【确定】

<img src="../../img/2021063022390002.png" alt="img" style="zoom: 50%;" />



### 二、关联远程仓库的两种方式

#### 1、克隆远程仓库

选择要克隆的仓库，并复制仓库地址：

![](../../img/image-20210630230009332.png)



克隆仓库项目：

命令： git clone 仓库地址，首次拉取代码会被询问，输入yes即可

<img src="../../img/2021063022390004.png" alt="img" style="zoom:80%;" />



拉取代码成功，本地仓库出现克隆的项目

<img src="../../img/image-20210630230217000.png" alt="image-20210630230217000" style="zoom:80%;" />





#### 2、初始化本地仓库	

在Gitee 直接创建仓库，进入仓库，点击克隆/下载，复制ssh的地址。

![image-20210630230848254](../../img/image-20210630230848254.png)



本地初始化一个仓库

命令：git init

![image-20210630231324144](../../img/image-20210630231324144.png)



本地仓库指向Gitee的远程仓库

```shell
命令： git remote add origin git@gitee.com:linhuaming/hello_world02.git
```

注意：

git remote add  origin（远程仓库名称）git@gitee.com:linhuaming/hello_world02.git（远程仓库地址）



#### 3、拉取和推送

拉取：

```java
命令： git fetch origin  // origin 为远程仓库名称
```

```java
命令： git merge origin/master  // origin 为远程仓库名称，master为本地分支
```

或者

```java
命令： git pull <远程主机名> <远程分支名>:<本地分支名>
例子： git pull origin master // origin 为远程仓库名称，master为本地分支
```



推送：

```java
命令： git push origin master1:master2  // origin 为远程仓库名称，master1为本地分支 master2为远程分支
```



注：如出现错误： failed to push some refs to 'git@gitee.com:******.git'

```java
命令： git pull --rebase origin master 
```



删除本地远程仓库链接

```java
命令： git remote rm origin // origin 为远程仓库名称
```

查看本地仓库

```java
git remote //查看本地链接的远程仓库名
git remote -v //查看本地链接的远程仓库名及地址
```



参考: 

https://www.cnblogs.com/dyd168/p/14319635.html

https://blog.csdn.net/yan072201/article/details/106429980/

https://blog.csdn.net/qq_39132095/article/details/106862151

