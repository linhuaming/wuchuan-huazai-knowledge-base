## IDEA中解决 git pull 冲突的方法（图文教程-通俗易懂）

[TOC]

### 一、事先准备

现在Giee 码云上面建立一个仓库（我这里事先建立好的hello_world01 远程仓库的）

![](https://img-blog.csdnimg.cn/aba060f7b0c542cdac2ff16013930c06.png)



在本地建立两个文件夹，用来模拟不同的用户（没办法啦，只有一台电脑）

![](https://img-blog.csdnimg.cn/8050058d96e840269ad9fa16a602cdc0.png)



然后在分别在测试用户A、测试用户B里面拉取远程hello_world01 远程仓库下来。如下图所示

![](https://img-blog.csdnimg.cn/5c69015aea164a68bc825c903f1d5ebd.png)



![](https://img-blog.csdnimg.cn/ea0b65e6f7f748648e378ccf2920991f.png)



### 二、先commit再pull

1、在测试用户A文件目录里面，修改README.md文件，然后commit , push 提交到gitee远程仓库。此时再远程仓库就可以看了，用户A的修改了。

![](https://img-blog.csdnimg.cn/d33865d589e54c959608782ec2b9ad87.png)

![](https://img-blog.csdnimg.cn/ddcd11aefcca4f3e8dc403b204f97285.png)

2、同样的，测试用户B也修改README.md文件，如下图。

 ![](https://img-blog.csdnimg.cn/d7113e231a0d47898863409eca2ab416.png)



3、然后在测试用户B中，先commit再pull，会提示冲突，点击Merge，解决冲突即可，最后再push到远程仓库。

![](https://img-blog.csdnimg.cn/82ef9cf86d7e43d58dae7ca221f13252.png)



4、再去远程仓库，就可以看到push提交的记录了。

![](https://img-blog.csdnimg.cn/80b6539ff42e4fed876d8a9b48b08af8.png)



### 三、先pull再commit

1、回到原来的事先准备，同样的在测试用户A，修改README.md文件，commit , push 提交到远程仓库。

2、测试用户B也修改README.md文件。

3、这次先不commit,我们先来pull，接着右下角会提示。（这是肯定的啦，因为我们代码冲突了嘛，拉取不下来。）

![](https://img-blog.csdnimg.cn/cd6337c204a04e23acf75b95fbef0037.png)



4、我们选择项目右键--Git--Repository--Stash Changes

![](https://img-blog.csdnimg.cn/667588a419e04ed78dacf388e31eb076.png)



在Message中随便取个名字，然后点击Create Stash。

![](https://img-blog.csdnimg.cn/0173335fc1dc49e1b5b4805eeb4d19a6.png)

>  以上几步的操作，其实就是把你在本地做的改变，通过stash先在git栈中保存起来。

> stash完后你会发现你本地进行的一些修改都已经不存在了，README文件也回到了之前的# hello_world01(而不是修改后的)。

>  这样我们就可以和远程仓库合并了，（我们已经把冲突先去掉了），git pull 顺利成功。

> pull完以后我们查看README.md发现此时的内容就是远程仓库的。



现在我们再把之前保存的更改取出来，选择项目右键--Git--Repository--UnStash Changes

![](https://img-blog.csdnimg.cn/adc41afbf69349bf9a5fb0af41c41760.png)



然后我们点Apply Stash，来到如下窗口，提示有代码冲突。最后再commit ，push 提交到远程仓库。

![](https://img-blog.csdnimg.cn/4f7ec0184532425cba79c9c8329d9d4e.png)



又是似曾相识的感觉，有没有！（所以我个人感觉其实如果你没有先commit就pull的话，你就要通过stash和unstash把改变先保存起来然后再取出来，这一步其实就相当于commit吧。









参考：

https://www.jb51.net/article/191640.htm