## Git 分支与标签使用

[TOC]

### 一、分支简介

​		几乎每一种版本控制系统都以某种形式支持分支。使用分支意味着你可以从开发主线上分离开来，然后在不影响主线的同时继续工作。

​		比如开发某个产品，在主分支进行推进开发的时候，突然想加一个额外复杂功能，如果说写一点提交一点，会影响主项目的开发，这时候，可以新建一个分支，在自己的分支里，开发新功能。开发一点，提交一点，不影响主项目；等分支功能开发测试完整，最终可以合并到主项目



### 二、分支使用

说明：dev就是具体的分支名称

**1、创建分支命令**

```shell
git branch dev
```

说明：当你以此方式在上次提交更新之后创建了新分支dev，如果后来又有更新提交， 然后又切换到了 **dev** 分支，Git 将还原你的工作目录到你创建分支时样子。



**2、切换分支命令**

```shell
git checkout dev
```



**3、合并分支命令**

```shell
git merge  dev
```

你可以多次合并到统一分支， 也可以选择在合并之后直接删除被并入的分支。



**4、列出分支命令**

```
git branch
```

说明：没有参数时，**git branch** 会列出你在本地的分支。

​			此例的意思就是，我们有一个叫做 **master** 的分支，master 前面带有*****号，代表是当前分支。

​			当你执行 **git init** 的时候，默认情况下 Git 就会为你创建 **master** 分支。



**5、删除分支命令**

```
git branch -d (branchname)
```



### 三、标签简介

​		如果你达到一个重要的阶段，并希望永远记住那个特别的提交快照，你可以使用 git tag 给它打上标签。

​		比如说，我们想为我们的 xxx 项目发布一个"1.0"版本。 我们可以用 git tag -a v1.0 命令给最新一次提交打上（HEAD）"v1.0"的标签。

​		-a 选项意为"创建一个带注解的标签"。 不用 -a 选项也可以执行的，但它不会记录这标签是啥时候打的，谁打的，也不会让你添加个标签的注解。 我推荐一直创建带注解的标签。



### 四、标签使用

**1、创建标签**

```shell
git tag -a v1.0 
```

说明：当你执行 git tag -a 命令时，Git 会打开你的编辑器，让你写一句标签注解，就像你给提交写注解一样。



**2、查看提交记录的标签**

```shell
git log --decorate
```

<img src="https://img-blog.csdnimg.cn/2668f4a04b79456c9132733bd1f34c03.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5ZC05bed5Y2O5LuU,size_20,color_FFFFFF,t_70,g_se,x_16#pic_center" style="zoom:50%;" />





**3、给某个提交记录追加标签**

```shell
git tag -a v0.9 85fc7e7
```

说明：85fc7e7 是提交记录编号。



**4、查看所有标签**

```shell
git tag
```



**5、指定标签信息**

```shell
git tag -a <tagname> -m "xxx标签"
```



**6、PGP签名标签**

```shell
git tag -s <tagname> -m "xxx标签"
```

