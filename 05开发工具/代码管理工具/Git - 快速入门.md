##  Git - 快速入门

[TOC]

#### 一、Git 配置

Git 提供了一个叫做 git config 的工具，专门用来配置或读取相应的工作环境变量。

这些环境变量，决定了 Git 在各个环节的具体工作方式和行为。这些变量可以存放在以下三个不同的地方：

- `/etc/gitconfig` 文件：系统中对所有用户都普遍适用的配置。若使用 `git config` 时用 `--system` 选项，读写的就是这个文件。
- `~/.gitconfig` 文件：用户目录下的配置文件只适用于该用户。若使用 `git config` 时用 `--global` 选项，读写的就是这个文件。
- 当前项目的 Git 目录中的配置文件（也就是工作目录中的 `.git/config` 文件）：这里的配置仅仅针对当前项目有效。每一个级别的配置都会覆盖上层的相同配置，所以 `.git/config` 里的配置会覆盖 `/etc/gitconfig` 中的同名变量。



**用户信息**

​	由于git是分布式管理工具，需要输入用户名和邮箱以作为标识。

​	`git config --global user.name "linhuaming"`

​	`git config --global user.email "1057449102@qq.com"` 



**文本编辑器**

​	设置Git默认使用的文本编辑器, 一般可能会是 Vi 或者 Vim。如果你有其他偏好，比如 Emacs 的话，

可以重新设置。

​	`git config --global core.editor emacs`



**分析工具**

​	还有一个比较常用的是，在解决合并冲突时使用哪种差异分析工具。比如要改用 vimdiff 的话。

​	`git config --global merge.tool vimdiff`



**查看配置信息**。

​	要检查已有的配置信息，可以使用 git config --list 命令。

​	`git config --list`



​	也可以直接查看指定的配置，可以使用 git config xxxx (这个就是指定的配置)

​	`git config user.name`



#### 二、基本用法

 **创建版本库**

​	版本库就是我们所说的“仓库”，英文名repository，你可以理解为一个目录，这个目录里面的所有文件都可以被Git管理，文件的修改，删除Git都能跟踪，以便任何时刻都可以追踪历史，或者在将来某个时刻还可以将文      件”还原”。

​	使用此命令创建版本库，`git init` ， 如下图所示。

<img src="../../img/image-20210616235108100.png" alt="image-20210616235108100" style="zoom:50%;center:left;" />



**添加到暂存区**

​	在git_demo02目录下创建一个文件，这里我创建的文件为demo.txt，内容为123456 ，

​	使用下列的命令 `git add `，将创建的文件添加到暂存区。

<img src="../../img/image-20210616235923679.png" alt="image-20210616235923679" style="zoom:50%;" />

**提交到仓库**

​	使用命令 `git commit -m`， 将暂存区文件提交到仓库（单引号内为注释）。

<img src="../../img/image-20210617000415366.png" alt="image-20210617000415366" style="zoom:50%;" />



**检查是否有未提交文件**

​	使用命令 `git status`，检查该版本库是否有文件未提交。

<img src="../../img/image-20210617000621080.png" alt="image-20210617000621080" style="zoom:50%;" />



**检查文件是否被修改**

​	先在demo.txt 修改内容后，使用命令 `git diff `, 查看文件修改了什么内容重修改状态。

<img src="../../img/image-20210617001019363.png" alt="image-20210617001019363" style="zoom:50%;" />



**查看历史变更记录**

​	使用命令 `git log`  ，查看历史修改记录，可以看得出来demo.txt 已经修改了两次，如下图所示。

<img src="../../img/image-20210617001820803.png" alt="image-20210617001820803" style="zoom:50%;" />



​	命令解析： 

​	git log：获得历史修改记录

​	git log --pretty=oneline：使记录只显示主要的内容，一行显示



**版本回退**

​	先用命令cat demo.txt 查看内容，再使用命令 `git reset --hard HEAD^` , 执行回退到上一个版本。

<img src="../../img/image-20210617003155162.png" alt="image-20210617003155162" style="zoom:50%;" />





​	可以看到内容已经回退到上一个版本，通过git reflog查看修改记录，发现最近的一次内容的修改已经看不到了，如果想回到最新的版本，可以通过如下命令进行回退， 如下图所示。

<img src="../../img/image-20210617003757120.png" alt="image-20210617003757120" style="zoom:50%;" />

​	命令解析：

​	cat：查看文件内容

​	git reset --hard HEAD^：回退到上一个版本

​	git reflog：获取历史版本号

​	git reset --hard 版本号：回退到该版本号对应的版本

​	**PS：**如果要回退到上上个版本，可以使用git reset --hard HEAD^^命令，但是这样稍显麻烦，如果回退到100个	版本之前，只需要执行这个命令即可：git reset --hard HEAD~100



参考：

https://www.cnblogs.com/imyalost/p/8762522.html

https://www.runoob.com/git/git-install-setup.html

https://www.cnblogs.com/ximiaomiao/p/7140456.html

https://segmentfault.com/a/1190000003743788

