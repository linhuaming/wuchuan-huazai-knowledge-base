## Git 本地仓库与Github仓库关联-TODO

[TOC]



#### 一、配置信息

**1、设置账号和邮箱**

​	git config --global user.name "github登录名"
​	git config --global user.email "github邮箱"

![img](../../img/20210123234626726.png)



**2、生成ssh公钥**

Git和github之间是通过SSH加密的，因此需要执行下面的操作：

使用命令： `ssh-keygen -t rsa –C “youselfemail@email.com” ` ，输入命令后根据提示回车三次即可。

![img](../../img/20210618000001.png)



**3、查看生成的SSH公钥：**
命令：cat ~/.ssh/id_rsa.pub

![img](../../img/983980-20180410124625292-2004831619.png)





#### 二、创建github 账号

由于本地仓库和远程的github仓库需要关联，首先需要登录github，注册一个账号，然后建立一个仓库。

注册链接：https://github.com/join?source=header-home

成功注册后，登录github，首页如下：

![img](../../img/983980-20180410124625292-2004831618.png)







参考：https://www.cnblogs.com/imyalost/p/8777684.html





