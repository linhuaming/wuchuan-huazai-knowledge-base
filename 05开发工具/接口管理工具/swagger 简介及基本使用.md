## swagger 简介

[toc]

### 一、什么是swagger

​		Swagger 是一个主要用来在线生成文档的插件，这里主要用来动态生成api接口供前后端进行交互，如果不生成的话就需要写静态文档来交互，那样不仅很慢而且不容易修改，那Swagger就可以解决这个问题。

- 号称世界上最流行的API框架
- Restful Api 文档在线自动生成器 => **API 文档 与API 定义同步更新**
- 直接运行，在线测试API
- 支持多种语言 （如：Java，PHP等）
- 官网：[https://swagger.io/](https://links.jianshu.com/go?to=https%3A%2F%2Fswagger.io%2F)



### 二、swagger的使用

#### 1、pom.xml 引入依赖

```xml
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>2.4.0</version>
</dependency>

<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>2.4.0</version>
</dependency>
```



#### 2、创建swagger 配置类

```java
/**
 * swagger配置类
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

}

```



#### 3、创建一个DemoController

```java

/**
 * 演示管理
 */
@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoController {

    @RequestMapping("/demo1")
    public String demo1() throws Exception {
        log.info("demo1()");
        return "success";
    }

}
```



#### 4、启动项目

访问地址：http://127.0.0.1:8088/swagger-ui.html#/ ，可以看到Swagger 的页面：

![img](https://upload-images.jianshu.io/upload_images/20337942-d21e4812e8ce6b51.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)



### 三、swagger 配置

#### 1、配置swagger 扫描的接口

刚刚启动后，页面的内容都是默认的，在实际使用中需要我们进行配置。

**配置swagger扫描的接口**

```java
@Bean
public Docket docket() {
   return new Docket(DocumentationType.SWAGGER_2)
      .apiInfo(apiInfo())
      .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
     .apis(RequestHandlerSelectors.basePackage("com.linhuaming.springboot.mybatisplus.demo.controller"))
      .build();
}
```



**RequestHandlerSelectors 用法**

```java
any() // 扫描所有，项目中的所有接口都会被扫描到
none() // 不扫描接口
// 通过方法上的注解扫描，如withMethodAnnotation(GetMapping.class)只扫描get请求
withMethodAnnotation(final Class<? extends Annotation> annotation)
// 通过类上的注解扫描，如.withClassAnnotation(Controller.class)只扫描有controller注解的类中的接口
withClassAnnotation(final Class<? extends Annotation> annotation)
basePackage(final String basePackage) // 根据包路径扫描接口
```



**除此之外，我们还可以通过`.paths`配置接口扫描过滤**

```java
@Bean
public Docket docket() {
   return new Docket(DocumentationType.SWAGGER_2)
      .apiInfo(apiInfo())
      .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
.apis(RequestHandlerSelectors.basePackage("com.linhuaming.springboot.mybatisplus.demo.controller"))
       // 配置如何通过path过滤,即这里只扫描请求以/demo开头的接口
      .paths(PathSelectors.ant("/demo/**"))
      .build();
}
```



**PathSelectors 也是可以多种配置的**

```dart
any() // 任何请求都扫描
none() // 任何请求都不扫描
regex(final String pathRegex) // 通过正则表达式控制
ant(final String antPattern) // 通过ant()控制
```



#### 2、配置swagger 的开关

通过enable()方法配置是否启用swagger，如果是false，swagger将不能在浏览器中访问了

```java
@Bean
public Docket docket() {
   return new Docket(DocumentationType.SWAGGER_2)
      .apiInfo(apiInfo())
      .enable(false) //配置是否启用Swagger，如果是false，在浏览器将无法访问
      .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
.apis(RequestHandlerSelectors.basePackage("com.linhuaming.springboot.mybatisplus.demo.controller"))
       // 配置如何通过path过滤,即这里只扫描请求以/njit开头的接口
      .paths(PathSelectors.ant("/demo/**"))
      .build();
}
```



动态配置当项目处于test、dev环境时显示swagger，否则不显示，首先在application.properties 中配置激活的配合环境，然后再做如下配置：

```java
@Bean
public Docket docket(Environment environment) {
   // 设置要显示swagger的环境
   Profiles of = Profiles.of("dev", "test");
   // 判断当前是否处于该环境
   // 通过 enable() 接收此参数判断是否要显示
   boolean flag = environment.acceptsProfiles(of);
   
   return new Docket(DocumentationType.SWAGGER_2)
      .apiInfo(apiInfo())
      .enable(flag) //配置是否启用Swagger，如果是false，在浏览器将无法访问
      .select()// 通过.select()方法，去配置扫描接口,RequestHandlerSelectors配置如何扫描接口
.apis(RequestHandlerSelectors.basePackage("com.linhuaming.springboot.mybatisplus.demo.controller"))
       // 配置如何通过path过滤,即这里只扫描请求以/njit开头的接口
      //.paths(PathSelectors.ant("/demo/**"))
      .build();
}
```



#### 3、配置API分组

​		很简单只要配置groupName()即可，如果想要配置多个分组，name就配多个Docket实例，注意实例的名称不能重复。

```java
@Bean
public Docket docket(Environment environment) {
   return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo())
      .groupName("hello") // 配置分组
       // 省略配置....
}
```

```java
@Bean
public Docket docket1(){
   return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).groupName("group1");
}
@Bean
public Docket docket2(){
   return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).groupName("group2");
}
@Bean
public Docket docket3(){
   return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).groupName("group3");
}
```



#### 4、实体配置

​		新建一个实体类可以在上面使用注解

```java
// @ApiModel为类添加注释
// @ApiModelProperty为类属性添加注释
@ApiModel("用户实体")
public class User {
   @ApiModelProperty("用户名")
   public String username;
   @ApiModelProperty("密码")
   public String password;
}
```



#### 5、常用注解

| **Swagger注解**                                        | **简单说明**                                         |
| ------------------------------------------------------ | ---------------------------------------------------- |
| @Api(tags = "xxx模块说明")                             | 作用在模块类上                                       |
| @ApiOperation("xxx接口说明")                           | 作用在接口方法上                                     |
| @ApiModel("xxxPOJO说明")                               | 作用在模型类上：如VO、BO、DTO、POJO                  |
| @ApiModelProperty(value = "xxx属性说明",hidden = true) | 作用在类方法和属性上，hidden设置为true可以隐藏该属性 |
| @ApiParam("xxx参数说明")                               | 作用在参数、方法和字段上，类似@ApiModelProperty      |
| @ApiImplicitParam("xxx参数说明")                       | 作用在参数、方法和字段上，类似@ApiModelProperty      |


