## 简单搭建一个vue项目(基于vue-cli2.0)-完整步骤

[TOC]

### 1、安装nodejs环境

- 下载地址为：https://nodejs.org/en/
- 检查是否安装成功：如果输出版本号，说明我们安装node环境成功

<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509004306817.png" alt="image-20210509004306817" style="zoom:50%;" />



- 由于npm 镜像在国外，网速比较慢，下载依赖的时候可能会很慢，我们可以使用淘宝的镜像：http://npm.taobao.org/

- 输入：`npm install -g cnpm –registry=https://registry.npm.taobao.org`

  即可安装npm镜像，以后再用到npm的地方直接用cnpm来代替就好了。
  
  
### 2、搭建vue项目

- 全局安装vue-cli，输入 `　npm install --global vue-cli` ， 如下图所示

<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509010054543.png" alt="image-20210509010054543" style="zoom:50%;" />



- 创建一个基于 webpack 模板的新项目: `vue init webpack hello-world01`

<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509011055913.png" alt="image-20210509011055913" style="zoom: 50%;" />

​	说明：

​	Project name	项目名称

​	Project description	项目描述

​	Author	作者名称

​	Vue build 	打包方式，回车即可；

​	Install vue-router 	是否要安装 vue-router，项目中肯定要使用到 所以Y 回车；

​	Use ESLint to lint your code	是否需要 js 语法检测 目前我们不需要 所以 n 回车；

​	Set up unit tests	是否安装 单元测试工具 目前我们不需要 所以 n 回车；

​	Setup e2e tests with Nightwatch	是否需要 端到端测试工具 目前我们不需要 所以 n 回车；

​	Should we run `npm install` ... 初始化项目是否使用npm加载依赖



​	显示下面内容 ，即初始化项目成功。

​	<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509011651973.png" alt="image-20210509011651973" style="zoom: 50%;" />



- 进入项目：cd hello-world01，安装依赖 npm install

<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509011904453.png" alt="image-20210509011904453" style="zoom:67%;" />

​	

​	安装成功后，项目文件夹中会多出一个目录：　node_modules

​	<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509012034866.png" alt="image-20210509012034866" style="zoom:50%;" />



- npm run dev，启动项目

  显示如下内容，则启动成功

  <img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509013500231.png" alt="image-20210509013500231" style="zoom:67%;" />

  

### 3、vue项目目录说明

​	项目各目录如下：

<img src="C:\Users\linhuaming\AppData\Roaming\Typora\typora-user-images\image-20210509014023439.png" alt="image-20210509014023439" style="zoom: 50%;" />

​		**1、build：构建脚本目录**

　　　　1）build.js  ==> 生产环境构建脚本；``

　　　　2）check-versions.js  ==> 检查npm，node.js版本；

　　　　3）utils.js  ==> 构建相关工具方法；

　　　　4）vue-loader.conf.js  ==> 配置了css加载器以及编译css之后自动添加前缀；

　　　　5）webpack.base.conf.js  ==> webpack基本配置；

　　　　6）webpack.dev.conf.js  ==> webpack开发环境配置；

　　　　7）webpack.prod.conf.js  ==> webpack生产环境配置；

　　**2、config：项目配置**

　　　　1）dev.env.js  ==> 开发环境变量；

　　　　2）index.js  ==> 项目配置文件；

　　　　3）prod.env.js  ==> 生产环境变量；

　　**3、node_modules：npm 加载的项目依赖模块**

　　**4、src：这里是我们要开发的目录，基本上要做的事情都在这个目录里。里面包含了几个目录及文件：**

　　　　1）assets：资源目录，放置一些图片或者公共js、公共css。这里的资源会被webpack构建；

　　　　2）components：组件目录，我们写的组件就放在这个目录里面；

　　　　3）router：前端路由，我们需要配置的路由路径写在index.js里面；

　　　　4）App.vue：根组件；

　　　　5）main.js：入口js文件；

　　**5、static：静态资源目录，如图片、字体等。不会被webpack构建**

　　**6、index.html：首页入口文件，可以添加一些 meta 信息等**

　　**7、package.json：npm包配置文件，定义了项目的npm脚本，依赖包等信息**

　　**8、README.md：项目的说明文档，markdown 格式**

　　**9、.xxxx文件：这些是一些配置文件，包括语法配置，git配置等**



参考：https://www.cnblogs.com/hellman/p/10985377.html