## mysql数据库MVCC-介绍通俗易懂

[TOC]

### 一、介绍

​	MVCC 的英文全称是 Multiversion Concurrency Control ，中文意思是多版本并发控制技术。原理是，通过数据行的多个版本管理来实现数据库的并发控制，简单来说就是保存数据的历史版本。可以通过比较版本号决定数据是否显示出来。读取数据的时候不需要加锁可以保证事务的隔离效果。



参考：

https://zhuanlan.zhihu.com/p/428066667