## MySQL索引底层原理-通俗理解-简单介绍

[TOC]

### 一、为什么使用索引

### 二、如何理解索引

### 三、了解下数据结构 - 树

### 四、分析索引使用的数据结构



参考：

这篇 MySQL 索引和 B+Tree 讲得太通俗易懂

https://zhuanlan.zhihu.com/p/293128007



Mysql索引：图文并茂，深入探究索引的原理和使用

https://zhuanlan.zhihu.com/p/335734145



深入理解 Mysql 索引底层原理  

https://zhuanlan.zhihu.com/p/113917726



关于MySQL索引原理，讲的最清晰的一篇文章

https://zhuanlan.zhihu.com/p/394429932



一文彻底搞懂MySQL索引

https://blog.csdn.net/aixuexidemomo/article/details/102780999
