## 阿里云-云服务器ECS安装WordPress(一)

[TOC]

### 一、安装宝塔面板

1、 安装命令

```shell
yum install -y wget && wget -O install.sh http://download.bt.cn/install/install.sh && sh install.sh
```

ps：

回车开始自动安装，安装大概需要5-15分钟
安装成功后，会显示出你的宝塔登录信息，帐号和密码，如图

![img](https://images2018.cnblogs.com/blog/1237756/201803/1237756-20180312142411631-1627458505.png)



2、访问宝塔面板，在浏览器输入 http://你的IP:8888/，显示如下页面，即可。

![](https://img-blog.csdnimg.cn/img_convert/ac951cfb1eeb3dcad804c022308329bd.png)



### 二、所需环境

以下环境，在宝塔面板中的软件商店安装即可。

1、mysql 5.7

2、PHP7.4

3、nginx 1.17.8



### 安装WordPress

也是在软件商店中安装word press 即可，具体安装教程，可参考下面。

教程链接：   [宝塔面板一键部署安装wordpress](https://www.chengxulvtu.com/bt-deploy-wordpress/)

![](https://img-blog.csdnimg.cn/aff786fb86064e24a1745d999502f00d.png)







