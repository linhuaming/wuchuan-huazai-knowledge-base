## 常用的CMS系统有哪些?

> CMS（Content Management System）内容管理系统，CMS系统是网站建设走向成功的重要组成部分，国内外的CMS种类比较多，下面介绍常用的CMS。



### 一、介绍

CMS是Content Management System的缩写，意为“内容管理系统”。 　　
CMS具有许多基于模板的优秀设计，可以加快网站开发的速度和减少开发的成本。

CMS的功能并不只限于文本处理，它也可以处理图片、Flash动画、声像流、图像甚至电子邮件档案。

CMS其实是一个很广泛的称呼，从一般的博客程序，新闻发布程序，到综合性的网站管理程序都可以被称为内容管理系统。


### 二、常用的cms系统

#### 1、企业网站系统

MetInfo(米拓)、蝉知、SiteServer CMS

#### 2、B2C商城系统

商派shopex、ecshop、hishop、xpshop

#### 3、门建站系统

Dede[CMS](https://www.metinfo.cn/)(织梦)、帝国CMS、PHPCMS、动易、cmstop

#### 4、博客系统

wordpress、Z-Blog

#### 5、论坛社区

discuz、phpwind、wecenter

#### 6、问题系统

Tipask、whatsns

#### 7、知识百科系统

HDwiki

#### 8、B2B门户系统

destoon、B2Bbuilder、友邻B2B

#### 9、人才招聘网站系统

骑士CMS、PHP云人才管理系统

#### 10、房产网站系统

FangCms

#### 11、在线教育建站系统

kesion(科汛)、EduSoho网校

#### 12、电影网站系统

苹果cms、ctcms、movcms

#### 13、小说文学建站系统

JIEQI CMS



