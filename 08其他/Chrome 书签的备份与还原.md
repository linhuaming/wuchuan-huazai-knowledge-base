## Chrome 书签的备份与还原

> 记录下谷歌浏览器书签的备份和还原，方便以后查找，同时也能帮助到大家



### 一、书签的位置

​	一般位于 C:\Users\linhuaming\AppData\Local\Google\Chrome\User Data\Defaultd 目录下的 Bookmarks 文件 。其中的linhuaming 目录是我个人的用户目录，要根据自己电脑用的实际情况查找，如下图所示



![image-20210610001819681](../img/image-20210610001819681.png)



### 二、书签的备份、还原

#### 1、备份

找到书签文件后，就容易了。接下来我们直接把Bookmarks 文件，拷贝保存就可以了。

#### 2、还原

直接把之前备份好的Bookmarks 文件，覆盖掉当前的Bookmarks 文件，即可




### 三、书签文件Bookmarks 说明

​	使用Notepad++ 打开文件，我们可以看下面的内容，如下图所示

![image-20210610002748442](../img/image-20210610002748442.png)

#### 1、内容说明

​	该文件里面是json内容的，里面有三个主要的内容分别是checksum、roots、version

- checksum 是校验码
- roots 是书签的根目录
- version 是书签的版本

#### 2、roots  内容

​	roots 包含 bookmark 、 other 、 synced 等

- bookmark 是默认的书签栏
- other 是其他书签栏
- synced 是移动设备书签

#### 3、bookmarks 内容

​	bookmarks 包含了如下内容：

- children 是否有下级书签或书签目录
- date_added 书签添加时间
- date_modified 书签修改时间
- guid 该书签的全局唯一标识
- id 书签的标识
- name 书签名称
- url 书签的连接

