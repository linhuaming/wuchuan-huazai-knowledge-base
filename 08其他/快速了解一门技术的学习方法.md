## 快速了解一门技术的学习方法

> 在平时的工作中，有时候需要快速了解学习一门技术，可以参考以下的方法。

[TOC]

#### 一、3W1H 学习法

##### 3W就是what、why、when

when：是什么

why：为什么使用

when：在什么应用场景下使用

##### 1H 就是 HOW

how：怎么使用



#### 二、了解+入门学习法

##### 简介、介绍、概述

##### 快速入门、学习教程、使用实例



例如：

spring 的简介、介绍、概述

spring 的快速入门、学习教程、使用实例（示例）