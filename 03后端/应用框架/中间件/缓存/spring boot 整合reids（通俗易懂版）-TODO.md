## spring boot 整合reids（通俗易懂版）

[TOC]

### 一、快速入门

#### 1、pom.xml 引入依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.4.9</version>
  </parent>

  <groupId>com.linhuaming</groupId>
  <artifactId>spring-boot-redis-demo</artifactId>
  <version>1.0-SNAPSHOT</version>

  <name>spring-boot-redis-demo</name>
  <!-- FIXME change it to the project's website -->
  <url>http://www.example.com</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
  </properties>

  <dependencies>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!-- reids 依赖 -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-redis</artifactId>
    </dependency>

    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.projectlombok</groupId>
      <artifactId>lombok</artifactId>
    </dependency>
  </dependencies>

</project>
```



#### 2、修改配置文件application.yml

```yml
server:
  port: 8082

spring:
  redis:
    # Redis数据库索引（默认为0）
    database: 1
    # redis的地址
    host: 127.0.0.1
    #redis连接端口
    port: 51478
    # reids的密码
    password: DFNCrT$JwvxuiYXP#fz2bhpz!Dol*JVcOE@#rf90Tu(lsP&l6$RYITikr*RXG4AoCTtjbX$iJfIdiBI#HqxNN%1TJiqz8ULy8eD

```



#### 3、添加reids 配置类

```java
package com.linhuaming.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.lang.annotation.Documented;

/**
 * redis 配置类
 */
@Configuration
public class RedisConfig {

    @Bean
    @SuppressWarnings("all")
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory){
        // 为了开发方便，直接使用String -> Object
        // 创建绑定链接
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);

        // 序列化配置
        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
        ObjectMapper om = new ObjectMapper();
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        jackson2JsonRedisSerializer.setObjectMapper(om);
        StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

        template.setKeySerializer(stringRedisSerializer);
        template.setHashKeySerializer(stringRedisSerializer);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.setHashValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();

        return template;
    }

}
```



#### 4、编写controller类测试

**user 实体类**

```java
package com.linhuaming.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class User implements Serializable {

    private String id;
    private String username;
    private String pssword;
    private Integer sex;
    private Integer age;

}
```



**DemoController类**

```java
package com.linhuaming.controller;


import com.linhuaming.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @GetMapping(value = "/demo1")
    public String demo1(){
        String msg = "this is demo1";
        return msg;
    }

    @GetMapping(value = "/demo2")
    public String demo2(){
        String username = "linhuaming";
        this.redisTemplate.opsForValue().set("username",username);
        return username;
    }

    @GetMapping(value = "/demo3")
    public User demo3(){
        User user = new User("1001","linhuaming","123456",1,26);
        this.redisTemplate.opsForValue().set("user",user);
        System.out.println(this.redisTemplate.opsForValue().get("user"));
        return  user;
    }

}
```



### 二、RedisTemplate 使用详解





参考：

https://www.cnblogs.com/cool-fun/p/15707666.html

https://www.cnblogs.com/JoshuaYu/p/15054580.html

https://www.cnblogs.com/ysySelf/p/14029925.html

