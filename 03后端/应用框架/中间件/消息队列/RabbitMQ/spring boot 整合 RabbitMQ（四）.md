## spring boot 整合 RabbitMQ（四）

[TOC]

### 一、步骤

>  **基于主题模式（Topic）**



#### 1、pom.xml 添加依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```



#### 2、yaml 配置文件，添加信息

```yaml
spring:
  rabbitmq:
    host: 127.0.0.1    #rabbitmq服务器地址
    port: 5672         #端口号
    username: guest    #用户名
    password: guest    #密码
    #virtual-host:     #虚拟主机
```



#### 3、添加配置类

```java
package com.linhuaming.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbitmq配置类
 */
@Configuration
public class RabbitMQConfig {
    /**
     * 交换机名称
     */
    public static final String TOPIC_EXCHANGE_NAME = "topic_exchange";

    /**
     * 队列名称
     */
    public static final String TOPIC_QUEUE_NAME1 = "topic_queue1";
    public static final String TOPIC_QUEUE_NAME2 = "topic_queue2";

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean("topicExchange")
    public Exchange topicExchange() {
        //第二个参数为是否支持持久化
        //对于不同的订阅模式，只需要调用不同的方法即可
        return ExchangeBuilder.topicExchange(TOPIC_EXCHANGE_NAME).durable(true).build();
    }

    /**
     * 声明队列1
     *
     * @return
     */
    @Bean("topicQueue1")
    public Queue topicQueue1() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME1).build();
    }

    /**
     * 声明队列2
     *
     * @return
     */
    @Bean("topicQueue2")
    public Queue topicQueue2() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME2).build();
    }

    /**
     * 队列1与交换机绑定
     * @param queue  队列
     * @param exchange  交换机
     * @return
     */
    @Bean
    public Binding topicQueueExchange1(@Qualifier("topicQueue1") Queue queue,
                                       @Qualifier("topicExchange") Exchange exchange) {
        //将哪个队列绑定到哪个交换机上，且对应的routingKey是什么
        return BindingBuilder.bind(queue).to(exchange).with("info.#").noargs();
    }

    /**
     * 队列2与交换机绑定
     * @param queue  队列
     * @param exchange  交换机
     * @return
     */
    @Bean
    public Binding topicQueueExchange2(@Qualifier("topicQueue2") Queue queue,
                                       @Qualifier("topicExchange") Exchange exchange) {
        //将哪个队列绑定到哪个交换机上，且对应的routingKey是什么
        return BindingBuilder.bind(queue).to(exchange).with("error.#").noargs();
    }

}
```



#### 4、生产者发送消息

```java
package com.linhuaming.controller;

import com.linhuaming.commo.entity.UserEntity;
import com.linhuaming.config.RabbitMQConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/rabbitmq/product")
public class RabbitMQProductController {

    // RabbitTemplate 用于发送消息，是一个类似于RedisTemplate、JdbcTemplate的类
    @Autowired(required = false)
    RabbitTemplate rabbitTemplate;

    @GetMapping("/testTopic1")
    public String testTopic1(){
        /**
         * 发送消息
         * 参数一：交换机名称
         * 参数二：路由routingKey,符合路由匹配规则即可
         * 参数三：发送的消息
         */
        String routingKey = "info.goods.list";
        String msg = "购买了一个商品！";
        rabbitTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE_NAME, routingKey, msg);
        return "发送消息成功！";
    }

    @GetMapping("/testTopic2")
    public String testTopic2(){
        /**
         * 发送消息
         * 参数一：交换机名称
         * 参数二：路由routingKey,符合路由匹配规则即可
         * 参数三：发送的消息
         */
        String routingKey = "error.order";
        String msg = "生成订单时出错了！";
        rabbitTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE_NAME, routingKey, msg);
        return "发送消息成功！";
    }

}
```



启动生产者应用，执行/rabbitmq/product/testTopic1 接口

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614222614962.png)

可以看到，此时 topic_queue1 队列已经有一条消息了。

![image-20220614222703221](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614222703221.png)

![image-20220614222806987](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614222806987.png)

> 注意：由于是基于主题模式下，生产者向交换机发送消息，同时指定了routing key 为 "info.goods.list"；
>
> 所以，交换机会根据 routing key 把消息发送到对应的队列上。



同理，执行/rabbitmq/product/testTopic2 接口

![image-20220614224613505](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614224613505.png)



此时 topic_queue2 队列也会有一条消息。

![image-20220614224645875](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614224645875.png)

![image-20220614224756003](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614224756003.png)



#### 5、消费者接收消息

```java
package com.linhuaming.listener;

import com.rabbitmq.client.Channel;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 监听消息,消费者
 */
@Slf4j
@Component
public class TopicListener {

    /**
     * 匹配info.#的队列
     *
     * @param message
     */
    @SneakyThrows
    @RabbitListener(queues = "topic_queue1")
    public void queue1(Message message, Channel channel) {
        byte[] body = message.getBody();
        System.out.println("msg = " + new String(body));
        //手动确认
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }

    /**
     * 匹配error.#的队列
     *
     * @param message
     */
    @SneakyThrows
    @RabbitListener(queues = "topic_queue2")
    public void queue2(Message message,Channel channel) {
        byte[] body = message.getBody();
        System.out.println("msg = " + new String(body));
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }

}
```



启动消费者应用，可以看到，接收到了topic_queue1，topic_queue2队列的消息

![image-20220614225844571](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614225844571.png)





### 二、序列化问题

#### 1、默认序列化方式

RabbitMQ 抽象出一个 MessageConvert 接口处理消息的序列化，其默认的实现是 SimpleMessageConverter ，但是却并不好用。

SimpleMessageConverter 对于要发送的消息体 body 为 byte[] 时不进行处理，如果是 String 则转成字节数组，如果是 Java 对象，则使用 jdk 序列化将消息转成字节数组，转出来的结果较大，含class类名，类相应方法等信息，因此性能较差，且会存在乱码问题。

下面尝试使用原始的序列化方式发送一个对象，看看是什么样子的：



**生产者发送消息：**

```java
@GetMapping("/testTopic3")
public String testTopic3(){
    /**
     * 发送消息
     * 参数一：交换机名称
     * 参数二：路由routingKey,符合路由匹配规则即可
     * 参数三：发送的消息
     */
    String routingKey = "error.order";
    UserEntity userEntity = new UserEntity();
    userEntity.setId("202206140001");
    userEntity.setUsername("凌桦铭");
    userEntity.setPassword("123456");
    userEntity.setSex(1);
    rabbitTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE_NAME, routingKey, userEntity);
    return "发送消息成功！";
}
```



**UserEntity 类**

> 注意：UserEntity对象，必须实现序列化接口！！！  

```java
package com.linhuaming.commo.entity;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

@Data
// @JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class UserEntity implements Serializable {

    private String id;

    private String username;

    private String password;

    private Integer sex;

    private MultipartFile imgFile;

}
```



生产者发送消息，但是我们点开看看：

![image-20220614230608743](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614230608743.png)

![image-20220614230532047](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614230532047.png)

>  一堆乱码，这显然很不友好，不利于查看。



#### 2、自定义序列化

自定义序列化的方式有很多，我们一般会将对象序列化成JSON对象，方便查看且结构清晰。

这里使用自带的Jackson2JsonMessageConverter序列化方式，当然，使用阿里提供的JSON系列化工具也可以。方法都是一样的。

实现自定义序列化以及反序列化，需要我们在配置类中进行配置：

```java
package com.linhuaming.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.messaging.handler.annotation.support.MessageHandlerMethodFactory;

/**
 * rabbitmq配置类
 */
@Configuration
public class RabbitMQConfig implements RabbitListenerConfigurer {
    /**
     * 交换机名称
     */
    public static final String TOPIC_EXCHANGE_NAME = "topic_exchange";

    /**
     * 队列名称
     */
    public static final String TOPIC_QUEUE_NAME1 = "topic_queue1";
    public static final String TOPIC_QUEUE_NAME2 = "topic_queue2";
    public static final String TOPIC_QUEUE_NAME3 = "topic_queue3";

    /**
     * 声明交换机
     *
     * @return
     */
    @Bean("topicExchange")
    public Exchange topicExchange() {
        //第二个参数为是否支持持久化
        //对于不同的订阅模式，只需要调用不同的方法即可
        return ExchangeBuilder.topicExchange(TOPIC_EXCHANGE_NAME).durable(true).build();
    }

    /**
     * 声明队列1
     *
     * @return
     */
    @Bean("topicQueue1")
    public Queue topicQueue1() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME1).build();
    }

    /**
     * 声明队列2
     *
     * @return
     */
    @Bean("topicQueue2")
    public Queue topicQueue2() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME2).build();
    }

    /**
     * 声明队列3
     *
     * @return
     */
    @Bean("topicQueue3")
    public Queue topicQueue3() {
        return QueueBuilder.durable(TOPIC_QUEUE_NAME3).build();
    }

    /**
     * 队列1与交换机绑定
     * @param queue  队列
     * @param exchange  交换机
     * @return
     */
    @Bean
    public Binding topicQueueExchange1(@Qualifier("topicQueue1") Queue queue,
                                       @Qualifier("topicExchange") Exchange exchange) {
        //将哪个队列绑定到哪个交换机上，且对应的routingKey是什么
        return BindingBuilder.bind(queue).to(exchange).with("info.#").noargs();
    }

    /**
     * 队列2与交换机绑定
     * @param queue  队列
     * @param exchange  交换机
     * @return
     */
    @Bean
    public Binding topicQueueExchange2(@Qualifier("topicQueue2") Queue queue,
                                       @Qualifier("topicExchange") Exchange exchange) {
        //将哪个队列绑定到哪个交换机上，且对应的routingKey是什么
        return BindingBuilder.bind(queue).to(exchange).with("warning.#").noargs();
    }

    /**
     * 队列3与交换机绑定
     * @param queue  队列
     * @param exchange  交换机
     * @return
     */
    @Bean
    public Binding topicQueueExchange3(@Qualifier("topicQueue3") Queue queue,
                                       @Qualifier("topicExchange") Exchange exchange) {
        //将哪个队列绑定到哪个交换机上，且对应的routingKey是什么
        return BindingBuilder.bind(queue).to(exchange).with("warning.#").noargs();
    }


    /******************************************** 序列化配置 ************************************************/
    //    /**
    //     * 序列化方式1：
    //     * @return
    //     */
    //    //    @Bean
    //    //    public RabbitTemplate jacksonRabbitTemplate(ConnectionFactory connectionFactory){
    //    //        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
    //    //        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
    //    //        return rabbitTemplate;
    //    //    }

    /**
     * 序列化方式2：
     * @return
     */
    @Bean
    public MessageConverter getMessageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    /******************************************** 反序列化配置 ************************************************/
    /**
     * 可以将JSON串反序列化为对象
     * @param rer
     */
    @Override
    public void configureRabbitListeners(RabbitListenerEndpointRegistrar rer){
        rer.setMessageHandlerMethodFactory(mhmf());
    }

    @Bean
    public MessageHandlerMethodFactory mhmf(){
        DefaultMessageHandlerMethodFactory mhf = new DefaultMessageHandlerMethodFactory();
        mhf.setMessageConverter(mappingJackson2MessageConverter());
        return mhf;
    }

    @Bean
    public MappingJackson2MessageConverter mappingJackson2MessageConverter(){
        return new MappingJackson2MessageConverter();
    }

}
```



生产者发送消息：

```java
@GetMapping("/testTopic3")
public String testTopic3(){
    /**
     * 发送消息
     * 参数一：交换机名称
     * 参数二：路由routingKey,符合路由匹配规则即可
     * 参数三：发送的消息
     */
    String routingKey = "warning.order";
    UserEntity userEntity = new UserEntity();
    userEntity.setId("202206140001");
    userEntity.setUsername("凌桦铭");
    userEntity.setPassword("123456");
    userEntity.setSex(1);
    rabbitTemplate.convertAndSend(RabbitMQConfig.TOPIC_EXCHANGE_NAME, routingKey, userEntity);
    return "发送消息成功！";
}
```



**消费者接收消息：**

>  注意：  queue3() ， 要多一个参数Object 

```java
   /**
     * 匹配error.#的队列
     * 
     * @param message
     */
    @RabbitListener(queues = "topic_queue3")
    @SneakyThrows
    public void queue3(Message message,Object obj,Channel channel) {
        byte[] body = message.getBody();
        System.out.println("接收 topic_queue3 队列, 消息: " + new String(body));
        channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
    }
```



设置完成之后，我们再发送一条：

![image-20220614235931504](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614235931504.png)



此时接收消息，没有问题

![image-20220614235248297](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220614235248297.png)



### 三、监听消息

#### 1、@RabbitListener

编写1个Service，声明一个监听方法，方法上标注@RabbitListener，传入需要监听的队列名。监听方法可以接收的参数如下(无需保证参数顺序)：

Message对象：原生消息的详细信息，包括消息头+消息体
自定义实体类对象：用消息体反序列化后得到的Javabean
Channel对象：当前传输的数据通道

```java
@Service
public class PersonService {
    /**
     * 监听队列baobao中的消息，有消息会自动取出并回调该方法
     * @param message 原生消息的详细信息，包括消息头+消息体
     * @param person  从消息体中解码出的javabean
     * @param channel 当前传输的数据通道
     */
    @RabbitListener(queues = "baobao")
    public void listen(Message message, Person person, Channel channel){
        System.out.println(message);
        System.out.println(person);
        System.out.println(channel);
    }
}
```



#### 2、@RabbitHandler

我们还可以采用@RabbitListener配合@RabbitHandler的方式完成对消息的监听：

@RabbitListener：标注在类上，指定监听哪些队列
@RabbitHandler：标注在每个接收并处理不同消息的重载方法上，区分处理不同类型的消息

**举例：**

```java
@Service
@RabbitListener(queues = {"baobao","baobao.news","baobao.map"})
public class PersonService {
    @RabbitHandler
    public void handlePersonMsg(Person person){
        System.out.println(person);
    }

    @RabbitHandler
    public void handleUserMsg(User user){
        System.out.println(user);
    }
}
```





参考：

https://blog.csdn.net/qq_44760609/article/details/125103507

https://blog.csdn.net/qq_35387940/article/details/100514134

https://blog.csdn.net/weixin_45070175/article/details/118559313

