## RabbitMQ 快速入门（三）

[TOC]

### 一、RabbitMQ工作原理

![](https://img2018.cnblogs.com/i-beta/1485457/202001/1485457-20200131173249426-136707265.png)



**组成部分说明：**

- 生产者（Producer）：发送消息的应用。
- 消费者（Consumer）：接收消息的应用。
- 消息（Message）：由生产者通过RabbitMQ发送给消费者的信息。
- 连接（Connection）：连接RabbitMQ和应用服务器的TCP连接。
- 通道（Channel）：连接里的一个虚拟通道。当你通过消息队列发送或者接收消息时，这个操作都是通过通道进行的。
- Broker：消息队列服务进程，此进程包括两个部分：Exchange和Queue
- 交换机（Exchange）：交换机负责从生产者那里接收消息，并根据交换类型分发到对应的消息列队里。要实现消息的接收，一个队列必须到绑定一个交换机。
- 队列（Queue）：存储消息的缓存。
- 绑定（Binding）：绑定是队列和交换机的一个关联连接。
- 路由键（Routing Key）：路由键是供交换机查看并根据键来决定如何分发消息到列队的一个键。路由键可以说是消息的目的地址。



**生产者发送消息流程：**

1、生产者和Broker建立TCP连接。

2、生产者和Broker建立通道。

3、生产者通过通道消息发送给Broker，由Exchange将消息进行转发。

4、Exchange将消息转发到指定的Queue（队列）



**消费者接收消息流程：**

1、消费者和Broker建立TCP连接

2、消费者和Broker建立通道

3、消费者监听指定的Queue（队列）

4、当有消息到达Queue时Broker默认将消息推送给消费者。

5、消费者接收到消息。

6、ack回复



### 二、消息发送模式-演示操作

**pom文件，添加amqp-client依赖**

```xml
<dependency>
    <groupId>com.rabbitmq</groupId>
    <artifactId>amqp-client</artifactId>
    <version>5.10.0</version>
</dependency>
```



**创建工具类**

```java
public class ConnectionUtil {
    /**
     * 建立与RabbitMQ的连接
     * @return
     * @throws Exception
     */
    public static Connection getConnection() throws Exception {
        // 定义连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        // 设置服务地址
        factory.setHost("127.0.0.1");
        //端口
        factory.setPort(5672);
        
        //设置账号信息，用户名、密码、vhost
        // factory.setVirtualHost("/kavito");//设置虚拟机，一个mq服务可以设置多个虚拟机，每个虚拟机就相当于一个独立的mq
        // factory.setUsername("kavito");
        // factory.setPassword("123456");
        
        // 通过工厂获取连接
        Connection connection = factory.newConnection();
        return connection;
    }
}
```



#### 1、简单队列

![](https://img-blog.csdnimg.cn/20190610232537261.png)

在上图的模型中，有以下概念：

- P：生产者，也就是要发送消息的程序
- C：消费者：消息的接受者，会一直等待消息到来。
- queue：消息队列，图中红色部分。可以缓存消息；生产者向其中投递消息，消费者从其中取出消息。



**生产者发送消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

/**
 * 简单队列,生产者
 */
@Slf4j
public class SimpleProducer {
    private final static String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws Exception {
        /**
         * 1.获取到连接
         */
        Connection connection = RabbitMQConnectionUtil.getConnection();

        /**
         * 2.从连接中创建通道，使用通道才能完成消息相关的操作
         */
        Channel channel = connection.createChannel();

        /**
         * 3.声明（创建）队列
         * queueDeclare(String var1, boolean var2, boolean var3, boolean var4, Map<String, Object> var5)
         * 参数明细
         * 1、queue 队列名称
         * 2、durable 是否持久化，如果持久化，mq重启后队列还在
         * 3、exclusive 是否独占连接，队列只允许在该连接中访问，如果connection连接关闭队列则自动删除,如果将此参数设置true可用于临时队列的创建
         * 4、autoDelete 自动删除，队列不再使用时是否自动删除此队列，如果将此参数和exclusive参数设置为true就可以实现临时队列（队列不用了就自动删除）
         * 5、arguments 参数，可以设置一个队列的扩展参数，比如：可设置存活时间
         */
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        /**
         * 4.消息内容
         */
        String message = "Hello World!";

        /**
         * 5.向指定的队列中发送消息
         * basicPublish(String var1, String var2, BasicProperties var3, byte[] var4)
         * 参数明细：
         * 1、exchange，交换机，如果不指定将使用mq的默认交换机（设置为""）
         * 2、routingKey，路由key，交换机根据路由key来将消息转发到指定的队列，如果使用默认交换机，routingKey设置为队列的名称
         * 3、props，消息的属性
         * 4、body，消息内容
         */
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
        log.info(" [SimpleProducer] Sent:"+message);

        /**
         * 6.关闭通道和连接(资源关闭最好用try-catch-finally语句处理)
         */
        channel.close();
        connection.close();
    }

}

```

**控制台**：

![image-20220607230734316](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220607230734316.png)



**web管理页面**

点击Queues 栏目，如下图所示

![image-20220607230930862](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220607230930862.png)

点击队列名称，进入队列，依次点击Get message > Get Message(s)   ， 可以看到消息结果，如下图所示

![image-20220607231141420](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220607231141420.png)



**消费者接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 简单队列,消费者
 */
@Slf4j
public class SimpleConsumer {
    private final static String QUEUE_NAME = "simple_queue";

    public static void main(String[] args) throws Exception {
        /**
         * 1.获取到连接
         */
        Connection connection = RabbitMQConnectionUtil.getConnection();

        /**
         * 2.从连接中创建通道，使用通道才能完成消息相关的操作
         */
        Channel channel = connection.createChannel();

        /**
         * 3.声明（创建）队列
         * queueDeclare(String var1, boolean var2, boolean var3, boolean var4, Map<String, Object> var5)
         * 参数明细
         * 1、queue 队列名称
         * 2、durable 是否持久化，如果持久化，mq重启后队列还在
         * 3、exclusive 是否独占连接，队列只允许在该连接中访问，如果connection连接关闭队列则自动删除,如果将此参数设置true可用于临时队列的创建
         * 4、autoDelete 自动删除，队列不再使用时是否自动删除此队列，如果将此参数和exclusive参数设置为true就可以实现临时队列（队列不用了就自动删除）
         * 5、arguments 参数，可以设置一个队列的扩展参数，比如：可设置存活时间
         */
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        /**
         * 4.实现消费方法
         */
        DefaultConsumer consumer = new DefaultConsumer(channel){
            /**
             * 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
             * @param consumerTag  消费者标签，用来标识消费者的，在监听队列时设置channel.basicConsume
             * @param envelope  信封，通过envelope
             * @param properties  消息属性
             * @param body  消息内容
             * @throws IOException
             */
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // 交换机
                String exchange = envelope.getExchange();
                // 消息id，mq在channel中用来标识消息的id，可用于确认消息已接收
                long deliveryTag = envelope.getDeliveryTag();
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[SimpleConsumer] received: "+msg);
            }
        };

        /**
         * 5.监听队列，第二个参数：是否自动进行消息确认
         * 参数明细：
         * 1、queue 队列名称
         * 2、autoAck 自动回复，当消费者接收到消息后要告诉mq消息已接收，如果将此参数设置为tru表示会自动回复mq，如果设置为false要通过编程实现回复
         * 3、callback，消费方法，当消费者接收到消息要执行的方法
         */
        channel.basicConsume(QUEUE_NAME, true, consumer);

    }
}
```



**控制台**

![image-20220607233657224](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220607233657224.png)

> 注意：左上角那里，我们发现，消费者已经获取了消息，但是程序没有停止，一直在监听队列中是否有新的消息。一旦有新的消息进入队列，就会立即打印。



**web管理界面**

再看看队列的消息,已经被消费了

![image-20220607233453086](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220607233453086.png)



**消息确认机制（ACK）**

通过刚才的案例可以看出，消息一旦被消费者接收，队列中的消息就会被删除。

那么问题来了：RabbitMQ怎么知道消息被接收了呢？

如果消费者领取消息后，还没执行操作就挂掉了呢？或者抛出了异常？消息消费失败，但是RabbitMQ无从得知，这样消息就丢失了！

因此，RabbitMQ有一个ACK机制。当消费者获取消息后，会向RabbitMQ发送回执ACK，告知消息已经被接收。不过这种回执ACK分两种情况：

- 自动ACK：消息一旦被接收，消费者自动发送ACK

- 手动ACK：消息接收后，不会发送ACK，需要手动调用



大家觉得哪种更好呢？

这需要看消息的重要性：

如果消息不太重要，丢失也没有影响，那么自动ACK会比较方便

如果消息非常重要，不容丢失。那么最好在消费完成后手动ACK，否则接收消息后就自动ACK，RabbitMQ就会把消息从队列中删除。如果此时消费者宕机，那么消息就丢失了。



**自动ACK存在的问题**

修改消费者，添加异常，如下：

![](https://img-blog.csdnimg.cn/20190611111613865.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2thdml0bw==,size_16,color_FFFFFF,t_70)



生产者不做任何修改，直接运行，消息发送成功：

![](https://img-blog.csdnimg.cn/20190611111805436.png)



运行消费者，程序抛出异常：

![](https://img-blog.csdnimg.cn/20190611111903348.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2thdml0bw==,size_16,color_FFFFFF,t_70)



可以看到，但是消息依然被消费，实际上我们还没获取到消息。

![](https://img-blog.csdnimg.cn/20190611111945679.png)



**演示手动ACK**

重新运行生产者发送消息：

![](https://img-blog.csdnimg.cn/20190611111805436.png)



同样，在手动进行ack前抛出异常，运行

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220608225937660.png)

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220608230018247.png)



 再看看， 消息没有被消费掉了！

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220608230633778.png)



#### 2、工作队列

![](https://img-blog.csdnimg.cn/20190611201227139.png)

work queues与简单队列相比，多了一个消费端，两个消费端共同消费同一个队列中的消息，但是一个消息只能被一个消费者获取。

这个消息模型在Web应用程序中特别有用，可以处理短的HTTP请求窗口中无法处理复杂的任务。

接下来我们来模拟这个流程：

P：生产者：任务的发布者

C1：消费者1：领取任务并且完成任务，假设完成速度较慢（模拟耗时）

C2：消费者2：领取任务并且完成任务，假设完成速度较快



**生产者发送消息：**

生产者循环发送50条消息

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

/**
 * 工作队列, 生产者
 */
@Slf4j
public class WorkProducer {
    private final static String QUEUE_NAME = "work_queue";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.循环发布任务
        for (int i = 0; i < 50; i++) {
            // 消息内容
            String message = "task .. " + i;
            channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
            log.info(" [WorkProducer] Sent: "+message);
            // Thread.sleep(i * 2);
        }

        // 关闭通道和连接
        channel.close();
        connection.close();
    }

}
```



**消费者1接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 工作队列, 消费者
 */
@Slf4j
public class WorkConsumer {
    private final static String QUEUE_NAME = "work_queue";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.创建会话通道,生产者和mq服务所有通信都在channel通道中完成
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.实现消费方法
        DefaultConsumer consumer = new DefaultConsumer(channel){
            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[WorkConsumer] received: "+msg);

                //模拟任务耗时1s
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // 手动进行ACK
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };

        // 5.监听队列，第二个参数：是否自动进行消息确认。
        channel.basicConsume(QUEUE_NAME, false, consumer);
    }

}

```



**消费者2接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 工作队列, 消费者2
 */
@Slf4j
public class WorkConsumer2 {
    private final static String QUEUE_NAME = "work_queue";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.创建会话通道,生产者和mq服务所有通信都在channel通道中完成
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.实现消费方法
        DefaultConsumer consumer = new DefaultConsumer(channel){
            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[WorkConsumer2] received: "+msg);

                // 手动进行ACK
                channel.basicAck(envelope.getDeliveryTag(),false);
            }
        };

        // 5.监听队列，第二个参数：是否自动进行消息确认。
        channel.basicConsume(QUEUE_NAME, false, consumer);
    }

}
```



接下来，两个消费者一同启动，然后生产者发送50条消息

![](https://img-blog.csdnimg.cn/20190611124709504.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2thdml0bw==,size_16,color_FFFFFF,t_70)

![](https://img-blog.csdnimg.cn/20190611124745242.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L2thdml0bw==,size_16,color_FFFFFF,t_70)

>  可以发现，两个消费者各自消费了不同25条消息，这就实现了任务的分发。



**刚才的实现有问题吗？**

- 消费者1比消费者2的效率要低，一次任务的耗时较长
- 然而两人最终消费的消息数量是一样的
- 消费者2大量时间处于空闲状态，消费者1一直忙碌



现在的状态属于是把任务平均分配，正确的做法应该是消费越快的，消费的越多。

怎么实现呢？

通过 BasicQos 方法设置prefetchCount = 1。这样RabbitMQ就会使得每个Consumer在同一个时间点最多处理1个Message。换句话说，在接收到该Consumer的ack前，他它不会将新的Message分发给它。相反，它会将其分派给不是仍然忙碌的下一个Consumer。

值得注意的是：prefetchCount在手动ack的情况下才生效，自动ack不生效。

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609225531660.png)



重新启动下生产者：

![image-20220609230018395](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609230018395.png)



消费者1，消费情况：

![image-20220609225727422](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609225727422.png)



消费者2，消费情况：

![image-20220609225802444](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609225802444.png)



订阅模型分类：

1、一个生产者多个消费者
2、每个消费者都有一个自己的队列
3、生产者没有将消息直接发送给队列，而是发送给exchange(交换机、转发器)
4、每个队列都需要绑定到交换机上
5、生产者发送的消息，经过交换机到达队列，实现一个消息被多个消费者消费
例子：注册->发邮件、发短信

X（Exchanges）：交换机一方面：接收生产者发送的消息。另一方面：知道如何处理消息，例如递交给某个特别队列、递交给所有队列、或是将消息丢弃。到底如何操作，取决于Exchange的类型。

Exchange类型有以下几种：

Fanout：广播，将消息交给所有绑定到交换机的队列

Direct：定向，把消息交给符合指定routing key 的队列

Topic：通配符，把消息交给符合routing pattern（路由模式） 的队列

Header：header模式与routing不同的地方在于，header模式取消routingkey，使用header中的 key/value（键值对）匹配队列。

Header模式不展开了，感兴趣可以参考这篇文章：https://blog.csdn.net/zhu_tianwei/article/details/40923131

Exchange（交换机）只负责转发消息，不具备存储消息的能力，因此如果没有任何队列与Exchange绑定，或者没有符合路由规则的队列，那么消息会丢失！



#### 3、发布/订阅模式

Publish/subscribe模型示意图 ：

![](https://img-blog.csdnimg.cn/20190611200955466.png)

> **交换机类型：**Fanout，也称为广播





**生产者发送消息：**

- 1） 声明Exchange，不再声明Queue
- 2） 发送消息到Exchange，不再发送到Queue

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

/**
 * 发布/订阅模式, 生产者
 */
@Slf4j
public class PubSubProducer {
    private final static String EXCHANGE_NAME = "test_fanout_exchange";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明exchange，指定类型为fanout
        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        // 4.消息内容
        String message = "注册成功！！";

        // 5.发布消息到Exchange
        channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
        log.info("[PubSubProducer] Sent:{}",message);

        // 6.关闭
        channel.close();
        connection.close();
    }

}
```



**消费者1，接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 发布/订阅模式, 消费者
 */
@Slf4j
public class PubSubConsumer {
    private final static String EXCHANGE_NAME = "test_fanout_exchange";

    private final static String QUEUE_NAME = "fanout_exchange_queue_sms";//短信队列

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.绑定队列到交换机
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");

        // 5.定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {

            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[PubSubConsumer] sms received:",msg);
            }
        };

        // 6.监听队列，自动返回完成
        channel.basicConsume(QUEUE_NAME, true, consumer);

    }
}
```



**消费者2，接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 发布/订阅模式, 消费者
 */
@Slf4j
public class PubSubConsumer2 {
    private final static String EXCHANGE_NAME = "test_fanout_exchange";

    private final static String QUEUE_NAME = "fanout_exchange_queue_email"; // 邮件队列

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.绑定队列到交换机
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "");

        // 5.定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {

            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[PubSubConsumer ] email received:",msg);
            }
        };

        // 6.监听队列，自动返回完成
        channel.basicConsume(QUEUE_NAME, true, consumer);

    }
}
```



启动生产者，发送一条消息

![image-20220609234727592](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609234727592.png)



web管理界面，点击Exchange ，可以看到有了刚才创建的交换机

![image-20220609235347792](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609235347792.png)

> 注意：先启动生产者，才会创建交换机，不然启动消费者先，会报错。





运行消费者1

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609234758769.png)



运行消费者2

![image-20220609234906589](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220609234906589.png)



#### 4、路由模式

Routing模型示意图：

![](https://img-blog.csdnimg.cn/20190611151532501.png)



P：生产者，向Exchange发送消息，发送消息时，会指定一个routing key

X：Exchange（交换机），接收生产者的消息，然后把消息递交给与routing key完全匹配的队列

C1：消费者，其所在的队列指定了需要routing key 为 error 的消息

C2：消费者，其所在的队列指定了需要routing key 为 info、error、warning 的消息



**生产者发送消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

/**
 * 路由模式, 生产者
 */
@Slf4j
public class RouterProducer {
    private final static String EXCHANGE_NAME = "test_direct_exchange";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明exchange，指定类型为 direct
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT.getType());

        // 4.消息内容
        String message = "注册成功！请短信回复[T]退订";

        // 5.发布消息到Exchange, 并且指定routing key 为：sms，只有短信服务能接收到消息
        channel.basicPublish(EXCHANGE_NAME, "sms", null, message.getBytes());
        log.info("[RouterProducer] Sent:{}",message);

        // 6.关闭
        channel.close();
        connection.close();
    }

}
```



**消费者1接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 路由模式, 消费者
 */
@Slf4j
public class RouterConsumer {
    private final static String EXCHANGE_NAME = "test_direct_exchange";

    private final static String QUEUE_NAME = "direct_exchange_queue_sms_router";//短信队列

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.绑定队列到交换机,同时指定需要订阅的routing key,可以指定多个
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "sms");

        // 5.定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {

            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[RouterConsumer] sms received: {}",msg);
            }
        };

        // 6.监听队列，自动返回完成
        channel.basicConsume(QUEUE_NAME, true, consumer);

    }
}
```



**消费者2接收消息：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 路由模式, 消费者2
 */
@Slf4j
public class RouterConsumer2 {
    private final static String EXCHANGE_NAME = "test_direct_exchange";

    private final static String QUEUE_NAME = "direct_exchange_queue_email_router";//短信队列

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.绑定队列到交换机,同时指定需要订阅的routing key,可以指定多个
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "email");

        // 5.定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {

            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body,"utf-8");
                log.info("[RouterConsumer2] emal received:",msg);
            }
        };

        // 6.监听队列，自动返回完成
        channel.basicConsume(QUEUE_NAME, true, consumer);

    }
}
```



**启动下生产者：**

![image-20220612224144978](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220612224144978.png)



**消费者1：**

我们发送sms的RoutingKey，发现结果：只有指定短信的消费者1收到消息了

![image-20220612230304096](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220612230304096.png)



#### 5、主题模式

Topics模型示意图：

![](https://img-blog.csdnimg.cn/20190611154905286.png)



> 每个消费者监听自己的队列，并且设置带统配符的routingkey,生产者将消息发给broker，由交换机根据routingkey来转发消息到指定的队列



Routingkey一般都是有一个或者多个单词组成，多个单词之间以“.”分割，例如：inform.sms

通配符规则：

`#`：匹配一个或多个词

`*`：匹配不多不少，恰好1个词

举例：

```shell
audit.#`：能够匹配`audit.irs.corporate` 或者 `audit.irs
audit.*`：只能匹配`audit.irs
```



**实例说明：**

从示意图可知，我们将发送所有描述动物的消息。消息将使用由三个字（两个点）组成的Routing key发送。路由关键字中的第一个单词将描述速度，第二个颜色和第三个种类：

```java
“<speed>.<color>.<species>”
```



```java
我们创建了三个绑定：Q1绑定了“*.orange.*”，Q2绑定了“.*.*.rabbit”和“lazy.＃”。

Q1匹配所有的橙色动物。

Q2匹配关于兔子以及懒惰动物的消息。

 下面做个小练习，假如生产者发送如下消息，会进入哪个队列：

quick.orange.rabbit       Q1 Q2   routingKey="quick.orange.rabbit"的消息会同时路由到Q1与Q2

lazy.orange.elephant    Q1 Q2

quick.orange.fox           Q1

lazy.pink.rabbit              Q2  (值得注意的是，虽然这个routingKey与Q2的两个bindingKey都匹配，但是只会投递Q2一次)

quick.brown.fox            不匹配任意队列，被丢弃

quick.orange.male.rabbit   不匹配任意队列，被丢弃

orange         不匹配任意队列，被丢弃

下面我们以指定Routing key="quick.orange.rabbit"为例，验证上面的答案
```



**生产者：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import lombok.extern.slf4j.Slf4j;

/**
 * 主题模式, 生产者
 */
@Slf4j
public class TopicProducer {
    private final static String EXCHANGE_NAME = "test_topic_exchange";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明exchange，指定类型为topic
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);

        // 4.消息内容
        String message = "这是一只行动迅速的橙色的兔子";

        // 5.发送消息，并且指定routing key为：quick.orange.rabbit
        channel.basicPublish(EXCHANGE_NAME, "quick.orange.rabbit", null, message.getBytes());
        log.info("[TopicProducer] Sent:{}",message);

        // 6.关闭
        channel.close();
        connection.close();
    }

}
```



**消费者1：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 主题模式, 消费者
 */
@Slf4j
public class TopicConsumer {
    private final static String EXCHANGE_NAME = "test_topic_exchange";

    private final static String QUEUE_NAME = "topic_exchange_queue_Q1";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.绑定队列到交换机，同时指定需要订阅的routing key。订阅所有的橙色动物
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.orange.*");

        // 5.定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body);
                log.info("[TopicConsumer] Q1 received: {}",msg);
            }
        };
        // 6.监听队列，自动ACK
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

}
```



**消费者2：**

```java
package com.linhuaming.test;

import com.linhuaming.utils.RabbitMQConnectionUtil;
import com.rabbitmq.client.*;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * 主题模式, 消费者2
 */
@Slf4j
public class TopicConsumer2 {

    private final static String EXCHANGE_NAME = "test_topic_exchange";

    private final static String QUEUE_NAME = "topic_exchange_queue_Q2";

    public static void main(String[] args) throws Exception {
        // 1.获取到连接
        Connection connection = RabbitMQConnectionUtil.getConnection();

        // 2.获取通道
        Channel channel = connection.createChannel();

        // 3.声明队列
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);

        // 4.绑定队列到交换机，同时指定需要订阅的routing key。订阅所有的橙色动物
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "*.orange.*");

        // 5.定义队列的消费者
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            // 获取消息，并且处理，这个方法类似事件监听，如果有消息的时候，会被自动调用
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                // body 即消息体
                String msg = new String(body);
                log.info("[TopicConsumer2] Q2 received: {}",msg);
            }
        };
        // 6.监听队列，自动ACK
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

}
```



**启动下生产者：**

![image-20220612233313161](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220612233313161.png)



>  结果C1、C2是都接收到消息了：

**消费者1：**

![image-20220612233407101](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220612233407101.png)

**消费者2：**

![image-20220612233423342](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220612233423342.png)



#### 6、RPC模式

RPC模型示意图：

![](https://img-blog.csdnimg.cn/20190611200118579.png)

基本概念：

Callback queue 回调队列，客户端向服务器发送请求，服务器端处理请求后，将其处理结果保存在一个存储体中。而客户端为了获得处理结果，那么客户在向服务器发送请求时，同时发送一个回调队列地址reply_to。

Correlation id 关联标识，客户端可能会发送多个请求给服务器，当服务器处理完后，客户端无法辨别在回调队列中的响应具体和那个请求时对应的。为了处理这种情况，客户端在发送每个请求时，同时会附带一个独有correlation_id属性，这样客户端在回调队列中根据correlation_id字段的值就可以分辨此响应属于哪个请求。

流程说明：

- 当客户端启动的时候，它创建一个匿名独享的回调队列。
- 在 RPC 请求中，客户端发送带有两个属性的消息：一个是设置回调队列的 reply_to 属性，另一个是设置唯一值的 correlation_id 属性。
- 将请求发送到一个 rpc_queue 队列中。
- 服务器等待请求发送到这个队列中来。当请求出现的时候，它执行他的工作并且将带有执行结果的消息发送给 reply_to 字段指定的队列。
- 客户端等待回调队列里的数据。当有消息出现的时候，它会检查 correlation_id 属性。如果此属性的值与请求匹配，将它返回给应用。



参考：

https://blog.csdn.net/kavito/article/details/91403659