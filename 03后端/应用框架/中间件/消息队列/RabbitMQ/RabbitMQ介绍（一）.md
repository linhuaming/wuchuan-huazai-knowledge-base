## RabbitMQ介绍（一）

[TOC]

### 一、RabbitMQ简介

​		 RabbitMQ是一个由erlang语言编写的、开源的、在AMQP基础上完整的、可复用的企业消息系统。支持多种语言，包括java、Python、ruby、PHP、C/C++等。

​		MQ：MQ是 message queue 的简称，是应用程序和应用程序之间通信的方法。
​		AMQP：advanced message queuing protocol ，一个提供统一消息服务的应用层标准高级消息队列协议，是应用层协议的一个开放标准，为面向消息的中间件设计。基于此协议的客户端与消息中间件可传递消息并不受客户端/中间件不同产品、不同开发语言等条件的限制。



**RabbitMQ特点：**

1. 可靠性：RabbitMQ 使用一些机制来保证可靠性，如持久化、传输确认、发布确认。

2. 灵活的路由：在消息进入队列之前，通过 Exchange 来路由消息的。对于典型的路由功能，RabbitMQ 已经提供了一些内置的 Exchange 来实现。针对更复杂的路由功能，可以将多个 Exchange 绑定在一起，也通过插件机制实现自己的 Exchange。

3. 消息集群：多个 RabbitMQ 服务器可以组成一个集群，形成一个逻辑 Broker 。

4. 高可用：队列可以在集群中的机器上进行镜像，使得在部分节点出问题的情况下队列仍然可用。

5. 多种协议：RabbitMQ 支持多种消息队列协议，比如 STOMP、MQTT 等等。

6. 多语言客户端：RabbitMQ 几乎支持所有常用语言，比如 Java、.NET、Ruby 等等。

7. 管理界面：RabbitMQ 提供了一个易用的用户界面，使得用户可以监控和管理消息 Broker 的许多方面。

8. 跟踪机制：如果消息异常，RabbitMQ 提供了消息跟踪机制，使用者可以找出发生了什么。

9. 插件机制：RabbitMQ 提供了许多插件，来从多方面进行扩展，也可以编写自己的插件。



### 二、RabbitMQ应用场景

**1、异步通信**

​	主业务执行结束后，从属业务通过MQ异步处理，减少业务的响应时间，提高用户体验。



**2、系统解耦**

​		一个业务需要多个模块共同实现，或一条消息有多个系统对应处理，只需要在主业务完成以后，发送一条MQ，其余模块消费MQ消息，即可实现业务，降低模块之间的耦合。



**3、流量削峰**

​		高并发情况下，业务异步处理，提供高峰期业务处理能力，避免系统瘫痪。



### 三、RabbitMQ的核心概念

![](https://img2018.cnblogs.com/i-beta/1485457/202001/1485457-20200131173249426-136707265.png)



**组成部分说明：**

- 生产者（Producer）：发送消息的应用。
- 消费者（Consumer）：接收消息的应用。
- 消息（Message）：由生产者通过RabbitMQ发送给消费者的信息。
- 连接（Connection）：连接RabbitMQ和应用服务器的TCP连接。
- 通道（Channel）：连接里的一个虚拟通道。当你通过消息队列发送或者接收消息时，这个操作都是通过通道进行的。
- Broker：消息队列服务进程，此进程包括两个部分：Exchange和Queue
- 交换机（Exchange）：交换机负责从生产者那里接收消息，并根据交换类型分发到对应的消息列队里。要实现消息的接收，一个队列必须到绑定一个交换机。
- 队列（Queue）：存储消息的缓存。
- 绑定（Binding）：绑定是队列和交换机的一个关联连接。
- 路由键（Routing Key）：路由键是供交换机查看并根据键来决定如何分发消息到列队的一个键。路由键可以说是消息的目的地址。



**生产者发送消息流程：**

1、生产者和Broker建立TCP连接。

2、生产者和Broker建立通道。

3、生产者通过通道消息发送给Broker，由Exchange将消息进行转发。

4、Exchange将消息转发到指定的Queue（队列）



**消费者接收消息流程：**

1、消费者和Broker建立TCP连接

2、消费者和Broker建立通道

3、消费者监听指定的Queue（队列）

4、当有消息到达Queue时Broker默认将消息推送给消费者。

5、消费者接收到消息。

6、ack回复



### 四、RabbitMQ五种消息发送模式

​	 生产者（Producer）发送->中间件->消费者（Consumer）接收消息。RabbitMQ包括五种队列模式，简单队列、工作队列、发布/订阅、路由、主题等。

#### 1、简单队列

- 生产者将消息发送到队列，消费者从队列获取消息。
- 一个队列对应一个消费者。

![img](https://img-blog.csdnimg.cn/a7407f2498fb44e986e96ac0c0ecf39e.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5ri4546L5a2Q,size_14,color_FFFFFF,t_70,g_se,x_16)



 #### 2、工作队列

- 一个生产者，多个消费者。
- 一个消息发送到队列时，只能被一个消费者获取。
- 多个消费者并行处理消息，提升消息处理速度。

​    注意:channel.basicQos(1)表示同一时刻只发送一条消息给消费者。

![img](https://img-blog.csdnimg.cn/9e32dab89b8140d3a7efc501039c24db.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5ri4546L5a2Q,size_16,color_FFFFFF,t_70,g_se,x_16)



#### 3、发布/订阅模式(Publish/Subcribe)

将消息发送到交换机，队列从交换机获取消息，队列需要绑定到交换机。

- 一个生产者，多个消费者。

- 每一个消费者都有自己的一个队列。

- 生产者没有将消息直接发送到队列，而是发送到交换机。

- 每一个队列都要绑定到交换机。

- 生产者发送的消息，经过交换机到达队列，实现一个消息被多个消费者获取的目的。

- 交换机类型为“fanout”。

  ​        注意：交换机本身没有存储消息的能力，消息只能存储到队列中。

![img](https://img-blog.csdnimg.cn/98aa59796b56424aaf48daac97f463e2.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5ri4546L5a2Q,size_16,color_FFFFFF,t_70,g_se,x_16)



#### 4、路由模式（Routing）

​    路由模式是发布/订阅模式的一种特殊情况。

- 路由模式的交换机类型为“direct”。
- 绑定队列到交换机时指定 key，即路由键，一个队列可以指定多个路由键。
- 生产者发送消息时指定路由键，这时，消息只会发送到绑定的key的对应队列中。

![img](https://img-blog.csdnimg.cn/d19e1cf547c34c50b10e285c0eddfe00.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5ri4546L5a2Q,size_20,color_FFFFFF,t_70,g_se,x_16)



#### 5、主题模式（Topic）

- 将路由键和某模式进行匹配。此时，队列需要绑定到一个模式上。
- 符号“#”匹配一个或多个词，“*”匹配不多不少一个词。
-  绑定队列到交换机指定key时，进行通配符模式匹配。
-  路由模式的交换机类型为“topic”。

![img](https://img-blog.csdnimg.cn/2df657907cac4998a5ae0ad337a17540.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBA5ri4546L5a2Q,size_20,color_FFFFFF,t_70,g_se,x_16)



### 五、RabbitMQ四种交换机

有4种不同的交换机类型：

- 扇形交换机：Fanout exchange
- 直连交换机：Direct exchange
- 主题交换机：Topic exchange
- 首部交换机：Headers exchange



#### 1、扇形交换机

- 扇形交换机是最基本的交换机类型，它所能做的事情非常简单———广播消息。
- 扇形交换机会把能接收到的消息全部发送给绑定在自己身上的队列。
- 因为广播不需要“思考”，所以扇形交换机处理消息的速度也是所有的交换机类型里面最快的。

![](https://img-blog.csdnimg.cn/2eaf42f3bc714ca0b544133750786b4e.png)



#### 2、直连交换机

- 直连交换机是一种带路由功能的交换机，一个队列会和一个交换机绑定，除此之外再绑定一个routing_key。

- 当消息被发送的时候，需要指定一个binding_key，这个消息被送达交换机的时候，就会被这个交换机送到指定的队列里面去。
- 同样的一个binding_key也是支持应用到多个队列中的。这样当一个交换机绑定多个队列，就会被送到对应的队列去处理。

![](https://img-blog.csdnimg.cn/51be272d06e1482db7762f09c9b5deba.png)



#### 3、主题交换机

发送到主题交换机上的消息需要携带指定规则的routing_key，主题交换机会根据这个规则将数据发送到对应的(多个)队列上。主题交换机的routing_key需要有一定的规则，交换机和队列的binding_key需要采用*.#.*.....的格式，每个部分用.分开，其中：

- *表示一个单词。
- #表示任意数量（零个或多个）单词。

![](https://img-blog.csdnimg.cn/95fe86bdef2f4b0ebb1deeb1ccdc21ad.png)

> 当一个队列的绑定键为#的时候，这个队列将会无视消息的路由键，接收所有的消息。



#### 4、首部交换机

​		定义一个Hash的数据结构，消息发送的时候，会携带一组hash数据结构的信息，当Hash的内容匹配上的时候，消息就会被写入队列。绑定交换机和队列的时候，Hash结构中要求携带一个键“x-match”，这个键的Value可以是any或者all，这代表消息携带的Hash是需要全部匹配(all)，还是仅匹配一个键(any)就可以了。相比直连交换机，首部交换机的优势是匹配的规则不被限定为字符串(string)。






参考：

https://blog.csdn.net/qq_40298351/article/details/102923178

https://www.cnblogs.com/chenhaoyu/p/14707303.html





