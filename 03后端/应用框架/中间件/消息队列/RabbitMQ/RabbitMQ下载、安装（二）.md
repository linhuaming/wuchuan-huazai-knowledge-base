## RabbitMQ下载、安装（二）

[TOC]

> 注意：下载的时候，erlang、rabbitmq两者版本要对应起来



### 一、安装erlang

**1、下载erlang**

下载地址： https://link.zhihu.com/?target=http%3A//erlang.org/download/otp_win64_23.2.exe



**2、安装erlang**

双击运行 **otp_win64_23.2.exe** ，点击下一步完成安装。

![image-20220605233459517](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220605233459517.png)



安装完成后配置环境变量 > 用户变量，如下图所示

![image-20220605234400159](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220605234400159.png)

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220605234308921.png" alt="image-20220605234308921" style="zoom: 50%;" />



在cmd输入 erl ，如图中所示，则代表erlang安装完成。

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220605234616091.png" alt="image-20220605234616091" style="zoom:50%;" />



### 二、安装RibbitMQ步骤（基于windows）

**1、下载RabbitMQ**

下载地址： 

https://github.com/rabbitmq/rabbitmq-server/releases/download/v3.8.11/rabbitmq-server-3.8.11.exe



**2、安装RabbitMQ**

双击运行 **rabbitmq-server-3.8.11.exe**，点击下一步完成安装

双击RabbitMQ Service - start 运行RabbitMQ

![](https://pic2.zhimg.com/80/v2-75dd3e2847c52420ceb36e99dec8104d_720w.jpg)



出现如下提示，则代表服务启动成功：

![](https://pic1.zhimg.com/80/v2-af6c8fe7820ebe90c220a18f6fc6acb4_720w.jpg)



发现在执行http://localhost:15672/ 提示该页无法访问，cmd进入rabbitmq安装目录sbin目录，分别执行这两句命令

```
rabbitmq-plugins enable rabbitmq_management
rabbitmqctl start_app
```



重启rabbitmq服务

![](https://img2022.cnblogs.com/blog/1879259/202202/1879259-20220217140648379-215271380.png)



访问RabbitMQ控制台，链接：http://localhost:15672 ， 输入账号  guest，密码  guest

![image-20220606003246857](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220606003246857.png)



参考：

https://zhuanlan.zhihu.com/p/349349248