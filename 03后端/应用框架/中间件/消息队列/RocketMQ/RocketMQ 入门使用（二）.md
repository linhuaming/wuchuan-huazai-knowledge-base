## RocketMQ 入门使用（二）

> 本文Rocket MQ 入门使用教程是在windows 10 环境下的

[TOC]

### 一、Rocket MQ下载、安装

#### 1、下载

地址：http://rocketmq.apache.org/release_notes/release-notes-4.3.0/ ，选择Binary进行下载。

![](https://img-blog.csdnimg.cn/3f58da7cc3e44a1f933b2d1ea0ee4899.png)



#### 2、安装

1、解压已下载的压缩包，如下图所示。

![](https://img-blog.csdnimg.cn/7609c998306d4c6382fafd18b959c1f2.png)



2、配置环境变量ROCKETMQ_HOME，如下图所示。

<img src="https://img-blog.csdnimg.cn/c3a12e32e39b4ba383cfbf08bcdd6082.png" style="zoom: 40%;" />



3、修改配置文件，如下图所示。

进入RocketMQ安装目录下的conf目录，里面有一个broker.conf配置文件。在文件末尾添加两行。

![](https://img-blog.csdnimg.cn/17708d108a2c44cfb85e9900a5519f41.png)



 ### 二、启动服务

#### 1、启动name server

在MQ安装目录的bin目录下进入CMD命令行，执行命令（或者双击**mqnamesrv.cmd**），启动nameserver，成功后会弹出提示框，不要关闭！

```shell
# 执行命令
start mqnamesrv.cmd
```



#### 2、启动broker

在bin目录下cmd进入命令行模式，执行命令，启动broker，成功后不要关闭！

```shell
start mqbroker.cmd -n 127.0.0.1:9876 -c ../conf/broker.conf autoCreateTopicEnable=true 
```



### 三、web 管理界面部署

想配置一下RocketMQ的web管理页面，发现网上的资料都是之前版本的说明，导致无法成功配置。

发现https://github.com/apache/rocketmq-externals ，这个git项目上并没有找到rocketmq-console这个目录。

原来是转移到 rocketmq-dashboard 仓库了。

![](https://img2020.cnblogs.com/blog/2561853/202112/2561853-20211206145521692-1282042143.png)



#### 1、下载rocketmq-dashboard

rocketmq-dashboard仓库：https://github.com/apache/rocketmq-dashboard



#### 2、编译打包

这里有一些坑，编译打包时可能会报错。需要把一些插件注释掉！

```xml 
<artifactId>maven-compiler-plugin</artifactId>
<artifactId>maven-checkstyle-plugin</artifactId>
<artifactId>frontend-maven-plugin</artifactId>
<artifactId>maven-antrun-plugin</artifactId>
```



注释掉以上plugin后，就可以执行下面命令了。

```shell
mvn clean package -Dmaven.test.skip=true
```



#### 3、启动web管理界面

执行命令：

```shell
java -jar rocketmq-dashboard-1.0.1-SNAPSHOT.jar
```



说明：在浏览器中输入 http://127.0.0.1:8888/#/ ，即可查看。

![](https://img-blog.csdnimg.cn/eff221691a8f42179baee80fa04d67f9.png)



### 四、入门使用

#### 1、pom 文件添加依赖

```xml
<dependency>
    <groupId>org.apache.rocketmq</groupId>
    <artifactId>rocketmq-client</artifactId>
    <version>4.4.0</version>
</dependency>
```



#### 2、消息生产者（发送消息）

**发送消息步骤：**

1. 创建消息生产者, 指定生产者所属的组名。
2. 指定Nameserver地址。
3. 启动生产者。
4. 创建消息对象，指定主题、标签和消息体。
5. 发送消息。
6. 关闭生产者。



代码如下：

```java
package com.linhuaming.rocketmq.demo.producer;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

import java.io.UnsupportedEncodingException;

/**
 * 消息生产者
 */
public class Producer {

    public static void main(String[] args) {
        // 1.创建一个生产者对象，并且指定一个生产者组
        DefaultMQProducer producer = new DefaultMQProducer("xiaolin-producer");
        // 2.指定mq服务器地址
        producer.setNamesrvAddr("127.0.0.1:9876");
        try {
            // 3.启动生产者
            producer.start();
            // 4.创建一个消息，参数分别为：主题、标签、消息体
            Message message = new Message("topic1","tag1","你好,我是消息2".getBytes("utf-8"));
            // 5.发送消息
            SendResult send = producer.send(message);
            System.out.printf("%s%n", send);
            // 6.关闭资源
            // producer.shutdown();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MQClientException e) {
            e.printStackTrace();
        } catch (MQBrokerException e) {
            e.printStackTrace();
        } catch (RemotingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
```



#### 3、消息消费者（接收消息）

**接收消息步骤：**

1. 创建消息消费者, 指定消费者所属的组名。
2. 指定Nameserver地址。
3.  指定消费者订阅的主题和标签。
4.  设置回调函数，编写处理消息的方法。
5.  启动消息消费者。

```java
package com.linhuaming.rocketmq.demo.consumer;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class Consumer {

    public static void main(String[] args) {
        // 1.创建一个拉取消息对象，并指定所属组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("xiaoxi-consumer");
        // 2.指定mq服务器地址
        consumer.setNamesrvAddr("127.0.0.1:9876");
        try {
            // 3.指定消费者订阅的主题和标签，第二个参数用于过滤条件用于指定接收什么tag，*什么都接收
            consumer.subscribe("topic1", "*");
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        // 4.注册一个消息监听器
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> msgs,
                                                            ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                // 可能有多个消息
                for (MessageExt msg : msgs) {
                    // 打印消息
                    System.out.println(new String(msg.getBody()));
                }
                // 返回发送成功
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        try {
            // 5.启动消费者
            consumer.start();
        } catch (MQClientException e) {
            e.printStackTrace();
        }
    }

}

```

