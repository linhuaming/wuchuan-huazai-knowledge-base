# Log4j 入门使用-配置文件详解

[TOC]

### 前言

Log4j log for java(java的日志) 是java主流的日志框架，提供各种类型，各种存储，各种格式，多样化的日志服务。



### 一、log4j 使用步骤

1、在pom.xml 文件中添加依赖

```xml
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```



2、配置log4j.properties

```properties
#日志的等级和要输出的形式,这里是要输出到控制台和文档里
log4j.rootLogger=DEBUG, ConsoleName ,FileNmae 

#控制台中具体要输出的样式 
#Console  
log4j.appender.ConsoleName=org.apache.log4j.ConsoleAppender  
log4j.appender.ConsoleName.layout=org.apache.log4j.PatternLayout  
log4j.appender.ConsoleName.layout.ConversionPattern=%d [%t] %-5p [%c] - %m%n

#文件中具体要输出的样式  
#File
log4j.appender.FileNmae = org.apache.log4j.FileAppender
log4j.appender.FileNmae.File = D://log4j-demo//log.log
log4j.appender.FileNmae.layout = org.apache.log4j.PatternLayout
log4j.appender.FileNmae.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n
```



3、编写测试类

```java
package com.linhuaming.log4j.demo;

import org.apache.log4j.Logger;

/**
 * Hello world!
 *
 */
public class App {
    private static Logger logger= Logger.getLogger(App.class);

    public static void main( String[] args ) {
        System.out.println( "Hello World!" );

        logger.debug("调试信息");
        logger.info("普通信息");
        logger.warn("警告信息");
        // 也可以输出异常
        logger.error("参数错误",new IllegalArgumentException());
    }
}
```

这时候控制台就会输出日志和D盘log4j-demo目录下就会出现一个日志文件og.log



### 二、Log4j日志等级

​		Log4j根据日志信息的重要程度，分OFF、FATAL、ERROR、WARN、INFO、DEBUG、ALL。当然再细分的话 还有 FATAL（严重错误），但是Log4j官方建议实际实用的话，Log4j建议只使用四个级别，优先级从高到低分别是 ERROR、WARN、INFO、DEBUG。

​		通过定义级别，可以作为应用程序中相应级别的日志信息的开关。
​		**比如在这里定义了INFO级别，则应用程序中所有DEBUG级别的日志信息将不被打印出来。（设置INFO级别，即：>=INFO 生效）**
**项目上生产环境的时候一定得把debug的日志级别重新调为warn或者更高，避免产生大量日志。**



### 三、Log4j 配置文件详解

**示例**

```properties
# 日志的等级和要输出的形式,这里是要输出到控制台和文档里
log4j.rootLogger=DEBUG, Console ,File,EFile,DailyRollingFile ,RollingFile

# 控制台中具体要输出的样式
# Console 控制台方式输出
log4j.appender.Console=org.apache.log4j.ConsoleAppender
log4j.appender.Console.layout=org.apache.log4j.PatternLayout
log4j.appender.Console.layout.ConversionPattern=%d [%t] %-5p [%c.%M()] - %m%n %l %n

# 文件中具体要输出的样式
# File 文件方式输出
log4j.appender.File = org.apache.log4j.FileAppender
log4j.appender.File.File = D://log4j-demo//log1-debug.log
log4j.appender.File.layout = org.apache.log4j.PatternLayout
log4j.appender.File.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n
log4j.appender.File.Append = false
log4j.appender.File.Threshold = DEBUG


# 文件中具体要输出的样式
# File 文件方式输出
log4j.appender.EFile = org.apache.log4j.FileAppender
log4j.appender.EFile.File = D://log4j-demo//log1-error.log
log4j.appender.EFile.layout = org.apache.log4j.PatternLayout
log4j.appender.EFile.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n
log4j.appender.EFile.Threshold = ERROR


# 每天产生一个日志文件
# DailyRollingFile
log4j.appender.DailyRollingFile = org.apache.log4j.DailyRollingFileAppender
log4j.appender.DailyRollingFile.File = D://log4j-demo//log2.log
log4j.appender.DailyRollingFile.layout = org.apache.log4j.PatternLayout
log4j.appender.DailyRollingFile.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n

# 文件大小到达指定尺寸的时候产生一个新的文件
# RollingFile
log4j.appender.RollingFile = org.apache.log4j.RollingFileAppender
log4j.appender.RollingFile.File = D://log4j-demo//log3.log
log4j.appender.RollingFile.MaxFileSize=1KB
log4j.appender.RollingFile.MaxBackupIndex=3
log4j.appender.RollingFile.layout = org.apache.log4j.PatternLayout
log4j.appender.RollingFile.layout.ConversionPattern =%d [%t] %-5p [%c] - %m%n

```

#### 1、rootLogger 配置

Log4j 根配置语法：

```properties
log4j.rootLogger = [ level ] , appenderName, appenderName, …
```

level 表示日志等级
appenderName表示输出类型（控制台 文档 ……）



#### 2、appender 输出类型配置

Log4j官方的appender给出了一下几种实现：

- org.apache.log4j.ConsoleAppender（控制台）
- org.apache.log4j.FileAppender（文件）
- org.apache.log4j.DailyRollingFileAppender（每天产生一个日志文
- org.apache.log4j.RollingFileAppender（文件大小到达指定尺寸的时候产生一个新的文件）
- org.apache.log4j.WriterAppender（将日志信息以流格式发送到任意指定的地方）



实际开发我们使用第1，第3和第4种实现；假如日志数据量不是很大，我们可以用DailyRollingFileAppender 每天产生一个日志，方便查看；假如日志数据量很大，我们一般用RollingFileAppender，固定尺寸的日志，假如超过了 就产生一个新的文件；

这里有两个新的配置项解释下：
**MaxFileSize 是日志文件的最大尺寸，根据实际需求来定 10KB 100KB也行。**
**MaxBackupIndex是日志文件的个数，假如超过了，则覆盖，主要考虑到的是硬盘的容量问题，根据实际需求来定 比如 100 500**



#### 3、layout日志信息格式

Log4j提供的layout有以下几种：

- org.apache.log4j.HTMLLayout（以HTML表格形式布局）
- org.apache.log4j.PatternLayout（可以灵活地指定布局模式）
- org.apache.log4j.SimpleLayout（包含日志信息的级别和信息字符串）
- org.apache.log4j.TTCCLayout（包含日志产生的时间、线程、类别等等信息）



**HTMLLayout**

我们把前面的实例的配置文件改下：

```properties
#Console  
log4j.appender.Console=org.apache.log4j.ConsoleAppender  
log4j.appender.Console.layout=org.apache.log4j.HTMLLayout
```

输出结果为html格式，标准的table表格格式，显示信息包括 线程 等级 类 报错信息；



**PatternLayout**

是我们以后推荐使用的，很灵活；
有个ConversionPattern属性，灵活配置输出属性：
%m 输出代码中指定的消息；
%M 输出打印该条日志的方法名；
%p 输出优先级，即DEBUG，INFO，WARN，ERROR，FATAL；
%r 输出自应用启动到输出该log信息耗费的毫秒数；
%c 输出所属的类目，通常就是所在类的全名；
%t 输出产生该日志事件的线程名；
%n 输出一个回车换行符，Windows平台为”rn”，Unix平台为”n”；
%d 输出日志时间点的日期或时间，默认格式为ISO8601，也可以在其后指定格式，比如：%d{yyyy-MM-dd HH:mm:ss,SSS}，输出类似：2002-10-18 22:10:28,921；
%l 输出日志事件的发生位置，及在代码中的行数；
注意：上面的5为了对齐级别字样。



**SimpleLayout**

```properties
#Console  
log4j.appender.Console=org.apache.log4j.ConsoleAppender  
log4j.appender.Console.layout=org.apache.log4j.SimpleLayout
```



**TTCCLayout**

```properties
#Console  
log4j.appender.Console=org.apache.log4j.ConsoleAppender  
log4j.appender.Console.layout=org.apache.log4j.SimpleLayout
```



#### 4、Threshold属性

用于指定输出等级，就是把对应的日志级别输出到指定的文件。

```properties
log4j.appender.File.Threshold = DEBUG
```

注意，其中的File 是appendName 



#### 5、Append属性

Log4j默认是不断的把日志内容追加到日志文件，这里就有个属性 Append 默认就是true；假如我们设置成false 就不追加了 直接覆盖前面的内容

```properties
log4j.appender.File.Append = false
```

注意，其中的File 是appendName 









