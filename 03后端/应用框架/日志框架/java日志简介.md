# java日志框架简介

[TOC]

### 一、java日志框架发展史简介

#### 1、介绍

​		**log4j** 最早得到广泛应用的日志工具是 log4j ，这个作者比较牛，不知道用那种的错觉也是要拜该作者所赐，log4j的作者：Ceki Gülcü ，log4j 已经差不多成为了java日志的标准了。



​		**JUL **当log4j 使用广泛的时候，Sun公司也想在日志这里分一杯羹，sun公司就在jdk1.4中增加了JUL(java.util.logging)日志接口，这样市面上就出现了两种不同的日志，如果一个项目中两个相互协作的团队使用了两个不同的日志工具，那么合并项目的时候日志问题就会很棘手。



​		**JCL  **为解决这个问题，Apache Commons Logging 开发了JCL（之前叫 Jakarta Commons Logging）将日志工具抽象成一个接口层，该接口层可以对不同日志工具进行适配换转接，JCL只提供了log接口，具体的实现则在运行时动态寻找。

​		因此，很多项目采用 JCL + log4j 这种搭配，不过当程序规模越来越庞大之后，JCL的动态绑定并不一定总能成功，因此急需一种更优秀的日志工具。



​		**SLF4J **此时log4j的作者Ceki Gülcü又站了出来，开发出了SLF4J日志工具，SLF4J工具采用在程序部署时静态绑定指定日志工具的方法来解决这一问题。

​		SLF4J通JCL一样，也只是提供log接口，具体的实现是在打包应用程序时所放入的绑定名字为（slf4j-XXX-version.jar）的转换器来决定，其中xxx可以是log4j、jcl、jul等日志类，你只需编写SLF4J接口的日志，放入绑定器来转换成其他格式的日志。

​		如果现在使用的是非SLF4J日志工具，可以桥接器（名字为XXX-over-slf4j.jar)把当前日志组件转换到SLF4J，SLF4J又根据绑定器把日志交给具体的日志实现工具。



​		**logback **Ceki Gülcü 还开发了一个slf4j的“亲儿子”---logback，logback是一个比slf4j更加优秀的日志工具。



​		**log4j2 **Ceki Gülcü还升级了下log4j --- log4j2代替了log4j。

​		

 到此主流的日志工具发展史基本介绍完毕了，发展过程就是：**log4j** >>  **JUL**  >>   **JCL** >>  **SLF4J** >>  **logback**  >>   **log4j2**。



#### 2、日志框架介绍以及优缺点

- **log4j**

[log4j ](https://link.jianshu.com/?t=https://logging.apache.org/log4j/extras/)是apache的一个开源项目，创始人Ceki Gulcu。Log4j应该说是Java领域资格最老，应用最广的日志工具。从诞生之日到现在一直广受业界欢迎。

Log4j是高度可配置的，并可通过在运行时的外部文件配置。它根据记录的优先级别，并提供机制，以指示记录信息到许多的目的地，诸如：数据库，文件，控制台，UNIX系统日志等。

Log4j中有三个主要组成部分：loggers:负责捕获记录信息；appenders :负责发布日志信息，以不同的首选目的地；layouts:负责格式化不同风格的日志信息。



- **JUL (java.util.logging)**

JDK1.4开始，通过[java.util.logging](https://link.jianshu.com/?t=https://docs.oracle.com/javase/7/docs/api/java/util/logging/package-summary.html) 提供日志功能。它能满足基本的日志需要，但是功能没有Log4j强大，而且使用范围也没有Log4j广泛。



- **JCL( commons Loggin)**

[common-logging](https://link.jianshu.com?t=https://commons.apache.org/proper/commons-logging/)是apache的一个开源项目。也称Jakarta Commons Logging，缩写JCL。common-logging的功能是提供日志功能的API接口，本身并不提供日志的具体实现（当然，common-logging内部有一个Simple logger的简单实现，但是功能很弱，直接忽略），而是在运行时动态的绑定日志实现组件来工作（如log4j、java.util.loggin）。



- **SLF4J**

[SLF4J](https://link.jianshu.com?t=https://www.slf4j.org/)全称为Simple Logging Facade for Java，而作者又是Ceki Gulcu！这位大神写了Log4j、Logback和slf4j，专注日志组件开发五百年，一直只能超越自己。

类似于Common-Logging，slf4j是对不同日志框架提供的一个API封装，可以在部署的时候不修改任何配置即可接入一种日志实现方案。但是，slf4j在编译时静态绑定真正的Log库。使用SLF4J时，如果你需要使用某一种日志实现，那么你必须选择正确的SLF4J的jar包的集合（各种桥接包）。

![](https://upload-images.jianshu.io/upload_images/3101171-f1d4d0c74823f0aa.png?imageMogr2/auto-orient/strip|imageView2/2/format/webp)

**common-logging 对比 slf4j**

slf4j库类似于Apache Common-Logging。但是，他在编译时静态绑定真正的日志库。这点似乎很麻烦，其实也不过是导入桥接jar包而已。

slf4j一大亮点是提供了更方便的日志记录方式：不需要使用logger.isDebugEnabled()来解决日志因为字符拼接产生的性能问题。slf4j的方式是使用{}作为字符串替换符，形式如下：logger.debug("id: {},name: {} ",id,name)



- **logback**

[Logback](https://link.jianshu.com?t=http://logback.qos.ch/)   是由log4j创始人Ceki Gulcu设计的又一个开源日记组件，目标是替代log4j。logback当前分成三个模块：logback-core,logback- classic和logback-access。logback-core是其它两个模块的基础模块。

logback-classic是log4j的一个 改良版本。此外logback-classic完整实现SLF4J API使你可以很方便地更换成其它日记系统如log4j或JUL。

logback-access访问模块与Servlet容器集成提供通过Http来访问日记的功能。



- **log4j2**

 [log4j2](https://link.jianshu.com?t=https://logging.apache.org/log4j/2.x/) 是log4j 的升级版 ，兼容log4j，相对的升级了：
 api分离：将接口实现分离，性能相对于log4j1提升了18倍，log4j2支持种日志的api：Log4j 1.2、 SLF4J、Commons Logging 、 java.util.logging (JUL) APIs；可以自动读取配置等等优点。



### 二、日志框架转换

**slf4j** 采用接口和实现分离的方式，它有个接口层，用户记录日志的时候只需安装 slf4j 的格式记录日志即可。

而系统真正写入的日志格式是依赖于实现层配置的内容的，如果你想真正记录 log4j2 格式的日志，则在实现层放入slf4j-log4j2.jar转换文件，这样就可以把用户按照 slf4j 格式写的日志在项目中转换成log4j2 格式的日志了。

同理，如果用户当前的项目是jcl格式的日志，用户想按照slf4j格式记录日志，那么只需在放入一个桥接器：jcl-over-slf4j.jar桥接器，而原本这个项目是通过jcl输出日志的，现在却会因为“jcl-over-slf4j.jar”桥接到slf4j中，而slf4j又会根据实现层绑定的具体日志实现工具实现日志输出。



![](https://upload-images.jianshu.io/upload_images/4290189-f160c20436b66168.png?imageMogr2/auto-orient/strip|imageView2/2/w/538/format/webp)



上层和下层都可以相互转换，就会出现一个问题，如果上层和下层使用了一个相互转换的工具，那么日志的实现就会被踢来踢去，如下图所示 ，红色就是代表两个会相互冲突。

![](https://upload-images.jianshu.io/upload_images/4290189-5995b316e433df7d.png?imageMogr2/auto-orient/strip|imageView2/2/w/399/format/webp)





对于其他日志格式的文件同样可以适用以上两张转换关系，整个的转换关系如下所示，红色的线表示会相互踢皮球的。

![](https://upload-images.jianshu.io/upload_images/4290189-1bda702eb1657767.png?imageMogr2/auto-orient/strip|imageView2/2/w/1200/format/webp)



![](https://upload-images.jianshu.io/upload_images/3101171-f1d4d0c74823f0aa.png?imageMogr2/auto-orient/strip|imageView2/2/format/webp)

### 三、日志框架选型方案

#### 1、JCL + log4j

#### 2、slf4j + logback







参考：

https://www.jianshu.com/p/ab727c90c069

https://www.cnblogs.com/mibloom/p/9871603.html

https://www.cnblogs.com/chenhongliang/p/5312517.html



























