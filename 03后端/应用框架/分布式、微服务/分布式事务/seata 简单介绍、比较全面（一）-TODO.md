## seata 简单介绍、比较全面（一）

[TOC]

### 前言

本文以一个用户下单购买商品的系统为例，介绍开源框架Seata的原理和使用，下单该系统涉及三部分服务：

1. 仓储服务：对给定的商品扣除仓储数量；
2. 订单服务：根据采购需求创建订单；
3. 帐户服务：从用户帐户中扣除余额；



分布式事务的主要作用是保证微服务情况下用户下单过程中数据的一致性。这里的一致性可以这样理解：不会出现用户余额扣除成功，但是仓储和订单相关操作失败的场景，三者要么同时成功，要么同时失败。



### 一、什么是seata

#### 1、介绍

Seata 是一款开源的[分布式](https://so.csdn.net/so/search?q=分布式&spm=1001.2101.3001.7020)事务解决方案，致力于提供高性能和简单易用的分布式事务服务。Seata 将为用户提供了 AT、TCC、SAGA 和 XA 事务模式，为用户打造一站式的分布式解决方案。



#### 2、 seata 的历史

我们简单了解一下相关的发展史。阿里巴巴作为国内领先的互联网公司，在微服务的实践，分布式事务问题的处理都是比较早的，已经具备了很强沉淀积累。

- 2014 - 阿里中间件团队发布txc（taobao transaction constructor）在阿里内部提供分布式事务服务；

- 2016 - txc经过改造和升级，变成了gts（global transaction service）在阿里云作为服务对外开放，也成为当时唯一一款对外的服务；

- 2019 - 阿里经过txc和gts的技术积累，决定开源（Apache开源协议）。并且，在github上发起了一个项目叫做fescar（fast easy commit and rollback）开始拥有了社区群体；

- 2019 - fescar被重命名为了seata（simple extensiable autonomous transaction architecture），项目迁移到了新的github地址。



#### 3、分布式事务场景

在微服务框架中，仓储/订单/账户服务部署在不同的服务器上，使用不同的数据库实例，与单机模式完全不同。单机模式中的事务通常要求事务涉及的数据源为同一个，并且事务涉及的数据库操作在同一个数据库链接中，分布式情况下显然不满足条件。

所以在分布式场景如何保证数据的一致性呢？这就是Seata需要解决的问题了。

![](https://pic2.zhimg.com/80/v2-aa0a8fdbc35b697bf61027ad2f3d2bb1_720w.jpg)

### 二、分布式事务相关概念

**事务：** 一个程序执行单元，是用户定义的一组操作序列，需要满足ACID属性。

**本地事务：**事务由本地资源管理器管理。

**分布式事务：**事务的操作位于不同的服务器节点。

**分支事务：**在分布式事务中，由资源管理器，管理的本地事务。

**全局事务：**一次性操作多个资源管理器完成的事务，由一组分支事务组成。



### 三、seata 的三大组件

seata框架包含三大组件：

- 事务管理器（Transaction Manager，TM）：定义全局事务，开启全局事务、提交全局事务或回滚全局事务。
- 事务协调器（Transaction Coordinator，TC）：维护全局事务和分支事务的状态，进行全局事务提交或全局事务回滚。
- 资源管理器（Resource Manager，RM）：管理分支事务中的资源，向事务管理器注册分支事务和并报告分支事务的状态，负责分支事务提交或回滚。



三个组件的相互协作，TC 以 Server 形式独立部署，TM和RM集成在应用中启动，其整体交互如下：

![](https://pic4.zhimg.com/80/v2-016b53f1ca0812e5c92191c17e192213_720w.jpg)



三大组件的流程，如下图所示：

![img](https://imgconvert.csdnimg.cn/aHR0cDovL21lZGlhLnRlYW1zaHViLmNvbS8xMDAwMC90bS8yMDIwLzA0LzE2LzkyMWYyNWE0LTBiMWYtNDg5Ny04NGRiLTQ0OGVlNTc4ZjI0MS9mM2EwNTJkMi00MmNkLTQ5OTMtOGUxZi1mZDkyOTQwOTFlMGIucG5n?x-oss-process=image/format,png)

分布式事务解决流程：

- 1.TM向TC注册一个全局事务，TC返回全局事务id，XID。
- 2.XID通过微服务调用链传播。
- 3.RM将本地事务注册为XID到TC的相应全局事务的一个分支事务。
- 4.TM通知TC提交或回滚XID所对应的全局事务
- 5.TC通知XID全局事务下的分支事务提交或回滚



### 四、seata 支持的四种工作模式

#### 1、AT（Auto Transaction） 模式

#### 2、 TCC 模式

#### 3、Saga 模式

#### 4、XA模式



参考：

https://zhuanlan.zhihu.com/p/436720146

https://zhuanlan.zhihu.com/p/333286547