# spring security oauth2.0 入门使用（简单版）

[TOC]

### 一、使用IDEA搭建spring boot项目

使用IDEA，搭建好spring boot项目，至于怎么搭建spring boot ，我这里就不介绍了，百度搜一下就行了。

目录结构如下：

![image-20211121144412447](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121144412447.png)



### 二、使用步骤

#### 1、pom文件中引入依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.9</version>
    </parent>

    <groupId>org.linhuaming</groupId>
    <artifactId>spring-security-oauth2-demo</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <security.oauth.version>2.1.8.RELEASE</security.oauth.version>
    </properties>

    <dependencies>
        <!-- web -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <!-- security -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-security</artifactId>
        </dependency>

        <!-- oauth2 -->
        <dependency>
            <groupId>org.springframework.security.oauth.boot</groupId>
            <artifactId>spring-security-oauth2-autoconfigure</artifactId>
            <version>${security.oauth.version}</version>
        </dependency>

        <!-- redis -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
    </dependencies>

</project>
```



#### 2、修改配置文件application.yml

```yml
server:
  port: 8082  # 修改端口
spring:
  # redis配置信息
  redis:
    host: 127.0.0.1
    database: 0
    port: 51478
    password: DFNCrT$JwvxuiYXP#fz2bhpz!Dol*JVcOE@#rf90Tu(lsP&l6$RYITikr*RXG4AoCTtjbX$iJfIdiBI#HqxNN%1TJiqz8ULy8eD
```



#### 3、添加授权服务配置类、资源服务配置类

说明：

OAuth2ServerConfig 里面有两个内部类，分别是AuthorizationServerConfiguration（授权服务配置类）、ResourceServerConfiguration（资源服务配置类）

因为授权服务配置类、资源服务配置类是oauth的核心，所以我就把这两个类写在OAuth2ServerConfig类里面。



```java
package org.linhuaming.oauth2.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

@Configuration
public class OAuth2ServerConfig {

    private static final String DEMO_RESOURCE_ID = "order";

    /**
     * 资源服务器类
     */
    @Configuration
    @EnableResourceServer
    protected static class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

//        @Override
//        public void configure(ResourceServerSecurityConfigurer resources) {
//            resources.resourceId(DEMO_RESOURCE_ID).stateless(true);
//        }

        @Override
        public void configure(HttpSecurity http) throws Exception {
            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED);
            http.authorizeRequests().anyRequest().authenticated();
        }

    }

    /**
     * 授权服务器类
     */
    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

        @Autowired
        AuthenticationManager authenticationManager;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Autowired
        private UserDetailsService userService;

        @Override
        public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
            // 允许表单认证
            oauthServer.allowFormAuthenticationForClients()
                     .checkTokenAccess("permitAll()");
        }

        /**
         * 使用密码模式需要配置
         * @param endpoints
         * @throws Exception
         */
        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.authenticationManager(authenticationManager)
                    .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
                    .userDetailsService(userService);
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
                    // 支持授权码模式,密码模式,客户端模式
                    .withClient("client_3")
                    //.resourceIds(DEMO_RESOURCE_ID)
                    .authorizedGrantTypes("authorization_code","password","client_credentials")
                    .scopes("all")
                    .secret(passwordEncoder.encode("123456"))
                    .redirectUris("https://www.baidu.com/callback");
        }
    }

}
```



#### 4、添加spring security 配置类

```java
package org.linhuaming.oauth2.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    /**
     * 编码方式
     * @return
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 这一步的配置是必不可少的，否则会自动配置一个AuthenticationManager,覆盖掉内存中的用户
     * @return
     * @throws Exception
     */
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        AuthenticationManager manager = super.authenticationManagerBean();
        return manager;
    }

    /**
     * 在内存中加载用户信息
     * @return
     */
    @Bean
    @Override
    protected UserDetailsService userDetailsService(){
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User.withUsername("user_1").password(passwordEncoder().encode("123456"))
                .authorities("USER").build());
        manager.createUser(User.withUsername("user_2").password(passwordEncoder().encode("123456"))
                .authorities("USER").build());
        return manager;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .anyRequest().authenticated()
                .and()
                .httpBasic()
                .and()
                .csrf()
                .disable();
    }

}
```



#### 5、TestEndpoints测试类

```java
package org.linhuaming.oauth2.demo.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestEndpoints {

    @GetMapping("/product/{id}")
    public String getProduct(@PathVariable String id) {
        //for debug
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "product id : " + id;
    }

    @GetMapping("/order/{id}")
    public String getOrder(@PathVariable String id) {
        //for debug
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return "order id : " + id;
    }

}
```



### 三、测试-客户端4种授权模式

#### 1、授权码模式

打开浏览器访问下面的链接：

http://localhost:8082/oauth/authorize?client_id=client_3&response_type=code&redirect_uri=https://www.baidu.com/callback&scope=all

此时会要求认证用户名和密码，这里的用户名和密码必须是是UserDetailsService里面的信息。

![image-20211121224942492](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121224942492.png)



输入用户名和密码后，会跳转到这个授权页面。

![image-20211121225305572](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121225305572.png)



选择后，点击Authorize按钮，然后自动跳转到这个页面，会发现链接中有个code参数，这个code参数就是，认证服务器返回的授权码。

![image-20211121230054189](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121230054189.png)



打开postman，把上一步获取到的code，放到下面的请求路径/oauth/token。请求后，会得到认证服务器的token信息

请求连接：http://localhost:8082/oauth/token?code=Ckh0m1&grant_type=authorization_code&redirect_uri=https://www.baidu.com/callback&scope=all&client_id=client_3&client_secret=123456

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121230450295.png" alt="image-20211121230450295" style="zoom:50%;" />



#### 2、简化模式-TODO



#### 3、密码模式

在postman输入下面的链接，密码模式grant_type参数必须为password

请求链接： http://localhost:8082/oauth/token?username=user_1&password=123456&grant_type=password&scope=all&client_id=client_3&client_secret=123456

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211122003738930.png" alt="image-20211122003738930" style="zoom:50%;" />



#### 4、客户端模式

在postman输入下面的链接，密码模式grant_type参数必须为client_credentials

请求链接：http://localhost:8082/oauth/token?grant_type=client_credentials&scope=all&client_id=client_3&client_secret=123456

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211122004440949.png" alt="image-20211122004440949" style="zoom:50%;" />



### 四、通过access_token参数访问资源



把上面获取到的access_token参数，放在下面请求链接中，去访问资源路径，测试下，发现可以正常访问。
请求链接：http://localhost:8082/product/123456?access_token=71ac9bce-8166-4a26-a121-2feb9eb515a1

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121231132029.png" alt="image-20211121231132029" style="zoom:50%;" />



如果没有携带access_token参数，则会提示没有认证，访问不了

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20211121231333491.png" alt="image-20211121231333491" style="zoom:50%;" />



