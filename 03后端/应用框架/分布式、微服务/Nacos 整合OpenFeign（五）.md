## Nacos 整合OpenFeign（五）

[TOC]

### 前言

​	服务间调用通过RestTemplate+Ribbon的方式进行调用的，实际开发当中服务之间的调用大多采用的是OpenFeign，它整合了Ribbon和Hystrix，采用声明式方式定义服务客户端，为服务调用提供了更加优雅编程的方式。



### 一、简介

#### 1、Feign是什么

​	Feign是一个声明式的Web Service客户端。它的出现使开发Web Service客户端变得很简单。使用Feign只需要创建一个接口加上对应的注解，比如：FeignClient注解。Feign有可插拔的注解，包括Feign注解和JAX-RS注解。Feign也支持编码器和解码器，Spring Cloud Open Feign对Feign进行增强支持Spring MVC注解，可以像Spring Web一样使用HttpMessageConverters等。

​	Feign是一种声明式、模板化的HTTP客户端。在Spring Cloud中使用Feign，可以做到使用HTTP请求访问远程服务，就像调用本地方法一样的，开发者完全感知不到这是在调用远程方法，更感知不到在访问HTTP请求。



#### 2、OpenFeign 功能

​	在Feign的实现下，我们只需创建一个接口并使用注解的方式来配置它（以前是 Dao 接口上面标注 @Mapper 注解，现在是一个微服务接口上面标注一个 @Feign 注解即可），即可完成对服务提供方的接口绑定，简化了使用 SpringCloud Ribbon 时自动封装服务调用客户端的开发量



#### 3、Feign和OpenFeign的区别

![](https://img-blog.csdnimg.cn/20200327203841619.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1dvb19ob21l,size_16,color_FFFFFF,t_70)



### 二、特性

1. 可插拔的注解支持，包括Feign注解和JAX-RS注解。
2. 支持可插拔的HTTP编码器和解码器（Gson，Jackson，Sax，JAXB，JAX-RS，SOAP）。
3. 支持Hystrix和它的Fallback。
4. 支持Ribbon的负载均衡。
5. 支持HTTP请求和响应的压缩。
6. 灵活的配置：基于 name 粒度进行配置。
7. 支持多种客户端：JDK URLConnection、apache httpclient、okhttp，ribbon）。
8. 支持日志
9. 支持错误重试
10. url支持占位符
11. 可以不依赖注册中心独立运行



### 三、使用步骤

#### 1、建立子工程common-api-openfeign

在父工程spring-cloud-alibaba-demo 下，建立common-api-openfeign工程，结构目录如下：

<img src="../../../img/image-20211020230522286.png" alt="image-20211020230522286" style="zoom:50%;" />

这篇重点关注common-api-openfeign模块就行。



**pom.xml 配置文件：**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>spring-cloud-alibaba-demo</artifactId>
        <groupId>org.linhuaming</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>common-api-openfeign</artifactId>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-openfeign</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

    </dependencies>

</project>
```



**TestFeignClient 接口：**

```java
package com.linhuaming;

import com.linhuaming.impl.TestFeignHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;


@Component
@FeignClient(value = "nacos-provider",fallback = TestFeignHystrix.class)
public interface TestFeignClient {

    // 这里对应provider原接口地址，必须一样！
    @GetMapping("/provider/hello")
    String hello();

}
```



**TestFeignHystrix 实现类（处理服务熔断和降级）：**

```java
package com.linhuaming.impl;

import com.linhuaming.TestFeignClient;
import org.springframework.stereotype.Component;

@Component
public class TestFeignHystrix implements TestFeignClient {

    @Override
    public String hello() {
        return "服务器繁忙，请稍后访问";
    }

}
```



**在nacos-consumer模块开启远程调用功能：**

<img src="../../../img/image-20211020232309462.png" alt="image-20211020232309462" style="zoom:50%;" />



接着引入common-api-openfeign坐标，这步很重要，不然下一步就会出错：

<img src="../../../img/image-20211020232615257.png" alt="image-20211020232615257" style="zoom:50%;" />



在naocs-consumer模块里的ConsumerController中注入刚刚编写的TestFeignClient接口：

<img src="../../../img/image-20211020232731485.png" alt="image-20211020232731485" style="zoom:50%;" />

在浏览器调用，到这思路就很清晰了，消费者通过feign去远程调用服务提供者，由它来实现负载均衡和服务熔断与降级。

用到了两个关键注解

- @EnableFeignClients
- @FeignClient



#### 2、开始测试

1. **开启nacos服务**
2. **启动nacos-provider和nacos-consumer服务**
3. **调用消费者接口**

<img src="../../../img/image-20211020233836585.png" alt="image-20211020233836585" style="zoom:50%;" />

### 四、OpenFeign之请求传参方式

#### 1、 url 方式

**服务提供方：**

```java
@GetMapping("/demo1")
public String demo1(@RequestParam("id") Integer id, @RequestParam("name") String name){
    log.info("接收到的id为 {}， 接受的name为 {}", id, name);
    return "products API demo1";
}

```

 

**openFeign 接口方：**

```java
@GetMapping("/demo1")
String demo1(@RequestParam("id") Integer id, @RequestParam("name") String name);

```



#### 2、RESTFul风格的路径传参  ，url/id/name

**服务提供方：**

```java
@GetMapping("/demo2/{id}/{name}")
public String demo2(@PathVariable("id") Integer id, @PathVariable("name") String name){
    log.info("接收到的id为 {}， 接受的name为 {}", id, name);
    return "products API demo2";
}

```



**openFeign 接口方：**

```java
@GetMapping("/demo2/{id}/{name}")
String demo2(@PathVariable("id") Integer id, @PathVariable("name") String name);
```



#### 3、对象的传参

**服务提供方：**

```java
@PostMapping("/demo3")
public String demo3(@RequestBody ProductEntity product){
    log.info("传参的product信息 {}", product);
    return "products API demo3";
}

```



**openFeign 接口方：**

```java
@PostMapping("/demo3")
String demo3(@RequestBody UserEntity user);

```



#### 4、对象的传参（文件方式）

**服务提供方：**

```java
@PostMapping("/demo4")
public String demo4(@RequestPart UserEntity user){
    log.info("传参的product信息 {}", user);
    return "products API demo4";
}

```



**openFeign 接口方：**

```java
@PostMapping("/demo4")
String demo4(@RequestPart UserEntity user);

```



#### 5、数组的传参

**服务提供方：**

```java
@GetMapping("/demo5")
public String demo5(@RequestParam("ids") String[] ids){
    for (String id : ids) {
        log.info("id {}", id);
    }
    return "product API demo5";
}

```



**openFeign 接口方：**

```java
@GetMapping("/demo5")
String demo5(@RequestParam("ids") String[] ids);

```

#### 6、集合的传参

**服务提供方：**

```java
@GetMapping("/demo6")
public String demo6(ConllectionVO conllectionVO){
    conllectionVO.getList().forEach(id -> log.info("id {}", id));
    return "product API demo6";
}

```



**openFeign 接口方：**

```java
@GetMapping("/demo6")
String demo6(@RequestParam("list") List<String> list);

```

**说明：**

**list集合其实和数组的数据传递并没有什么太大的区别。只能通过querystring的方式传递值 ids=xxx&ids=xxx&ids=xxx**
**feignclient是一个伪HttpClient，并不能自动的识别参数传递，所以需要加注解**



### 五、OpenFeign日志打印功能

// TODO

###  六、OpenFeign超时控制

// TODO

