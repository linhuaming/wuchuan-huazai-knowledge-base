## Spring Cloud Alibaba 简单介绍

[TOC]

### 一、介绍

​	Spring Cloud Alibaba 致力于提供微服务开发的一站式解决方案。该项目包含开发分布式应用微服务的必需组件，方便开发者通过 Spring Cloud 编程模型轻松使用这些组件来开发分布式应用服务。

​	依托 Spring Cloud Alibaba，您只需要添加一些注解和少量配置，就可以将 Spring Cloud 应用接入阿里微服务解决方案，通过阿里中间件来迅速搭建分布式应用系统。



​	**相关的组件：**

​		**1、Sentinel**

​			把流量作为切入点，从流量控制、熔断降级、系统负载保护等多个维度保护服务的稳定性。

​		**2、Nacos**

​			一个更易于构建云原生应用的动态服务发现、配置管理和服务管理平台。

​		**3、RocketMQ**

​			一款开源的分布式消息系统，基于高可用分布式集群技术，提供低延时的、高可靠的消息发布与订阅		服务。

​		**4、Dubbo**

​			Apache Dubbo™ 是一款高性能 Java RPC 框架。

​		**5、Seata**

​			阿里巴巴开源产品，一个易于使用的高性能微服务分布式事务解决方案。

​		**6、Alibaba Cloud ACM**

​			一款在分布式架构环境中对应用配置进行集中管理和推送的应用配置中心产品。

​		**7、Alibaba Cloud OSS**

​			阿里云对象存储服务（Object Storage Service，简称 OSS），是阿里云提供的海量、安全、低成本、		高可靠的云存储服务。您可以在任何应用、任何时间、任何地点存储和访问任意类型的数据。

​		**8、Alibaba Cloud SchedulerX**

​			 阿里中间件团队开发的一款分布式任务调度产品，提供秒级、精准、高可靠、高可用的定时（基于 		Cron 表达式）任务调度服务。

​		**9、Alibaba Cloud SMS**

​		 覆盖全球的短信服务，友好、高效、智能的互联化通讯能力，帮助企业迅速搭建客户触达通道。



​	**spring cloud alibaba 提供的组件跟Spring Cloud官方提供的组件的对应关系：**

```java
	- Nacos = Eureka/Consule + Config + Admin
	- Sentinel = Hystrix + Dashboard + Turbine
	- Dubbo = Ribbon + Feign
	- RocketMQ = RabbitMQ
	- Schedulerx = Quartz
```



### 二、分布式系统包含哪些内容

​	主要包含：分布式配置、服务注册与发现、服务熔断、服务调用、服务路由、、分布式消息、负载均衡、分布式事务

![img](../../../img/20211013221401.png)





