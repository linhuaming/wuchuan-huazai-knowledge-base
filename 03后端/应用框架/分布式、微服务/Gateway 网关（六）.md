## Geteway 网关（六）

[TOC]



### 前言

这章主要记录一下nacos整合gateway网关的过程吧，网关在微服务架构中还是挺重要的！



### 一、简介

​	Spring Cloud GateWay是Spring Cloud的一个全新项目，目标是取代Netflix Zuul，它基于 Spring5.0+SpringBoot2.0+WebFlux(基于高性能的Reactor模式响应式通信框架Netty，异步非阻塞模型)等技术开发，性能高于Zuul，官方测试，GateWay是Zuul的1.6倍，旨在为微服务架构提供一种简单有效的统一的API路由管理方式。



**网关的作用：**

​	Spring Cloud GateWay不仅提供统一的路由方式(反向代理)并且基于 Filter(定义过滤器对请求过滤， 完成一些功能) 链的方式提供了网关基本的功能，例如:鉴权、流量控制、熔断、路径重写、日志监控 等。



**网关在微服务架构中的位置：**

<img src="../../../img/2021102123530001.png" alt="img" style="zoom: 80%;" />

**核心概念：**

Zuul1.x 阻塞式IO 2.x 基于Netty；
Spring Cloud GateWay天生就是异步非阻塞的，基于Reactor模型；
一个请求—>网关根据一定的条件匹配—匹配成功之后可以将请求转发到指定的服务地址；

而在这个过程中，我们可以进行一些比较具体的控制(限流、日志、黑白名单)

- 路由(route): 

​	网关最基础的部分，也是网关比较基础的工作单元。路由由一个ID、一个目标 URL(最终路由到的地址)、一系列的断言(匹配条件判断)和Filter过滤器(精细化控制)组成。如果断言为true，则匹配该路由。

- 断言(predicates):

​	参考了Java8中的断言java.util.function.Predicate，开发人员可以匹配Http 请求中的所有内容(包括请求头、请求参数等)(类似于nginx中的location匹配一样)，如果断 言与请求相匹配则路由。

- 过滤器(filter):

​	一个标准的Spring webFilter，使用过滤器，可以在请求之前或者之后执行业务 逻辑。
下面是一张官网的图：

<img src="../../../img/2021102123530002.png" alt="在这里插入图片描述" style="zoom:80%;" />



### 二、使用步骤

父项目结构图：

<img src="../../../img/2021102123530003.png" alt="image-20211022000419525" style="zoom:50%;" />



#### 1、建立server-gateway 子工程

在父工程spring-cloud-alibaba-demo 下，建立server-gateway工程，结构目录如下：

<img src="../../../img/2021102123530004.png" alt="image-20211022000652949" style="zoom:50%;" />

​	Spring cloud gateway是基于webflux的，如果非要web支持的话需要导入spring-boot-starter-webflux而不是spring-boot-start-web。所以这里不需要依赖父工程，或者注释、去掉父工程的web依赖，在需要的子模块单独添加。



**pom.xml 配置文件：**

注意我这里没有引入父工程，而是springboot父启动依赖，也不要引入web依赖而是webflux

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    
    <!--<parent>
        <artifactId>spring-cloud-alibaba-demo</artifactId>
        <groupId>org.linhuaming</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>-->

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.4.RELEASE</version>
    </parent>

    <modelVersion>4.0.0</modelVersion>

    <artifactId>server-gateway</artifactId>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-webflux</artifactId>
        </dependency>

        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-gateway</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-commons</artifactId>
            <version>2.0.4.RELEASE</version>
            <scope>compile</scope>
        </dependency>

    </dependencies>

</project>
```



**bootstrap.yml 配置文件：**

```yaml
server:
  port: 8040
spring:
  application:
    name: server-gateway
  cloud:
    nacos:
      discovery:
        server-addr: 127.0.0.1:8848
    gateway:
      discovery:
        locator:
          #开启服务发现
          enabled: true
          #服务名支持大小写
          lower-case-service-id: true
      routes:
        - id: nacos-gateway-provider
          uri: lb://nacos-provider
          #uri: http://localhost:8070
          predicates:
            - Path=/provider/**
          filters:
            - StripPrefix=1
```



**GateWayApplication启动类：**

```java
package com.linhuaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class GateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class,args);
    }

}
```



启动测试：

将之前写好的provider和gateway启动向nacos注册，如下图所示：

![image-20211022001448678](../../../img/2021102123530005.png)



接着访问网关：

- 通过服务名访问也是可以的；

  http://localhost:8040/nacos-provider/provider/hello

- 通过网关断言方式

  http://localhost:8040/provider/provider/hello

  

最后都能访问成功！

<img src="../../../img/2021102123530006.png" alt="image-20211022001658330" style="zoom:50%;" />

###  三、GateWay路由规则详解

​	Spring Cloud GateWay帮我们内置了很多Predicates功能，实现了各种路由匹配规则(通过 Header、请求参数等作为条件)匹配到对应的路由。

![在这里插入图片描述](../../../img/2021102223560001.png)





#### 1、时间点匹配

```yml
 spring:
  cloud:
    gateway:
      routes:
       - id: after_route
         uri: https://example.org
         predicates:
          - After=2021-10-23T00:30:00+08:00[Asia/Shanghai]
```

说明： 只能在2021-10-23  00:30 后的时间才可以路由，Spring Cloud Gateway的Before断言采用的ZonedDateTime进行匹配时间，这里要注意存在时区的问题，需要配置[Asia/Shanghai]作为中国时区



#### 2、时间点前匹配

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: before_route
        uri: https://example.org
        predicates:
        - Before=2021-10-30T00:00:00+08:00[Asia/Shanghai]
```



#### 3、时间区间匹配

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: between_route
        uri: https://example.org
        predicates:
        - Between=2017-01-20T17:42:47.789-07:00[America/Denver], 2017-01-21T17:42:47.789-07:00[America/Denver]

```



#### 4、指定Cookie正则匹配指定值

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: cookie_route
        uri: https://example.org
        predicates:
        - Cookie=chocolate, ch.p
```



#### 5、指定Header正则匹配指定值

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: header_route
        uri: https://example.org
        predicates:
        - Header=requestId, \d+
```

说明：  表示请求头中一定要有 requestId 参数并且值是数字类型的，才会进行转发，否则不会进行转发。也可以指定参数和参数的值。



#### 6、请求Host匹配指定值

```yml
spring:
  cloud:
    gate7\way:
      routes:
      - id: host_route
        uri: https://example.org
        predicates:
        - Host=**.somehost.org,**.anotherhost.org
```



#### 7、请求Methods 匹配指定请求方式

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: method_route
        uri: https://example.org
        predicates:
        - Method=GET,POST
```

说明：  这个配置表示只给 GET、POST 请求进行路由。



#### 8、请求路径正则匹配

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: path_route
        uri: https://example.org
        predicates:
        - Path=/get
```

说明：  表示路径满足 get这个规则，都会被进行转发到  https://example.org/get



#### 9、请求包含某参数

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: query_route
        uri: https://example.org
        predicates:
        - Query=username
```

说明：  表示请求中一定要有 name 参数才会进行转发，否则不会进行转发。



#### 10、请求包含某参数并且参数值匹配正则表达式

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: query_route
        uri: https://example.org
        predicates:
        - Query=username,linhm.*
```



#### 11、远程地址匹配

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: remoteaddr_route
        uri: https://example.org
        predicates:
        - RemoteAddr=192.168.1.1/24
```

说明： 客户端ip只能是 192.168.1.1/24 的才可以路由。



#### 12、权重匹配

```yml
spring:
  cloud:
    gateway:
      routes:
      - id: weight_high
        uri: https://weighthigh.org
        predicates:
        - Weight=group1, 8
      - id: weight_low
        uri: https://weightlow.org
        predicates:
        - Weight=group1, 2
```

说明： 这条路由规则将把80%的流量转发到weighthigh.org，并将20%的流量转发到weighlow.org



### 四、GateWay过滤器

从过滤器生命周期(影响时机点)的⻆度来说，主要有两个pre和post:

- pre： 这种过滤器在请求被路由之前调用。我们可利用这种过滤器实现身份验证、在集群中 选择 请求的微服务、记录调试信息等。

- post： 这种过滤器在路由到微服务以后执行。这种过滤器可用来为响应添加标准的 HTTP Header、收集统计信息和指标、将响应从微服务发送给客户端等

  

从过滤器类型的⻆度，Spring Cloud GateWay的过滤器分为GateWayFilter和GlobalFilter两种

- GateWayFilter： 应用到单个路由路由上
- GlobalFilter： 应用到所有的路由上



​	过滤器方式比较多，详细可以参考官网：
​	https://docs.spring.io/spring-cloud-gateway/docs/3.0.0-SNAPSHOT/reference/html/#gatewayfilter-factories





参考：

https://zhuanlan.zhihu.com/p/364130694

https://blog.csdn.net/rzpy_qifengxiaoyue/article/details/108249285

https://www.cnblogs.com/codegzy/p/15360075.html





