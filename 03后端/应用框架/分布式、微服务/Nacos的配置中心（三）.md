## Nacos的配置中心（三）

> Nacos相当于Eureka+config,它通过 Nacos Server 和 spring-cloud-starter-alibaba-nacos-config 实现配置的动态变更

​	

[TOC]

### 一、Nacos配置中心的服务领域模型

对于Nacos配置中心管理，通过namespace，group，dataid定位到一个配置文件。 

![img](../../../img/v2-0778d5701bd6d81f1170ae73991b3595_1440w.jpg)

![img](../../../img/20210730.png)

![img](../../../img/20210730000000001.png)



##### 1、新建namespace

配置完成以后就可以出现在配置列表了，各个命名空间是隔离的。

![image-20210801013002541](../../../img/image-20210801013002541.png)

##### 2、新增配置

步骤：新增配置 > 发布配置 > 发布成功

![image-20210801013118119](../../../img/image-20210801013118119.png)



![image-20210801013242167](../../../img/image-20210801013242167.png)





### 二、 搭建spring boot工程

完整的工程结构，如下图所示

<img src="../../../img/image-20210801011315199.png" alt="image-20210801011315199" style="zoom:50%;" />

##### 1、添加依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>org.linhuaming</groupId>
  <artifactId>nacos-config</artifactId>
  <version>1.0-SNAPSHOT</version>

  <name>nacos-config</name>
  <!-- FIXME change it to the project's website -->
  <url>http://www.example.com</url>

  <!-- 父依赖均是springboot-->
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.3.4.RELEASE</version>
  </parent>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
  </properties>

  <dependencies>
      <!--   
    <dependency>
      <groupId>com.alibaba.boot</groupId>
      <artifactId>nacos-config-spring-boot-starter</artifactId>
      <version>0.2.7</version>
    </dependency>
 	-->


    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
      <groupId>com.alibaba.cloud</groupId>
      <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
      <version>2.1.2.RELEASE</version>
    </dependency>

    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
      <version>3.7</version>
    </dependency>

  </dependencies>

  <dependencyManagement>
    <!--其中,scope和type是有必要的,否则maven会报错哦-->
    <dependencies>
        
      <dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-alibaba-dependencies</artifactId>
        <version>2.2.5.RELEASE</version>
        <scope>import</scope>
        <type>pom</type>
      </dependency>
        
    </dependencies>
  </dependencyManagement>

</project>

```



##### 2、 新建配置文件bootstarp.yml

这里也可以用properties，看个人习惯，但一定要命名为bootstrap！

```yaml
server:
  port: 8080
spring:
  profiles:
    active: dev
  application:
    # name: nacos-config-demo  #指定项目的名字
    name: nacos-config
  cloud:
    nacos:
      config:
        server-addr: 127.0.0.1:8848
        # namespace: 1fb54663-97b8-40bd-884b-5300e071903e  #指定namespace
        # group: DEFAULT_GROUP                             #指定GROUP
        # 指定DataId
        prefix: ${spring.application.name}
        file-extension: yml
```

说明：

介绍一下Data Id的定义规则：

- Data Id的命名规则为： {prefix}-{spring.profile.active}.{file-extension}
- 当spring.profile.active 为空时，定义规则为{prefix}.{file-extension}；
- prefix 默认为`spring.application.name`的值，也可以通过配置项`spring.cloud.nacos.config.prefix`来配置；



##### 3、 添加一个配置数据的封装类

Configdata.java 

```java
package org.linhuaming.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope //支持动态配置
public class Configdata {
    /**
     * 其中通过@Value注解，去读取配置中心对应的配置文件中的值
     */
    @Value("${nacos.info}")
    private String info;

    public String getInfo(){
        return info;
    }
}
```



编写一个NacosConfigController.java

```java
package org.linhuaming.controller;

import org.linhuaming.data.Configdata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NacosConfigController {

    @Autowired
    private Configdata configdata;

    @RequestMapping("/getInfo")
    public String getInfo(){
        System.out.println(configdata.getInfo());
        return configdata.getInfo();
    }

}
```



##### 4、启动spring boot 应用即可

主启动类没有要加的特殊注解，就正常的springboot启动就可以了。

- 我们试着调一下刚刚的getInfo接口，如下图所示。

  OK，获取成功！

<img src="../../../img/image-20210801013525524.png" alt="image-20210801013525524" style="zoom:50%;" />





- 我们再试着去配置中心修改刚刚的配置，如下图所示。

  我们试着修改内容为： hello nacos,change

![image-20210801013643984](../../../img/image-20210801013643984.png)



- 这里我们不用重启服务，直接再次调用接口，如下图所示。

  可以看到发现内容已经修改了，是不是很神奇！这就是刚刚RefreshScope注解的作用---动态配置！

  <img src="../../../img/image-20210801014032491.png" alt="image-20210801014032491" style="zoom:50%;" />

### 三、Nacos加载多个配置文件与共享配置文件

参考：  https://blog.didispace.com/spring-cloud-alibaba-nacos-config-3/



