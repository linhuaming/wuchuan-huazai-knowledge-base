## Nacos 环境搭建（二）

[TOC]

### 一、 下载并启动nacos

1、下载地址：https://github.com/alibaba/nacos/releases

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190506115204839.png)

选择下载 ： nacos-server-1.0.0.zip 即可



2、解压，进入bin目录，如下图所示。

![image-20210720225718842](../../../img/image-20210720225718842.png)



**服务启动：**

**Linux/Unix/Mac**

启动命令(standalone代表着单机模式运行，非集群模式):

```shell
sh startup.sh -m standalone
```

如果您使用的是ubuntu系统，或者运行脚本报错提示[[符号找不到，可尝试如下运行：

```shell
bash startup.sh -m standalone
```



**Windows**

启动命令(standalone代表着单机模式运行，非集群模式):

```shell
startup.cmd -m standalone
```



Nacos默认端口8848。默认的账号密码为 nacos/nacos。http://localhost:8848/nacos/index.html



