## skywalking 链路追踪 - 简单介绍及快速入门

[TOC]

### 一、为什么需要Skywalking

对于一个大型的几十个、几百个微服务构成的微服务架构系统，通常会遇到一些问题，比如：

- 如何串联整个调用链路，快速定位问题?
- 如何缕清各个微服务之间的依赖关系？
- 如何进行各个微服务接口的性能分析？
- 如何跟踪整个业务流程的调用处理顺序？



​		在分布式系统，尤其是微服务系统中，一次外部请求往往需要内部多个模块，多个中间件，多台机器的相互调用才能完成。在这一系列的调用中，可能有些是串行的，而有些是并行的。在这种情况下，我们如何才能确定这整个请求调用了哪些应用？哪些模块？哪些节点？以及它们的先后顺序和各部分的性能如何呢？



### 二、Skywalking 是什么

​		SkyWalking是一个国产开源框架。SkyWalking是分布式系统的应用程序性能监视工具，专为微服务、云原生架构和基于容器（Docker、K8s）架构而设计。它是一款优秀的APM（Application Performance Management）工具，包括了分布式追踪、性能分析、应用和服务依赖分析等。



### 三、Skywalking 架构



![image-20220529214351648](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529214351648.png)

上述架构图中主要分为四个部分，如下：

- 上面的Agent：负责收集日志数据，并且传递给中间的OAP服务器。
- 中间的OAP：负责接收 Agent 发送的 Tracing 和Metric的数据信息，然后进行分析(Analysis Core) ，存储到外部存储器( Storage )，最终提供查询( Query )功能。
- 左面的UI：负责提供web控制台，查看链路，查看各种指标，性能等等。
- 右面Storage：负责数据的存储，支持多种存储类型，比如ES、mysql 等。



​		看了架构图之后，思路很清晰了，Agent负责收集日志传输数据，通过GRPC的方式传递给OAP进行分析并且存储到数据库中，最终通过UI界面将分析的统计报表、服务依赖、拓扑关系图展示出来。



### 四、服务端如何搭建

skywalking同样是通过jar包方式启动，需要下载jar包，地址：
https://skywalking.apache.org/downloads/



##### 1、下载安装包

选择**V8.7.0**这个版本，如下图：

![](https://img-blog.csdnimg.cn/img_convert/2cb56e0ef60cd0f3a87990be651e1123.png)

> 以上只是自己的选择，可以按照自己的需要选择其他版本



解压之后完整目录如下图：

![](https://img-blog.csdnimg.cn/img_convert/a568814c7f59ca8767d1116e3cf830e7.png)

重要的目录结构分析如下：

- **agent**：客户端需要指定的目录，其中有一个jar，就是负责和客户端整合收集日志
- **bin**：服务端启动的脚本
- **config**：一些配置文件的目录
- **logs**：oap服务的日志目录
- **oap-libs**：oap所需的依赖目录
- **webapp**：UI服务的目录



##### 2、安装，配置文件修改

启动之前需要对配置文件做一些修改，修改如下：

**2.1、/config/application.yml**

这个是oap服务的配置文件，需要修改注册中心为nacos，如下图：

![](https://img-blog.csdnimg.cn/img_convert/b207b9f0eae8d05edff794b7d3130f50.png)

**配置①**：修改默认注册中心选择nacos，这样就不用在启动参数中指定了。

**配置②**：修改nacos的相关配置，由于陈某是本地的，则不用改动，根据自己情况修改。



**2.2、webapp/webapp.yml**

这个是UI服务的配置文件，其中有一个**server.port**配置，是UI服务的端口，默认**8080**，陈某将其改成**8888**，避免端口冲突，如下图：

![img](https://img-blog.csdnimg.cn/img_convert/2839f204b6a382d8114f10d97bdc3237.png)



##### 3、启动服务

启动命令在/bin目录下，这里需要启动两个服务，如下：

oap服务：对应的启动脚本oapService.bat，Linux下对应的后缀是sh

UI服务：对应的启动脚本webappService.bat，Linux下对应的后缀是sh

当然还有一个startup.bat启动文件，可以直接启动上述两个服务，我们可以直接使用这个脚本，直接双击，将会弹出两个窗口则表示启动成功，如下图：

![](https://img-blog.csdnimg.cn/img_convert/b94a03eff22807a81c0fa2b03c778aa3.png)



> 注意：这里因为选择了nacos，还需要把nacos启动先，否则启动失败。

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529221106321.png)







此时直接访问：http://localhost:8888/，直接进入UI端，如下图：

![](https://img-blog.csdnimg.cn/img_convert/68f23739b1fa56d2f6164abd50c15d5d.png)



### 五、客户端如何搭建

客户端也就是单个微服务，由于Skywalking采用字节码增强技术，因此对于微服务无代码侵入，只要是普通的微服务即可，不需要引入什么依赖。



这是我本地Spring Cloud 的三个服务，如下：

- **nacos-consumer**：消费者微服务
- **nacos-provider**：生产者服务
- **server-gateway**：网关服务



结构目录如下图：

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529215652099.png" alt="image-20220529215652099" style="zoom:50%;" />



想要传输数据必须借助skywalking提供的agent，只需要在启动参数指定即可，命令如下：

```java
-javaagent:D:\dev_software\apache-skywalking-apm-8.7.0\apache-skywalking-apm-bin\agent\skywalking-agent.jar
-Dskywalking.agent.service_name=server-gateway
-Dskywalking.collector.backend_service=127.0.0.1:11800
```



上述命令解析如下：

- -javaagent：指定skywalking-agent.jar的路径
- -Dskywalking.agent.service_name：指定在skywalking中的服务名称，一般是微服务的spring.application.name
- -Dskywalking.collector.backend_service：指定oap服务绑定的地址，由于这里是本地，并且oap服务默认的端口是11800，因此只需要配置为127.0.0.1:11800



上述参数可以在命令行通过java -jar xxx指定，在IDEA中操作如下图：

![image-20220529220545990](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529220545990.png)



上述三个微服务都需要配置skywalking的启动配置，配置成功后正常启动即可。

> 注意：agent的jar包路径不能包含中文，不能有空格，否则运行不成功。



成功启动后，直接通过网关访问：

http://127.0.0.1:8040/nacos-provider/provider/hello

![image-20220529223513615](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529223513615.png)



此时查看skywalking的UI端，可以看到三个服务已经监控成功了，如下图：

![2022052923220001](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/2022052923220001.png)



点击拓扑图这里，服务之前的依赖关系也是可以很清楚地看到，如下图：

![image-20220529223839145](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529223839145.png)



请求链路的信息也是能够很清楚地看到，比如请求的url，执行时间、调用的服务，如下图：

![image-20220529224201443](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220529224201443.png)

感觉怎样？是不是很高端，比zipkin的功能更加丰富。



### 六、数据如何持久化

你会发现只要服务端重启之后，这些链路追踪数据将会丢失了，因为skywalking默认持久化的方式是存储在内存中。

当然这里也是可以通过插拔方式的替换掉存储中间件，企业中往往是使用**ES**存储，这章介绍一下**MySQL**的方式存储，ES方式后面单独开一篇文章介绍。

##### 1、修改配置文件

修改config/application.yml文件中的存储方式，总共需要修改两处地方。

- 修改默认的存储方式为mysql，如下图：

![img](https://img-blog.csdnimg.cn/img_convert/c9fb70212985a3dbb8af75fcd2d507ae.png)



- 修改Mysql相关的信息，比如用户名、密码等，如下图：

![img](https://img-blog.csdnimg.cn/img_convert/ebe47bec0adf9c6aa863d073742fda72.png)



##### 2、添加mysql的依赖

默认的oap中是没有jdbc驱动依赖，因此需要我们手动添加一下，只需要将驱动的jar放在**oap-libs**文件夹中，如下图：

![](https://img-blog.csdnimg.cn/img_convert/5d06f3734544c1c58411755b0b33df8f.png)



好了，已经配置完成，启动服务端，在skywalking这个数据库中将会自动创建表，如下图：

![img](https://img-blog.csdnimg.cn/img_convert/9c6ad8dc2142b71185bb70bcc8d9196b.png)



### 七、日志监控如何做

如何将日志数据传输到skywalking中呢？如下图：

下图是Spring cloud+ELK +Skywalking架构图，只是大致结构。

![image-20220530001528659](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220530001528659.png)





##### 1、pom.xml 添加依赖

```xml
<dependency>
   <groupId>org.apache.skywalking</groupId>
   <artifactId>apm-toolkit-logback-1.x</artifactId>
   <version>${project.release.version}</version>
</dependency>
```



##### 2、logback.xml 添加配置信息

新建一个logback-spring.xml放在resource目录下，配置如下图：

```xml
<!-- 控制台打印 -->
<appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
        <!-- 对日志进行格式化。 -->
        <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
            <!-- 使用 MDC模式 -->
            <layout class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.mdc.TraceIdMDCPatternLogbackLayout">
                <pattern>
                    <!-- 日志格式化示例 -->
                    %d{yyyy-MM-dd HH:mm:sss.SSS} [%X{tid}] |%level|%class|%thread|[%X{TRACE_ID}]|%method|%line|%msg%n
                </pattern>
            </layout>
        </encoder>
</appender>

<!-- 没有用到logstash此处可以不写 -->
<!-- logstash配置-->
<!-- 在日志中加入skywalking traceId 链路追踪id -->
<appender name="LOGSTASH" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
        <destination>127.0.0.1:4560</destination>
        <!-- 日志输出编码 -->
        <encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder">
            <provider class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.logstash.TraceIdJsonProvider">
            </provider>
        </encoder>
</appender>

<!-- gRPC 配置 -->
<!-- gRPC 报告器可以将收集到的日志转发到 SkyWalking OAP服务器或SkyWalking Satellite sidecar。Trace id、segment id 和 span id 将自动附加到日志。无需修改现有布局 -->
<appender name="grpc-log" class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.log.GRPCLogClientAppender">
    <encoder class="ch.qos.logback.core.encoder.LayoutWrappingEncoder">
        <layout class="org.apache.skywalking.apm.toolkit.log.logback.v1.x.mdc.TraceIdMDCPatternLogbackLayout">
            <Pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%X{tid}] [%thread] %-5level %logger{36} -%msg%n</Pattern>
        </layout>
    </encoder>
</appender>

<root>
    <level value="INFO" />
    <appender-ref ref="stdout" />
    <appender-ref ref="LOGSTASH"/>  <!--添加输出源logstash-->
    <appender-ref ref="grpc-log" />
</root>

```

配置参考：https://skywalking.apache.org/docs/main/v8.5.0/en/setup/service-agent/java-agent/application-toolkit-logback-1.x/



##### 3、agent.config 配置文件

如果**agent**和**oap**服务不在同一台服务器上，需要在/agent/config/agent.config配置文件末尾添加如下配置：

```properties
plugin.toolkit.log.grpc.reporter.server_host=${SW_GRPC_LOG_SERVER_HOST:10.10.10.1}
plugin.toolkit.log.grpc.reporter.server_port=${SW_GRPC_LOG_SERVER_PORT:11800}
plugin.toolkit.log.grpc.reporter.max_message_size=${SW_GRPC_LOG_MAX_MESSAGE_SIZE:10485760}
plugin.toolkit.log.grpc.reporter.upstream_timeout=${SW_GRPC_LOG_GRPC_UPSTREAM_TIMEOUT:30}
```

**配置说明：**

![](https://img-blog.csdnimg.cn/img_convert/ecd8ae1336841b517e36306807bf4db3.png)



##### 4、日志测试

访问：http://127.0.0.1:8040/gateway/test1

控制台打印的日志如下图：

![image-20220530230647073](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220530230647073.png)



在skywalking web管理界面，日志模块输出的日志如下图：

![image-20220530230753302](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220530230753302.png)



### 八、性能监控如何做

skywalking在性能剖析方面真的是非常强大，提供到基于堆栈的分析结果，能够让运维人员一眼定位到问题。

新建一个 /gateway/sleep 接口，如下：

![image-20220530231318324](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220530231318324.png)

> 上述代码中休眠了2秒，看看如何在skywalking中定位这个问题。



在性能剖析模块->新建任务->选择服务、填写端点、监控时间，操作如下图：

![image-20220530233257413](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220530233257413.png)



### 九、监控告警如何做

对于服务的异常信息，比如接口有较长延迟，skywalking也做出了告警功能，如下图：

![image-20220530233740517](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220530233740517.png)

skywalking中有一些默认的告警规则，如下：

- 最近3分钟内服务的平均响应时间超过1秒
- 最近2分钟服务成功率低于80%
- 最近3分钟90%服务响应时间超过1秒
- 最近2分钟内服务实例的平均响应时间超过1秒



当然除了以上四种，随着Skywalking不断迭代也会新增其他规则，这些规则的配置在config/alarm-settings.yml配置文件中，如下图：

![](https://img-blog.csdnimg.cn/img_convert/c48e94f62145ecb5cc514b5b0838cf1b.png)



每个规则都由相同的属性组成，这些属性的含义如下图：

![](https://img-blog.csdnimg.cn/img_convert/fec82fd7c93d041ecc9833868263a675.png)



如果想要调整默认的规则，比如监控返回的信息，监控的参数等等，只需要改动上述配置文件中的参数即可。

当然除了以上默认的几种规则，skywalking还适配了一些钩子（webhooks）。其实就是相当于一个回调，一旦触
发了上述规则告警，skywalking则会调用配置的webhook，这样开发者就可以定制一些处理方法，比如发送邮
件、微信、钉钉通知运维人员处理。



当然这个钩子也是有些规则的，如下：

- POST请求
- **application/json** 接收数据
- 接收的参数必须是AlarmMessage中指定的参数（可选）。



新建一个接口，如下：

![image-20220531001308454](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531001308454.png)



接口定制完成后，只需要在config/alarm-settings.yml配置文件中添加这个钩子，如下图：

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531001109514.png" alt="image-20220531001109514" style="zoom:50%;" />



好了，这就已经配置完成了，测试也很简单，还是调用上面案例中的睡眠两秒的接口：
http://127.0.0.1:8040/gateway/sleep，多调用几次，则会触发告警，如下图：

![image-20220531000046146](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531000046146.png)









参考：

https://blog.csdn.net/m0_66269798/article/details/122636533

https://zhuanlan.zhihu.com/p/344020712?ivk_sa=1024320u

https://blog.csdn.net/qq_41910252/article/details/122746979