## Logstash 简单介绍及快速入门（一）

[TOC]

### 一、Logstash 简介

​		Logstash作为Elasicsearch常用的实时数据采集引擎，可以采集来自不同数据源的数据，并对数据进行处理后输出到多种输出源，是ELK的重要组成部分。

​		Logstash 能够动态地采集、转换和传输数据，不受格式或复杂度的影响。利用 Grok 从非结构化数据中派生出结构化数据，从 IP 地址解码出地理坐标，匿名化或排除敏感字段，并简化整体处理过程。也就是一个采集 > 过滤 > 输出的过程，从哪里采集，采集后到哪里去，中间要不要过滤掉一下东西。



### 二、Logstash主要组件

#### 1、input 输入

input 就是Logstash的入口，数据首先需要经过这一步。它支持采集很多输入选择，比如syslog、redis、file、es、exec、log4j、jdbc、kafka、tcp、beats等。



#### 2、filter 过滤

filter是一个个负责筛选的部分，数据从源传输到输出的过程中，Logstash 过滤器能够解析各个事件，识别已命名的字段以构建结构，并将它们转换成通用格式。它支持很多过滤库，如下：

| 过滤器库 | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| mutate   | 对字段的处理，比如添加字段、对字段取值和赋值、切割或者合并   |
| json     | 向json里添加或者删除字段                                     |
| data     | 对时间格式的转化等                                           |
| grok     | 由单个简单的正则组合，对一大段文字进行切割并映射到关键字的工具 |



#### 3、output 输出

Elasticsearch 是首选输出方向，但也支持很多选择（与输入差不多），可以将数据发送到您要指定的地方，比如redis、file、es、tcp等。



### 三、下载、安装

**1、下载地址**

Logstash下载地址： https://www.elastic.co/cn/downloads/past-releases/logstash-7-12-1 ，我这里选择的是v7.12.1 版本。

![image-20220601234655806](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220601234655806.png)



**2、安装**

解压后的文件为：

![image-20220601234815662](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220601234815662.png)



 在logstash解压后的config目录下创建：logstash-test.conf 文件，内容如下:

```properties
input {
    stdin{
    }
} 
 
output {
    stdout{
    }
}
```

> 说明：
>
> input 就是数据输入的地方， output 也就是数据输出到哪里。
>
> stdin 代表的是从cmd控制台输入，stdout 代表的是数据会输出到cmd控制台。



logstash启动，建立一个startup-test.bat 文件，输入一下命令，然后双击该文件：

```
../bin/logstash -f logstash-test.conf
```

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220602000607738.png" alt="image-20220602000607738"  />

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220602001118985.png" alt="image-20220602001118985" style="zoom: 50%;" />



测试一下，在cmd控制台，输入hello logstash , 回车，然后会看到输入一下内容

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220602001320995.png" alt="image-20220602001320995" style="zoom:50%;" />



验证logstash是否启动成功，出现下面界面说明启动成功，访问：http://localhost:9600/

<img src="C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220602001048271.png" alt="image-20220602001048271" style="zoom:50%;" />



### 四、参数与配置

#### 1、logstash启动，常用参数

| 参数 | 说明                              | 举例                                                    |
| ---- | --------------------------------- | ------------------------------------------------------- |
| -e   | 使用命令行里的配置参数启动实例    | ./bin/logstash -e ‘input {stdin {}} output {stdout {}}’ |
| -f   | 指定启动实例的配置文件            | ./bin/logstash -f test.conf                             |
| -w   | 指定filter线程数量，默认线程数是5 |                                                         |
| -l   | 指定日志文件名称                  | ./bin/logstash-f test.conf -l logs/test.log             |



#### 2、配置文件结构及语法

**区段**

Logstash通过`{}`来定义区域，区域内可以定义插件，一个区域内可以定义多个插件，如下：

```properties
input {
    stdin {
    }
    beats {
        port => 5044
    }
}
```



**数据类型**

Logstash仅支持少量的数据类型：

Boolean：ssl_enable => true

 Number：port => 33

 String：name => “Hello world”

 Commonts：# this is a comment



**字段引用**

  Logstash数据流中的数据被称之为Event对象，Event以JSON结构构成，Event的属性被称之为字段，如果你像在配置文件中引用这些字段，只需要把字段的名字写在中括号[]里就行了，如[type]，对于嵌套字段每层字段名称都写在[]里就可以了，比如：[tags][type]；除此之外，对于Logstash的arrag类型支持下标与倒序下表，如：[tags][type][0],[tags][type][-1]。



**条件判断**

Logstash支持下面的操作符：

 等式（equality）：==, !=, <, >, <=, >=

正则表达式（ regexp）：=~, !~

包括（inclusion）：in, not in

布尔（boolean）：and, or, nand, xor

一元运算符（unary）：!

```properties
if EXPRESSION {
  ...
} else if EXPRESSION {
  ...
} else {
  ...
}
```



**环境变量引用**

Logstash支持引用系统环境变量，环境变量不存在时可以设置默认值，例如：

```properties
export TCP_PORT=12345
```

```properties
input {
  tcp {
    port => "${TCP_PORT:54321}"
  }
}
```



#### 3、常用输入插件

**1）File 读取插件**

文件读取插件主要用来抓取文件的变化信息，将变化信息封装成Event进程处理或者传递。

```properties
input {
  file {
    path => ["/var/log/*.log", "/var/log/message"]
    type => "system"
    start_position => "beginning"
  }
}
```



>  常用参数

| 参数名称               | 类型               | 默认值  | 描述信息                                                     |
| ---------------------- | ------------------ | ------- | ------------------------------------------------------------ |
| add_field              | hash               | {}      | 用于向Event中添加字段                                        |
| close_older            | number             | 3600    | 设置文件多久秒内没有更新就关掉对文件的监听                   |
| codec                  | string             | “plain” | 输入数据之后对数据进行解码                                   |
| delimiter              | string             | “\n”    | 文件内容的行分隔符，默认按照行进行Event封装                  |
| discover_interval      | number             | 15      | 间隔多少秒查看一下path匹配对路径下是否有新文件产生           |
| enable_metric          | boolean            | true    |                                                              |
| exclude                | array              | 无      | path匹配的文件中指定例外，如：path => “/var/log/*“；exclude =>”*.gz” |
| id                     | string             | 无      | 区分两个相同类型的插件，比如两个filter，在使用Monitor API监控是可以区分，建议设置上ID |
| ignore_older           | number             | 无      | 忽略历史修改，如果设置3600秒，logstash只会发现一小时内被修改过的文件，一小时之前修改的文件的变化不会被读取，如果再次修改该文件，所有的变化都会被读取，默认被禁用 |
| max_open_files         | number             | 无      | logstash可以同时监控的文件个数（同时打开的file_handles个数），如果你需要处理多于这个数量多文件，可以使用“close_older”去关闭一些文件 |
| path                   | array              | 无      | ***必须设置项\***，用于匹配被监控的文件，如“/var/log/*.log”或者“/var/log/**/*.log”，必须使用绝对路径 |
| sincedb_path           | string             | 无      | 文件读取记录，必须指定一个文件而不是目录，文件中保存没个被监控的文件等当前inode和byteoffset，默认存放位置“$HOME/.sincedb*” |
| sincedb_write_interval | number             | 15      | 间隔多少秒写一次sincedb文件                                  |
| start_position         | “beginning”，“end” | ” end”  | 从文件等开头还是结尾读取文件内容，默认是结尾，如果需要导入文件中的老数据，可以设置为“beginning”，该选项只在第一次启动logstash时有效，如果文件已经存在于sincedb的记录内，则此配置无效 |
| stat_interval          | number             | 1       | 间隔多少秒检查一下文件是否被修改，加大此参数将降低系统负载，但是增加了发现新日志的间隔时间 |
| tags                   | array              | 无      | 可以在Event中增加标签，以便于在后续的处理流程中使用          |
| type                   | string             |         | Event的type字段，如果采用elasticsearch做store，在默认情况下将作为elasticsearch的type |



**2）beats 监听插件**

Beats插件用于建立监听服务，接收Filebeat或者其他beat发送的Events；

```properties
input {
    beats {
        port => 5044
    }
}
```



>  常用参数

| 参数名称                    | 类型                         | 默认值    | 描述信息                                                     |
| --------------------------- | ---------------------------- | --------- | ------------------------------------------------------------ |
| cipher_suites               | array                        |           | 密码加密算法列表，根据优先级排序                             |
| client_inactivity_timeout   | number                       | 60        | 多长时间之后关闭空闲的连接                                   |
| codec                       |                              |           |                                                              |
| enable_metric               |                              |           |                                                              |
| host                        | string                       | “0.0.0.0” | 监听的IP地址                                                 |
| id                          |                              |           |                                                              |
| include_codec_tag           | boolean                      | true      |                                                              |
| port                        | number                       | 无        | 必须设置项，监听服务监听的端口                               |
| ssl                         | boolean                      | false     | 是否启用ssl                                                  |
| ssl_certificate             | string                       | 无        | ssl证书路径                                                  |
| ssl_certificate_authorities | array                        | []        | 定义证书文件或者路径列表，当“ssl_verify_mode”被设置为“peer”或者“force_peer”时有效 |
| ssl_handshake_timeout       | number                       | 10000     | 间隔多少毫秒ssl握手超时                                      |
| ssl_key                     | string                       | 无        | ssl密钥                                                      |
| ssl_key_passphrase          | string                       | 无        | ssl密钥密码                                                  |
| ssl_verify_mode             | “none”、”peer”、”force_peer” | none      |                                                              |
| tags                        |                              |           |                                                              |
| tls_max_version             | number                       | 1.2       | Themaximum TLS version allowed for the encrypted connections. The value must bethe one of the following: 1.0 for TLS 1.0, 1.1 for TLS 1.1, 1.2 for TLS 1.2 |
| tls_min_version             | number                       | 1         | Theminimum TLS version allowed for the encrypted connections. The value must beone of the following: 1.0 for TLS 1.0, 1.1 for TLS 1.1, 1.2 for TLS 1.2 |



**3）tcp 监听插件**

TCP插件有两种工作模式，“Client”和“Server”，分别用于发送网络数据和监听网络数据。

```properties
input {
    tcp {
  		mode => "server"
		host => "127.0.0.1"
		port => 4560
		codec => json_lines
	}
}
```



> 常用参数

| 参数名称              | 类型               | 默认值   | 描述信息                                                     |
| --------------------- | ------------------ | -------- | ------------------------------------------------------------ |
| add_field             |                    |          |                                                              |
| codec                 |                    |          |                                                              |
| enable_metric         |                    |          |                                                              |
| host                  |                    |          |                                                              |
| id                    |                    |          |                                                              |
| mode                  | “server”、“client” | “server” | “server”监听“client”的连接请求，“client”连接“server”         |
| port                  | number             | 无       | 必须设置项，“server”模式时指定监听端口，“client”模式指定连接端口 |
| proxy_protocol        | boolean            | false    | Proxyprotocol support, only v1 is supported at this time     |
| ssl_cert              |                    |          |                                                              |
| ssl_enable            |                    |          |                                                              |
| ssl_extra_chain_certs |                    |          |                                                              |
| ssl_key               |                    |          |                                                              |
| ssl_key_passphrase    |                    |          |                                                              |
| ssl_verify            |                    |          |                                                              |
| tags                  |                    |          |                                                              |
| type                  |                    |          |



**4）redis 读取插件**

用于读取Redis中缓存的数据信息。

```properties
input {
  redis {
    host => "127.0.0.1"
    port => 6379
    data_type => "list"
    key => "logstash-list"
  }
}
```



>  常用参数

| 参数名称      | 类型                            | 默认值    | 描述信息                                                     |
| ------------- | ------------------------------- | --------- | ------------------------------------------------------------ |
| add_field     |                                 |           |                                                              |
| batch_count   | number                          | 125       | 使用redis的batch特性，需要redis2.6.0或者更新的版本           |
| codec         |                                 |           |                                                              |
| data_type     | list，channel， pattern_channel | 无        | 根据设置不同，订阅redis使用不同的命令，依次是：BLPOP、SUBSCRIBE、PSUBSCRIBE，需要注意的是“channel”和“pattern_channel”是广播类型，相同的数据会同时发送给订阅了该channel的logstash，也就是说在logstash集群环境下会出现数据重复，集群中的每一个节点都将收到同样的数据，但是在单节点情况下，“pattern_channel”可以同时定于满足pattern的多个key |
| db            | number                          | 0         | 指定使用的redis数据库                                        |
| enable_metric |                                 |           |                                                              |
| host          | string                          | 127.0.0.1 | redis服务地址                                                |
| id            |                                 |           |                                                              |
| key           | string                          | 无        | 必须设置项，reidslist或者channel的key名称                    |
| password      | string                          | 无        | redis密码                                                    |
| port          | number                          | 6379      | redis连接端口号                                              |
| tags          |                                 |           |                                                              |
| threads       | number                          | 1         |                                                              |
| timeout       | number                          | 5         | redis服务连接超时时间，单位：秒                              |



**5）syslog 监听插件**

监听操作系统syslog信息。

```properties
input {
  syslog {
	host => "127.0.0.1"
    port => 514
  }
}
```



> 常用参数

| 参数名称        | 类型    | 默认值                                                       | 描述信息                                                     |
| --------------- | ------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| add_field       |         |                                                              |                                                              |
| codec           |         |                                                              |                                                              |
| enable_metric   |         |                                                              |                                                              |
| facility_labels | array   | [“kernel”,”user-level”, “mail”, “system”,”security/authorization”, “syslogd”, “lineprinter”, “network news”, “UUCP”, “clock”,”security/authorization”, “FTP”, “NTP”, “logaudit”, “log alert”, “clock”, “local0”,”local1”, “local2”, “local3”, “local4”,”local5”, “local6”, “local7”] | Labelsfor facility levels. These are defined in RFC3164.     |
| host            | string  | “0.0.0.0”                                                    | 监听地址                                                     |
| id              |         |                                                              |                                                              |
| locale          | string  | 无                                                           | 区域设置，类似linux的locale，日期格式设置                    |
| port            | number  | 514                                                          | 监听端口，Remember that ports less than 1024(privileged ports) may require root to use. |
| proxy_protocol  |         |                                                              |                                                              |
| severity_labels | array   | [“Emergency”,”Alert”, “Critical”, “Error”,”Warning”, “Notice”, “Informational”,”Debug”] | Labelsfor severity levels. These are defined in RFC3164.     |
| tags            |         |                                                              |                                                              |
| timezone        | string  | 无                                                           | 指定时区以便格式化日期                                       |
| type            | string  | 无                                                           | This is the base class for Logstash inputs. Add a `type` field to all events handled by this input. |
| use_labels      | boolean | true                                                         | Uselabel parsing for severity and facility levels.           |



#### 4、常用过滤插件

​		丰富的过滤器插件的是 logstash威力如此强大的重要因素，过滤器插件主要处理流经当前Logstash的事件信息，可以添加字段、移除字段、转换字段类型，通过正则表达式切分数据等，也可以根据条件判断来进行不同的数据处理方式。

**1）grok正则捕获**

​		grok 是Logstash中将非结构化数据解析成结构化数据以便于查询的最好工具，非常适合解析syslog logs，apache log， mysql log，以及一些其他的web log

- 预定义表达式调用

Logstash提供120个常用正则表达式可供安装使用，安装之后你可以通过名称调用它们，语法如下：`%{SYNTAX:SEMANTIC}`

  SYNTAX：表示已经安装的正则表达式的名称

  SEMANTIC：表示从Event中匹配到的内容的名称

例如：Event的内容为“[debug] 127.0.0.1 - test log content”，匹配`%{IP:client}`将获得“client: 127.0.0.1”的结果，前提安装了IP表达式；如果你在捕获数据时想进行数据类型转换可以使用`%{NUMBER:num:int}`这种语法，默认情况下，所有的返回结果都是string类型，当前Logstash所支持的转换类型仅有“int”和“float”；



一个稍微完整一点的事例：

日志文件http.log内容：*55.3.244.1 GET /index.html 15824 0.043*

表达式：`%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}`

 配置文件内容：

```properties
input {
  file {
    path => "/var/log/http.log"
  }
}
filter {
  grok {
    match => {"message" => "%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}"}
  }
}
```

 输出结果：

```properties
client: 55.3.244.1
method: GET
request: /index.html
bytes: 15824
duration: 0.043
```



- 自定义表达式调用

语法：`(?<field_name>the pattern here)`

举例：捕获10或11和长度的十六进制queue_id可以使用表达式`(?<queue_id>[0-9A-F]{10,11})`



安装自定义表达式：

与预定义表达式相同，你也可以将自定义的表达式配置到Logstash中，然后就可以像于定义的表达式一样使用；以下是操作步骤说明：

  1、在Logstash根目录下创建文件夹“patterns”，在“patterns”文件夹中创建文件“extra”（文件名称无所谓，可自己选择有意义的文件名称）；

  2、在文件“extra”中添加表达式，格式：patternName regexp，名称与表达式之间用空格隔开即可，如下：

```properties
# contents of ./patterns/postfix:
POSTFIX_QUEUEID [0-9A-F]{10,11}
```

  3、使用自定义的表达式时需要指定“patterns_dir”变量，变量内容指向表达式文件所在的目录，举例如下：

```properties
## 日志内容 ##
Jan  1 06:25:43 mailserver14 postfix/cleanup[21403]: BEF25A72965: message-id=<20130101142543.5828399CCAF@mailserver14.example.com>
```

```properties
## Logstash配置 ##
filter {
  grok {
    patterns_dir => ["./patterns"]
    match => { "message" => "%{SYSLOGBASE} %{POSTFIX_QUEUEID:queue_id}: %{GREEDYDATA:syslog_message}" }
  }
}
```

```properties
## 运行结果 ##
timestamp: Jan 1 06:25:43
logsource: mailserver14
program: postfix/cleanup
pid: 21403
queue_id: BEF25A72965
```



> grok常用配置参数

| 参数名称            | 类型    | 默认值                | 描述信息                                                     |
| ------------------- | ------- | --------------------- | ------------------------------------------------------------ |
| add_field           |         |                       |                                                              |
| add_tag             |         |                       |                                                              |
| break_on_match      | boolean | true                  | match字段存在多个pattern时，当第一个匹配成功后结束后面的匹配，如果想匹配所有的pattern，将此参数设置为false |
| enable_metric       |         |                       |                                                              |
| id                  |         |                       |                                                              |
| keep_empty_captures | boolean | false                 | 如果为true，捕获失败的字段奖设置为空值                       |
| match               | array   | {}                    | 设置pattern数组: match=> {“message” => [“Duration: %{NUMBER:duration}”,”Speed: %{NUMBER:speed}”]} |
| named_captures_only | boolean | true                  | If true, only store named captures from grok.                |
| overwrite           | array   | []                    | 覆盖字段内容: match=> { “message” => “%{SYSLOGBASE} %{DATA:message}” } overwrite=> [ “message” ] |
| patterns_dir        | array   | []                    | 指定自定义的pattern文件存放目录，Logstash在启动时会读取文件夹内patterns_files_glob 匹配的所有文件内容 |
| patterns_files_glob | string  | “*”                   | 用于匹配patterns_dir中的文件                                 |
| periodic_flush      | boolean | false                 | 定期调用filter的flush方法                                    |
| remove_field        | array   | []                    | 从Event中删除任意字段: remove_field=> [ “foo_%{somefield}” ] |
| remove_tag          | array   | []                    | 删除“tags”中的值: remove_tag=> [ “foo_%{somefield}” ]        |
| tag_on_failure      | array   | [“_grokparsefailure”] | 当没有匹配成功时，将此array添加到“tags”字段内                |
| tag_on_timeout      | string  | “_groktimeout”        | 当匹配超时时，将此内容添加到“tags”字段内                     |
| timeout_millis      | number  | 30000                 | 设置单个match到超时时间，单位：毫秒，如果设置为0，则不启用超时设置 |

- 其他
- 一般的正则表达式只能匹配单行文本，如果一个Event的内容为多行，可以在pattern前加“(?m)”
- 对于Hash和Array类型，Hash表示键值对，Array表示数组
- Grok表达式在线debug地址：http://grokdebug.herokuapp.com
- 预定义正则表达式参考地址：https://github.com/logstash-plugins/logstash-patterns-core/tree/master/patterns



**2）date时间处理插件**

​		该插件用于时间字段的格式转换，比如将“Apr 17 09:32:01”（MMM dd HH:mm:ss）转换为“MM-dd HH:mm:ss”。而且通常情况下，Logstash会为自动给Event打上时间戳，但是这个时间戳是Event的处理时间（主要是input接收数据的时间），和日志记录时间会存在偏差（主要原因是buffer），我们可以使用此插件用日志发生时间替换掉默认是时间戳的值。

配置示例

```properties
filter {
	data {
        
    }
}
```



> 常用配置参数

| 参数名称       | 类型   | 默认值       | 描述信息                                                     |
| -------------- | ------ | ------------ | ------------------------------------------------------------ |
| add_field      |        |              |                                                              |
| add_tag        |        |              |                                                              |
| enable_metric  |        |              |                                                              |
| id             |        |              |                                                              |
| locale         |        |              |                                                              |
| match          | array  | []           | 时间字段匹配，可自定多种格式，直到匹配到或者匹配结束，格式：[ field,formats… ]，如：match=> [ “logdate”, “MMM dd yyyy HH:mm:ss”, “MMM d yyyy HH:mm:ss”, “ISO8601” ] |
| periodic_flush |        |              |                                                              |
| remove_field   |        |              |                                                              |
| remove_tag     |        |              |                                                              |
| tag_on_failure |        |              |                                                              |
| target         | string | “@timestamp” | 指定match匹配并且转换为date类型的存储位置（字段），默认覆盖到“@timestamp” |
| timezone       | string | 无           | 指定时间格式化的时区                                         |



**3）mutate 数据修改插件**

mutate 插件是 Logstash另一个重要插件。它提供了丰富的基础类型数据处理能力。可以重命名，删除，替换和修改事件中的字段。

配置示例

```properties
filter {
	mutate {
        
    }
}
```



> 常用配置参数

| 参数名称       | 类型  | 默认值 | 描述信息                                                     |
| -------------- | ----- | ------ | ------------------------------------------------------------ |
| add_field      |       |        |                                                              |
| add_tag        |       |        |                                                              |
| convert        | hash  | 无     | 将指定字段转换为指定类型，字段内容是数组，则转换所有数组元素，如果字段内容是hash，则不做任何处理，目前支持的转换类型包括：integer,float, string, and boolean.例如： convert=> { “fieldname” => “integer” } |
| enable_metric  |       |        |                                                              |
| gsub           | array | 无     | 类似replace方法，使用指定内容替换掉目标字符串的现有内容，前提是目标字段必须是字符串，否则不做任何处理，例如：[ “fieldname”, “/”, “*“, “fieldname2”, “[\\?#-]”, “.”]，解释：使用“*”替换掉“fieldname”中的所有“／”，使用“.”替换掉“fieldname2”中的所有“\”“?”、“#”和“-” |
| id             |       |        |                                                              |
| join           | hash  | 无     | 使用指定的符号将array字段的每个元素连接起来，对非array字段无效。例如： 使用“,”将array字段“fieldname”的每一个元素连接成一个字符串： join=> { “fieldname” => “,” } |
| lowercase      | array | 无     | 将自定的字段值转换为小写                                     |
| merge          | hash  | 无     | 合并两个array或者hash，如果是字符串，将自动转换为一个单元素数组；将一个array和一个hash合并。例如： 将”added_field”合并到”dest_field”： merge=> { “dest_field” => “added_field” } |
| periodic_flush |       |        |                                                              |
| remove_field   |       |        |                                                              |
| remove_tag     |       |        |                                                              |
| rename         | hash  | 无     | 修改一个或者多个字段的名称。例如： 将”HOSTORIP”改名为”client_ip”： rename=> { “HOSTORIP” => “client_ip” } |
| replace        | hash  | 无     | 使用新值完整的替换掉指定字段的原内容，支持变量引用。例如： 使用字段“source_host”的内容拼接上字符串“: My new message”之后的结果替换“message”的值： replace=> { “message” => “%{source_host}: My new message” } |
| split          | hash  | 无     | 按照自定的分隔符将字符串字段拆分成array字段，只能作用于string类型的字段。例如： 将“fieldname”的内容按照“,”拆分成数组： split=> { “fieldname” => “,” } |
| strip          | array | 无     | 去掉字段内容两头的空白字符。例如： 去掉“field1”和“field2”两头的空格： strip=> [“field1”, “field2”] |
| update         | hash  | 无     | 更新现有字段的内容，例如： 将“sample”字段的内容更新为“Mynew message”： update=> { “sample” => “My new message” } |
| uppercase      | array | 无     | 将字符串转换为大写                                           |



**4）json 插件**

JSON插件用于解码JSON格式的字符串，一般是一堆日志信息中，部分是JSON格式，部分不是的情况下

 

- 配置示例

```properties
filter {
	json {
        source => ...
    }
}
```

```properties
## 事例配置，message是JSON格式的字符串："{\"uid\":3081609001,\"type\":\"signal\"}" ##
filter {
    json {
        source => "message"
        target => "jsoncontent"
    }
}
```

```properties
## 输出结果 ##
{
    "@version": "1",
    "@timestamp": "2014-11-18T08:11:33.000Z",
    "host": "web121.mweibo.tc.sinanode.com",
    "message": "{\"uid\":3081609001,\"type\":\"signal\"}",
    "jsoncontent": {
        "uid": 3081609001,
        "type": "signal"
    }
}
## 如果从事例配置中删除`target`，输出结果如下 ##
{
    "@version": "1",
    "@timestamp": "2014-11-18T08:11:33.000Z",
    "host": "web121.mweibo.tc.sinanode.com",
    "message": "{\"uid\":3081609001,\"type\":\"signal\"}",
    "uid": 3081609001,
    "type": "signal"
}
```



> 常用配置参数

| 参数名称             | 类型    | 默认值 | 描述信息                                                     |
| -------------------- | ------- | ------ | ------------------------------------------------------------ |
| add_field            |         |        |                                                              |
| add_tag              |         |        |                                                              |
| enable_metric        |         |        |                                                              |
| id                   |         |        |                                                              |
| periodic_flush       |         |        |                                                              |
| remove_field         |         |        |                                                              |
| remove_tag           |         |        |                                                              |
| skip_on_invalid_json | boolean | false  | 是否跳过验证不通过的JSON                                     |
| source               | string  | 无     | ***必须设置项\***，指定需要解码的JSON字符串字段              |
| tag_on_failure       |         |        |                                                              |
| target               | string  | 无     | 解析之后的JSON对象所在的字段名称，如果没有，JSON对象的所有字段将挂在根节点下 |



**5）elasticSearch 查询过滤插件**

用于查询Elasticsearch中的事件，可将查询结果应用于当前事件中



>  常用配置参数

| 参数名称       | 类型    | 默认值             | 描述信息                                                     |
| -------------- | ------- | ------------------ | ------------------------------------------------------------ |
| add_field      |         |                    |                                                              |
| add_tag        |         |                    |                                                              |
| ca_file        | string  | 无                 | SSL Certificate Authority file path                          |
| enable_sort    | boolean | true               | 是否对结果进行排序                                           |
| fields         | array   | {}                 | 从老事件中复制字段到新事件中，老事件来源于elasticsearch（用于查询更新） |
| hosts          | array   | [“localhost:9200”] | elasticsearch服务列表                                        |
| index          | string  | “”                 | 用逗号分隔的elasticsearch索引列表，如果要操作所有所有使用“_all”或者“”，保存数据到elasticsearch时，如果索引不存在会自动以此创建 |
| password       | string  | 无                 | 密码                                                         |
| periodic_flush |         |                    |                                                              |
| query          | string  | 无                 | 查询elasticsearch的查询字符串                                |
| remove_field   |         |                    |                                                              |
| remove_tag     |         |                    |                                                              |
| result_size    | number  | 1                  | 查询elasticsearch时，返回结果的数量                          |
| sort           | string  | “@timestamp:desc”  | 逗号分隔的“:”列表，用于查询结果排序                          |
| ssl            | boolean | false              | SSL                                                          |
| tag_on_failure |         |                    |                                                              |
| user           | string  | 无                 | 用户名                                                       |



**6）其他插件**

还有很多其他有用插件，如：Split、GeoIP、Ruby，这里就不一一写了，等以后用到再补充



#### 5、常用输出插件

**1）ElasticSearch输出插件**

用于将事件信息写入到Elasticsearch中，官方推荐插件，ELK必备插件

 配置示例

```properties
output {
    elasticsearch {
        hosts => ["127.0.0.1:9200"]
        index => "filebeat-%{type}-%{+yyyy.MM.dd}"
        template_overwrite => true
    }
}
```



> 常用配置参数

| 参数名称 | 类型 | 默认值 | 描述信息 |
| -------- | ---- | ------ | -------- |
|          |      |        |          |



**2）Redis输出插件**

用于将Event写入Redis中进行缓存，通常情况下Logstash的Filter处理比较吃系统资源，复杂的Filter处理会非常耗时，如果Event产生速度比较快，可以使用Redis作为buffer使用



配置示例

```properties
output {
    redis {
        host => "127.0.0.1"
        port => 6379
        data_type => "list"
        key => "logstash-list"
    }
}
```



> 常用配置参数

| 参数名称 | 类型 | 默认值 | 描述信息 |
| -------- | ---- | ------ | -------- |
|          |      |        |          |



**3）File输出插件**

用于将Event输出到文件内

配置示例

```properties
output {
    file {
        path => ...
        codec => line { format => "custom format: %{message}"}
    }
}
```



> 常用配置参数

| 参数名称 | 类型 | 默认值 | 描述信息 |
| -------- | ---- | ------ | -------- |
|          |      |        |          |



**4）TCP输出插件**

通过TCP套接字写入事件。每个事件json由换行符分隔。可以从客户端接受连接，也可以连接到服务器，这取决于mode。

配置示例

```properties
output {
    tcp {
        host => ...
        port => ...
    }
}
```



>  常用配置参数

| 参数名称 | 类型 | 默认值 | 描述信息 |
| -------- | ---- | ------ | -------- |
|          |      |        |          |



#### 6、常用编码插件

**JSON 编码插件**

直接输入预定义好的 JSON 数据，这样就可以省略掉 filter/grok 配置

配置示例

```properties
json {
}
```



>  常用配置参数

| 参数名称 | 类型 | 默认值 | 描述信息 |
| -------- | ---- | ------ | -------- |
|          |      |        |          |



### 五、Logstash 实例

**1、接收Filebeat，输出的Redis**

```properties
input {
    beats {
        port => 5044
    }
}

output {
    redis {
        host => "127.0.0.1"
        port => 6379
        data_type => "list"
        key => "logstash-list"
    }
}
```



**2、读取Redis数据，根据“type”判断，分别处理，输出到ES**

```properties
input {
    redis {
        host => "127.0.0.1"
        port => 6379
        data_type => "list"
        key => "logstash-list"
    }
}

filter {
    if [type] == "application" {
        grok {
            match => ["message", "(?m)-(?<systemName>.+?):(?<logTime>(?>\d\d){1,2}-(?:0?[1-9]|1[0-2])-(?:(?:0[1-9])|(?:[12][0-9])|(?:3[01])|[1-9]) (?:2[0123]|[01]?[0-9]):(?:[0-5][0-9]):(?:(?:[0-5][0-9]|60)(?:[:.,][0-9]+)?)) \[(?<level>(\b\w+\b)) *\] (?<thread>(\b\w+\b)) \((?<point>.*?)\) - (?<content>.*)"]
        }
        date {
            match => ["logTime", "yyyy-MM-dd HH:mm:ss,SSS"]
        }
        json {
            source => "message"
        }
        date {
            match => ["timestamp", "yyyy-MM-dd HH:mm:ss,SSS"]
        }
    }
    if [type] == "application_bizz" {
        json {
            source => "message"
        }
        date {
            match => ["timestamp", "yyyy-MM-dd HH:mm:ss,SSS"]
        }
    }
    mutate {
        remove_field => ["@version", "beat", "logTime"]
    }
}

output {
    stdout{
    }
    elasticsearch {
        hosts => ["127.0.0.1:9200"]
        index => "filebeat-%{type}-%{+yyyy.MM.dd}"
        document_type => "%{documentType}"
        template_overwrite => true
    }
}
```



参考：

https://zhuanlan.zhihu.com/p/398831110

https://blog.csdn.net/chenleiking/article/details/73563930

https://blog.csdn.net/muriyue6/article/details/105517041

