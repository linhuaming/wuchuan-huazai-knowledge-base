## Kibana 简单介绍及入门使用（一）

[TOC]

### 一、Kibana 是什么

​		Kibana是一个开源分析和可视化平台，旨在与Elasticsearch协同工作。您使用Kibana搜索，查看和与存储在Elasticsearch索引中的数据进行交互。您可以轻松执行高级数据分析，并在各种图表，表格和地图中可视化您的数据。

​		Kibana让我们理解大量数据变得很容易。它简单、基于浏览器的接口使你能快速创建和分享实时展示Elasticsearch查询变化的动态仪表盘。安装Kibana非常快，你可以在几分钟之内安装和开始探索你的Elasticsearch索引数据，不需要写任何代码，没有其他基础软件依赖。



### 二、下载、安装

**1、下载地址**

Kibana下载地址：https://www.elastic.co/cn/downloads/past-releases#kibana， 下载对应的版本即可,我下载的是v7.12.1

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531230140816.png)



解压后，目录结构，如下图：

![image-20220531224359019](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531224359019.png)

**2、安装**

修改**config\kibana.yml** 配置文件，此处根据自己实际解压后的路径去找kibana.yml配置文件

![image-20220531224708523](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531224708523.png)



设置elasticsearch.url为启动的elasticsearch(http://localhost:9200/)(其实按照默认可以不用修改配置文件)

![](https://img-blog.csdnimg.cn/20200409131436453.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L211cml5dWU2,size_16,color_FFFFFF,t_70)



进入kibana的bin目录，双击kibana.bat

![image-20220531224917177](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531224917177.png)



访问：http://localhost:5601，出现以下界面即完成安装

![image-20220531225200425](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531225200425.png)



提示我们可以使用示例数据，也可以使用自己已有的数据，我把示例数据都下载了，单击侧面导航中的 Discover 进入 Kibana 的数据探索功能：

![](https://img2020.cnblogs.com/blog/662544/202003/662544-20200316124947014-1689568981.png)



### 三、如何加载自定义索引

接下来演示加载已经创建book索引，单击 Stack Management 选项

![image-20220531230345504](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531230345504.png)



然后单击 Index Patterns 选项。

![image-20220531230431836](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531230431836.png)



点击Create index pattern定义一个新的索引模式。

![image-20220531230538008](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531230538008.png)

在Index pattern 填写索引匹配后，点击Next step

![image-20220531230757870](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531230757870.png)



在这里记得选择@timestamp 字段，后面可以根据日期时间过滤数据，点击Create index pattern

![image-20220531231039438](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531231039438.png)



出来如下界面，列出了所有index中的字段

![image-20220531231234529](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531231234529.png)



点击左侧的discover接下来，我们再来使用一下kibana查看已经导入的索引数据

![image-20220531231501181](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531231501181.png)



可以看到，已经能展示和检索出我们之前导入的数据，奥利给！

![image-20220531231707048](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531231707048.png)





### 四、如何搜索数据

在左边的搜索框输入关键字、右边的时间过滤选择日期，点击Refersh 即可搜索。

可以看到，我们能很方便地搜索栏使用Llucene查，查询语法可以参考[Lucene查询语法汇总](https://www.cnblogs.com/chenqionghe/p/12501218.html) 

![image-20220531232121353](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531232121353.png)



此外还可以使用kibana 的KQL查询语法，点击下面即可开启。如下：

查询语法可以参考[KQL查询语法汇总](https://www.cnblogs.com/-beyond/p/14159002.html) 

![image-20220531232403777](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531232403777.png)



### 五、如何切换中文

在`config/kibana.yml`添加

```json
i18n.locale: "zh-CN"
```



重新启动，即可生效，可以看到已经中文显示了（还是我们中文看着舒服呀！！！）

![image-20220531233241963](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220531233241963.png)





### 六、如何使用控制台

### 七、如何使用可视化

### 八、如何使用仪表盘







参考：

https://www.cnblogs.com/chenqionghe/p/12503181.html