## Spring boot 整合 Logstash

[TOC]

> logstash 的安装本文就不介绍了，着重说 spring boot整合logstash



### 一、概述

​		logstash一般常用的有两种模式，一种是应用服务器写本地日志，在logstash中配置读取本地日志文件，对指定的日志文件进行数据抽取，这种模式一般适用于一般的应用或者系统日志采集到中央日志系统；还有一种模式是tcp网络信息流的模式，直接将信息流直接发送给logstash进行JSON格式化，不需要在应用系统本地先落地文件，这种模式看起来有点像kafka这类消息队列的功能，从消息流传递角度看，确实比较类似。



### 二、步骤

**1、logstash中的config目录下添加  logstash-dev.conf 文件 ，如下图所示**

![image-20220525002503495](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220525002503495.png)



logstash-dev.conf 内容如下：

这里采用的是上面的第二种模式，用tcp传输日志数据。

```json
input {
    # 这里主要是配置toc的ip、port,待会spring boot的logback.xml 配置文件,就填写这个地址
    # 用于推送日志数据到logstash。
	tcp {
		mode => "server"
		host => "127.0.0.1"
		port => 4560
		codec => json_lines
	}
}

output {
    # logstash 输出日志数据到ES，ip、port就填写ES的即可。
	elasticsearch {
		hosts => "localhost:9200"
		index => "springboot-logstash-%{+YYYY.MM.dd}"
	}
}
```



**2、pom.xml 引入依赖**

```xml
 <!-- 集成logstash-->
 <dependency>
     <groupId>net.logstash.logback</groupId>
     <artifactId>logstash-logback-encoder</artifactId>
     <version>5.3</version>
 </dependency>
```



**3、logback.xml 日志配置文件，添加配置**

```xml
<?xml version="1.0" encoding="UTF-8"?>

<!-- scan:当此属性设置为true时，配置文件如果发生改变，将会被重新加载，默认值为true。
     scanPeriod:设置监测配置文件是否有修改的时间间隔，如果没有给出时间单位，默认单位是毫秒。当scan为true时，此属性生效。默认的时间间隔为1分钟。
     debug:当此属性设置为true时，将打印出logback内部日志信息，实时查看logback运行状态。默认值为false。 -->
<configuration scan="true" scanPeriod="60 seconds" debug="false">

    <!-- 上下文变量设置,用来定义变量值,其中name的值是变量的名称，value的值时变量定义的值。
        通过<property>定义的值会被插入到logger上下文中。定义变量后，可以使“${}”来使用变量。 -->
    <property name="CONTEXT_NAME" value="logback-test" />
    <property name="logs.dir" value="./" />

    <!-- 上下文名称：<contextName>, 每个logger都关联到logger上下文，
        默认上下文名称为“default”。但可以使用<contextName>设置成其他名字，用于区分不同应用程序的记录。
        一旦设置，不能修改。 -->
    <contextName>${CONTEXT_NAME}</contextName>

    <!-- <appender>是<configuration>的子节点，是负责写日志的组件。
        有两个必要属性name和class。
        name指定appender名称，
        class指定appender的实现类。 -->
    <appender name="stdout" class="ch.qos.logback.core.ConsoleAppender">
        <!-- 对日志进行格式化。 -->
        <encoder>
            <pattern>
                %d{yyyy-MM-dd HH:mm:ss.SSS}|%level|%class|%thread|[%X{TRACE_ID}]|%method|%line|%msg%n
            </pattern>
            <!-- 日志格式化示例 -->
            <!--
            日期时间
            2022-05-12 23:58:38.711
            日志级别
            |INFO
            类路径
            |com.linhuaming.springboot.mybatisplus.demo.controller.DemoController

            线程信息、方法名、行数
            |http-nio-8088-exec-2|demo2|32

            日志信息
            |demo2(),user:User{id=null, username='null', password='null', sex=null, info='null', age=null, version=null}
            -->
        </encoder>
    </appender>

    <appender name="file" class="ch.qos.logback.core.rolling.RollingFileAppender">
        <!-- 被写入的文件名，可以是相对目录，也可以是绝对目录，如果上级目录不存在会自动创建，没有默认值。 -->
        <file>${logs.dir}/logback-test.log</file>

        <!-- 当发生滚动时的行为  -->
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <!-- 必须包含“%i”例如，假设最小值和最大值分别为1和2，命名模式为 mylog%i.log,会产生归档文件mylog1.log和mylog2.log。还可以指定文件压缩选项，例如，mylog%i.log.gz 或者 没有log%i.log.zip -->
            <FileNamePattern>${logs.dir}/logback-test.%i.log</FileNamePattern>
            <!-- 窗口索引最小值 -->
            <minIndex>1</minIndex>
            <!-- 窗口索引最大值 -->
            <maxIndex>1</maxIndex>
        </rollingPolicy>

        <!-- 激活滚动的条件。 -->
        <triggeringPolicy class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <!-- 活动文件的大小，默认值是10MB -->
            <maxFileSize>30MB</maxFileSize>
        </triggeringPolicy>

        <!-- 对记录事件进行格式化。 -->
        <encoder>
            <charset>UTF-8</charset>
            <Pattern>
                %d{yyyy-MM-dd HH:mm:ss.SSS}|%level|%class|%thread|%method|%line|%msg%n
            </Pattern>
        </encoder>
    </appender>


    <!-- logstash配置 -->
    <appender name="LOGSTASH" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
        <destination>127.0.0.1:4560</destination>
        <!-- 日志输出编码 -->
        <encoder charset="UTF-8" class="net.logstash.logback.encoder.LoggingEventCompositeJsonEncoder">
            <providers>
                <timestamp>
                    <timeZone>UTC</timeZone>
                </timestamp>
                <pattern>
                    <pattern>
                        {
                        "logLevel": "%level",
                        "serviceName": "${springAppName:-}",
                        "pid": "${PID:-}",
                        "thread": "%thread",
                        "class": "%logger{40}",
                        "rest": "%message"
                        }
                    </pattern>
                </pattern>
            </providers>
        </encoder>
        <!--<encoder charset="UTF-8" class="net.logstash.logback.encoder.LogstashEncoder"/>-->
    </appender>

    <!-- 特殊的<logger>元素，是根logger。只有一个level属性，因为已经被命名为"root".
        level:设置打印级别，大小写无关：TRACE, DEBUG, INFO, WARN, ERROR, ALL 和 OFF，不能设置为INHERITED或者同义词NULL。默认是DEBUG。
        <root>可以包含零个或多个<appender-ref>元素，标识这个appender将会添加到这个loger。 -->
    <root>
        <level value="INFO" />
        <appender-ref ref="stdout" />
        <appender-ref ref="file" />
        <appender-ref ref="LOGSTASH"/>  <!--添加输出源logstash-->
    </root>

    <!-- 用来设置某一个包或者具体的某一个类 的日志打印级别、以及指定<appender>,
        name:用来指定受此logger约束的某一个包或者具体的某一个类。
        level:用来设置打印级别，大小写无关：TRACE, DEBUG, INFO, WARN, ERROR, ALL 和 OFF，还有一个特殊值INHERITED或者同义词NULL，代表强制执行上级的级别。如果未设置此属性，那么当前loger将会继承上级的级别。
        additivity:是否向上级logger传递打印信息。默认是true。(这个logger的上级就是上面的root)
        <logger>可以包含零个或多个<appender-ref>元素，标识这个appender将会添加到这个logger。-->
    <logger name="com.linhuaming.springboot.mybatisplus.demo.controller.DemoController" level="INFO" additivity="true"></logger>

</configuration>

```

​		

​		成功启动项目， 访问项目，触发 log 的输出，在 kibana 页面如果可以看到 springboot-logstash-xxx 的索引，就已经成功一半了，接下来，在 kibana 页面创建索引就行了。

