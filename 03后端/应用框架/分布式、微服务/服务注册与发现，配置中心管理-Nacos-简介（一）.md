## 服务注册与发现，配置中心管理-Nacos-简介（一）

[TOC]

### 一、简介

​	Nacos是什么？好像没听过，不要紧。那Eureka听说过吧，在Spring Cloud中做服务注册中心组件，类似的还有Zookeeper、Consul。

​	所以Nacos也是一个注册中心组件咯，当然是，不过它不仅仅是注册中心。

​	Nacos也是一个配置中心，比如Spring Cloud Config，将配置文件版本化管理。

​	那么Nacos到底是什么呢, 总结为官网一句话就是:

​	Nacos是阿里巴巴开源的一款支持服务注册与发现，配置管理以及微服务管理的组件。用来取代以前常用的注册中心（zookeeper , eureka等等），以及配置中心（spring cloud config等等）。Nacos是集成了注册中心和配置中心的功能，做到了二合一。	



#### 1、Nacos的发展历程

​	首先要说Nacos的发展历程就要从阿里巴巴的内部产品ConfigServer说起了，因为Nacos是ConfigServer 的开源实现

​	早在2008年阿里就开始服务化的进程(那个时候我好像还在上小学啊)，在那个时候阿里内部自研的服务发现解决方案就叫做ConfigServer

​	ConfigServer经历了十年的发展从V1.0的单机版演变为目前对外公布的V4.0集群版。

​	2018年7月阿里巴巴高级技术专家许真恩（慕义）发布了Nacos首个开源版本V0.1.0，Nacos作为ConfigServer的开源实现截止目前已经更新到了V1.0.1的大版本，并且支持大规模生产版本。



#### 2、Nacos能做什么

- 服务注册发现和服务健康检测
- 动态配置服务
- 动态DNS服务
- 服务及元数据管理

