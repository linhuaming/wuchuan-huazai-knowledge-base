## Nacos 服务注册与发现（四）

[TOC]



### 一、注册中心示意图

**一幅图读懂Nacos作为注册中心的基本运用**

![img](../../../img/2021101923090001.jpg)

​	

同为注册中心，其实跟ZooKeeper和Eureka是差不多的，但Nacos功能更加强大！！



### 二、搭建工程

我的结构目录：

<img src="../../../img/image-20211019231437061.png" alt="image-20211019231437061" style="zoom:50%;" />

忽略common-api-openfeign、nacos-config、server-gateway，重点关注nacos的服务提供者和消费者。

先看看官方的版本说明：[alibaba/spring-cloud-alibaba](https://link.zhihu.com/?target=https%3A//github.com/alibaba/spring-cloud-alibaba/wiki/%E7%89%88%E6%9C%AC%E8%AF%B4%E6%98%8E)

<img src="../../../img/202110292317000002.jpg" alt="preview" style="zoom:50%;" />



先介绍一下我的环境：

- jdk：1.8
- maven :  3.5.2
- spring cloud :  Finchley.SR2
- spring cloud alibaba :  2.0.4.RELEASE

- springboot version :  2.0.4.RELEASE



#### 1、建立父工程

至于maven 搭建父项目，我这里就不出教程了，大家可以自行百度一下。。。

spring-cloud-alibaba-demo 的pom.xml 如下：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.0.4.RELEASE</version>
        <!--<relativePath/> -->
        <!-- lookup parent from repository -->
    </parent>

    <groupId>org.linhuaming</groupId>
    <artifactId>spring-cloud-alibaba-demo</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>
    <modules>
        <module>nacos-config</module>
        <module>nacos-consumer</module>
        <module>nacos-provider</module>
        <module>common-api-openfeign</module>
        <module>server-gateway</module>
    </modules>


    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
        <java.version>1.8</java.version>
        <spring-cloud.version>Finchley.SR2</spring-cloud.version>
        <spring-cloud-alibaba.version>2.0.4.RELEASE</spring-cloud-alibaba.version>
    </properties>

    <dependencies>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-dependencies</artifactId>
            <version>${spring-cloud.version}</version>
            <type>pom</type>
            <scope>import</scope>
        </dependency>

    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>2.0.4.RELEASE</version>
            </plugin>
        </plugins>
    </build>



</project>
```



#### 2、建立子工程nacos-provider 服务提供者

在spring-cloud-alibaba-demo工程下新建module命名nacos-provider，工程结构如下：

<img src="../../../img/image-20211019233056929.png" alt="image-20211019233056929" style="zoom:50%;" />

**pom.xml 如下：**

下面的spring cloud alibaba sentinel 依赖大家，现在没有用到可以去掉先。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>spring-cloud-alibaba-demo</artifactId>
        <groupId>org.linhuaming</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>nacos-provider</artifactId>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

        <!-- spring cloud alibaba sentinel 依赖 -->
        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-sentinel</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

    </dependencies>

</project>
```



**bootstrap.yml 配置文件：**

```yml
server:
  port: 8070
spring:
  application:
    name: nacos-provider
  cloud:
    nacos:
      discovery:
        # nacos服务地址
        server-addr: 127.0.0.1:8848
      # namespace: 9b04b97d-d133-4788-9ce6-b09df640b83f
```



**NacosProviderApplication 启动类：**

```java
package com.linhuaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableDiscoveryClient // // 开启服务发现-nacos客户端
public class NacosProviderApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext =
                SpringApplication.run(NacosProviderApplication.class, args);
        Environment env = applicationContext.getEnvironment();

        String port = env.getProperty("server.port");
        System.out.println("server port:"+port);
    }

}
```



**ProviderController 控制器类：**

```java
package com.linhuaming.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/provider")
public class ProviderController {

    @GetMapping("/hello")
    public String hello(){
        String msg = "hello provider change";
        return msg;
    }

}
```



主启动类main方法：

看到如下打印信息，则启动成功！

![image-20211019234527586](../../../img/image-20211019234527586.png)



接着我们会在nacos控制台的服务列表看到我们的nacos-provider服务：

![image-20211019234651139](../../../img/image-20211019234651139.png)



现在我们去浏览器调用一下看看：

访问 http://localhost:8070/provider/hello

![image-20211019235119662](../../../img/image-20211019235119662.png)



#### 3、建立子工程nacos-consumer 服务消费者

同样新建子module，nacos-consumer结构如下：

<img src="../../../img/image-20211019235431002.png" alt="image-20211019235431002" style="zoom:50%;" />

**pom.xml 配置文件：**

依赖(common-api-openfeign这个先不用管，可以先注释掉)：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>spring-cloud-alibaba-demo</artifactId>
        <groupId>org.linhuaming</groupId>
        <version>1.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>nacos-consumer</artifactId>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>com.alibaba.cloud</groupId>
            <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
            <version>2.0.4.RELEASE</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-commons</artifactId>
            <version>2.0.4.RELEASE</version>
            <scope>compile</scope>
        </dependency>

        <!-- 引入common-api-openfeign -->
        <dependency>
            <groupId>org.linhuaming</groupId>
            <artifactId>common-api-openfeign</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-openfeign-core</artifactId>
            <version>2.0.4.RELEASE</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
        </dependency>

    </dependencies>

</project>
```



**bootstrap.yml 配置文件：**

```yaml
server:
  port: 8060
spring:
  application:
    name: nacos-consumer
  cloud:
    nacos:
      discovery:
        # nacos服务地址
        server-addr: 127.0.0.1:8848
```



**NacosConsumerApplication 启动类：**

```java
package com.linhuaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
@EnableDiscoveryClient // 开启服务发现-nacos 客户端
public class NacosConsumerApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext =
            SpringApplication.run(NacosConsumerApplication.class, args);

        Environment env = applicationContext.getEnvironment();
        String port = env.getProperty("server.port");
        System.out.println("server port:"+port);
    }

}
```



**ConsumerController 控制器类：**

```java
package com.linhuaming.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/consumer")
public class ConsumerController {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Autowired
    RestTemplate restTemplate;

    @RequestMapping(value = "/test")
    public String echoRemoteEcho() {
        // 使用restTemplate url拼写规则:  传输协议://服务名/访问路径
        return "provider echo is:" + restTemplate.
                getForObject("http://nacos-provider/provider/hello",String.class);

    
    }

}
```



启动主启动类：

看到如下打印信息，则启动成功！

![image-20211020001335490](../../../img/image-20211020001335490.png)



接着我们会在nacos控制台的服务列表看到我们的nacos-consumer服务：

![image-20211020001425902](../../../img/image-20211020001425902.png)



在浏览器调用接口：

访问 http://localhost:8060/consumer/test

![image-20211020001621067](../../../img/image-20211020001621067.png)



**补充：**

如果nacos配置了集群，服务要向集群注册的话，配置里面注册地址以逗号隔开

```properties
spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848,127.0.0.1:8850,127.0.0.1:8852
```





参考：  https://zhuanlan.zhihu.com/p/363513172