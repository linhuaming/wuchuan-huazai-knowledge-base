## Spring 简单介绍（一）

[TOC]

### 一、概述

#### 1、什么是spring

​		Spring是一个开源框架，Spring是于2003 年兴起的一个轻量级的Java 开发框架，由Rod Johnson 在其著作Expert One-On-One J2EE Development and Design中阐述的部分理念和原型衍生而来。它是为了解决企业应用开发的复杂性而创建的。框架的主要优势之一就是其分层架构，分层架构允许使用者选择使用哪一个组件，同时为 J2EE 应用程序开发提供集成的框架。Spring使用基本的JavaBean来完成以前只可能由EJB完成的事情。然而，Spring的用途不仅限于服务器端的开发。从简单性、可测试性和松耦合的角度而言，任何Java应用都可以从Spring中受益。Spring的核心是控制反转（IoC）和面向切面（AOP）。简单来说，Spring是一个分层的JavaSE/EE full-stack(一站式) 轻量级开源框架。



#### 2、spring的核心

Spring的核心是**控制反转（IoC）**和**面向切面（AOP）**

- IOC (Inversion of control): [控制反转](https://www.baidu.com/s?wd=控制反转&tn=44039180_cpr&fenlei=mv6quAkxTZn0IZRqIHckPjm4nH00T1YYuHF9uhDdnWfzmvDsuHuB0ZwV5Hcvrjm3rH6sPfKWUMw85HfYnjn4nH6sgvPsT6KdThsqpZwYTjCEQLGCpyw9Uz4Bmy-bIi4WUvYETgN-TLwGUv3EnHn3PjDYP1cLnjn4Pj0srHbYn0)控制权由对象本身转向容器；由容器根据配置文件去创建实例并创建各个实例之间的依赖关系。
- AOP(Aspect Oriented Programming)，也就是面向方面编程的技术。AOP基于IoC基础，是对OOP的有益补充。



#### 3、spring优点

**1、IOC/DI方便解耦，简化开发 （高内聚低耦合）**

- Spring就是一个大工厂（容器），可以将所有对象创建和依赖关系维护，交给Spring管理
- spring工厂是用于生成bean

**2、 AOP编程的支持**

- Spring提供面向切面编程，可以方便的实现对程序进行权限拦截、运行监控等功能

**3、声明式事务的支持**

- 只需要通过配置就可以完成对事务的管理，而无需手动编程

**4、方便程序的测试**

- Spring对Junit4支持，可以通过注解方便的测试Spring程序

**5、方便集成各种优秀框架**

- Spring不排斥各种优秀的开源框架，其内部提供了对各种优秀框架（如：Struts、Hibernate、MyBatis、Quartz等）的直接支持

**6、降低JavaEE API的使用难度**

- Spring 对JavaEE开发中非常难用的一些API（JDBC、JavaMail、远程调用等），都提供了封装，使这些API应用难度大大降低



#### 4、spring 体系结构

![](https://images2017.cnblogs.com/blog/1118963/201708/1118963-20170816115752584-578966124.png)



- **Data Access/Integration层**包含有JDBC、ORM、OXM、JMS和Transaction模块。
- **Web层**包含了Web、Web-Servlet、WebSocket、Web-Porlet模块。
- **AOP模块**提供了一个符合AOP联盟标准的面向切面编程的实现。
- **Core Container(核心容器)：**包含有Beans、Core、Context和SpEL模块。
- **Test模块**支持使用JUnit和TestNG对Spring组件进行测试。



### 二、什么是IOC

​		IOC (Inversion of Control) : 控制反转， 是一个理论，概念，思想。把对象的创建，赋值，管理工作都交给代码之外的容器实现， 也就是对象的创建是有其它外部资源完成，这样做实现了与解耦合。

​		正转：对象的创建、赋值等操作交由程序员手动完成，即使用类似new Xxx(Xxx Xxx)、Xxx.setXxx()语句完成对象的创建与赋值，缺点是一旦程序功能发生改变，涉及到的类就要修改代理，耦合度高，不便于维护和管理。

​		反转：对象的创建、赋值等操作交由代码之外的容器实现，有容器代替程序员完成对象的创建、赋值；且当程序功能发生变化时，只需要修改容器的配置文件即可。



### 三、什么是AOP

AOP称为面向切面编程，在程序开发中主要用来解决一些系统层面上的问题，比如日志，事务，权限等待，Struts2的拦截器设计就是基于AOP的思想，是个比较经典的例子。



#### 1、基本概念

- Aspect(切面):通常是一个类，里面可以定义切入点和通知
- JointPoint(连接点):程序执行过程中明确的点，一般是方法的调用
- Advice(通知):AOP在特定的切入点上执行的增强处理，有before,after,afterReturning,afterThrowing,around
- Pointcut(切入点):就是带有通知的连接点，在程序中主要体现为书写切入点表达式
- AOP代理：AOP框架创建的对象，代理就是目标对象的加强。Spring中的AOP代理可以使用JDK动态代理，也可以是CGLIB代理，前者基于接口，后者基于子类。



#### 2、基于注解的AOP配置方式

- **启用@AsjectJ支持**

在applicationContext.xml中配置下面一句:

```xml
<aop:aspectj-autoproxy />
```



- **通知类型介绍**

1. @Before:在目标方法被调用之前做增强处理,@Before只需要指定切入点表达式即可

2. @AfterReturning:在目标方法正常完成后做增强,@AfterReturning除了指定切入点表达式后，还可以指定一个返回值形参名returning,代表目标方法的返回值

3. @AfterThrowing:主要用来处理程序中未处理的异常,@AfterThrowing除了指定切入点表达式后，还可以指定一个throwing的返回值形参名,可以通过该形参名

来访问目标方法中所抛出的异常对象

4. @After:在目标方法完成之后做增强，无论目标方法时候成功完成。@After可以指定一个切入点表达式

5. @Around:环绕通知,在目标方法完成前后做增强处理,环绕通知是最重要的通知类型,像事务,日志等都是环绕通知,注意编程中核心是一个ProceedingJoinPoint



参考：

https://www.cnblogs.com/zqy-blogzone/p/7372871.html

https://blog.csdn.net/mms520www/article/details/109330160

