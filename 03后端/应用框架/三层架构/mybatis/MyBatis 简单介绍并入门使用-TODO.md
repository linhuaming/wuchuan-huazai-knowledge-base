## MyBatis 简单介绍并入门使用

[TOC]

### 一、介绍

- MyBatis，原名 iBatis，由原来的 apache 迁移到了谷歌，更名为 MyBatis。MyBatis 指的就是 iBatis 3.x 后的版本。

- MyBatis 是采用 ORM（Object Relational Mapping，对象关系映射）思想实现的持久化层框架，之前学习持久层解决方案 JDBC、JdbcTemplate、DBUtil都不是框架，JDBC 是一种规范，JdbcTemplate 和 DBUtil 都是简单封装了 JDBC 的工具类。

- MyBatis 支持自定义 SQL、存储过程以及高级映射，免除了几乎所有的 JDBC 代码以及设置参数和获取结果集的工作。可以通过简单的 XML 或注解把原始类型、接口和 Java POJO(Plain Old Java Objects，普通老式 Java 对象) 映射为数据库中的记录。

- 使用 MyBatis 框架，让开发人员专注于 SQL 的编写，不再去关注注册驱动、获取连接、执行 SQL、释放资源等操作，极大简化了持久层开发工作。



### 二、特性

- MyBatis 是半自动 ORM 框架。

- 将 SQL 定制抽取出来，以配置文件的方式交由开发人员编写。

- 封装了 预编译、设置参数、执行 SQL、封装结果等操作。

- 实现了 SQL 与 Java 编码分离。

- 只需要掌握 SQL 和 MyBatis 相关配置就可以使用，学习成本相对更低。

- 由于 SQL 需要由开发人员编写，方便维护与优化。

- 相比于 Hibernate 更加轻量级。



### 三、JDBC基础

**JDBC代码示例:**

```java
package com.linhuaming.mybatis.demo.test;

import java.sql.*;

public class JDBCTest {

    public static void main(String[] args) throws SQLException {
        Connection connection = null;
        PreparedStatement prepareStatement = null;
        ResultSet rs = null;
        try {
            // 加载驱动
            Class.forName("com.mysql.jdbc.Driver");

            // 获取连接
            String url = "jdbc:mysql://127.0.0.1:48923/db_test";
            String user = "root";
            String password = "root";
            connection = DriverManager.getConnection(url, user, password);

            // 编写sql
            String sql = "select * from `user` where id=?";
            prepareStatement = connection.prepareStatement(sql);
            // 设置参数
            prepareStatement.setInt(1,2);
            // 执行查询
            rs = prepareStatement.executeQuery();
            while (rs.next()) {
                System.out.print(rs.getString("id")+" ");
                System.out.print(rs.getString("username_")+" ");
                System.out.print(rs.getString("password_")+" ");
                System.out.print(rs.getInt("sex_")+" ");
                System.out.print(rs.getString("info_")+" ");
                System.out.print(rs.getInt("age_")+" ");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            // 关闭连接，释放资源
            if (rs != null) {
                rs.close();
            }
            if (prepareStatement != null) {
                prepareStatement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }

    }

}
```



**JDBC 存在问题点：**

![](https://img2018.cnblogs.com/blog/1456626/201903/1456626-20190327200758073-1117780956.png)



参考

https://mybatis.org/mybatis-3/zh/index.html

https://www.cnblogs.com/diffx/p/10611082.html

https://blog.csdn.net/qq_44713454/article/details/103020636