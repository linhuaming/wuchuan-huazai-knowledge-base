## spring boot实现统一异常处理

[TOC]

### 一、介绍

​		在基于SpringBoot的Web应用中，对于Http请求处理过程中发生的各种错误，如常见的400、404和500等错误，SpringBoot默认提供了一种映射到错误页面/error的机制来处理所有的错误，并且该页面也由SpringBoot默认提供，不需要开发者自己编写。

​		该页面会显示请求的错误状态码， 以及一些错误原因和消息，如下图分别为SpringBoot默认提供的404错误和500错误页面：



![](https://img-blog.csdnimg.cn/20200813171420472.png)



![](https://img-blog.csdnimg.cn/20200813171436824.png)



​		上述/error错误页面路径可以理解为SpringBoot默认为我们写了一个模版错误页面，然后默认还写了一个Controller，该Controller中包含一个/error请求地址映射指向该错误页面。当SpringBoot的错误处理机制捕获到请求异常之后，则会将用户的原请求携带上错误信息，然后转发到这个/error页面，页面再显示错误的相关信息。

​		虽然SpringBoot提供了默认的错误显示页面，但是仅使用该默认错误页面会存在大量的局限性：

- 该页面比较简陋，对于用户而言并不友好。
- 500错误暴露了服务器的详细出错原因，存在严重安全隐患。
- 在前后端分离的项目中，客户端需要的不是页面，而是JSON数据。



### 二、spring boot 统一异常处理的几种方法

#### 1、自定义错误页面

https://www.cnblogs.com/youcoding/p/13186489.html

https://blog.csdn.net/han12398766/article/details/117596232



2、实现ErrorController接口（已完成）

3、@ControllerAdvice+@ExceptionHandler注解（已完成）

4、 配置SimpleMappingExceptionResolver类

5、自定义HandlerExceptionResolver类



参考：

https://m.w3cschool.cn/article/96456065.html

https://www.cnblogs.com/leeego-123/p/11338063.html

https://blog.csdn.net/MrKorbin/article/details/107982350