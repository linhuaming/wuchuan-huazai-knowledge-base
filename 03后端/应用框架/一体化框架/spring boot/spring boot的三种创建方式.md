## spring boot的三种创建方式

[TOC]

**工程创建的三种方式：**

1、在线创建

2、通过IDE来创建（Intellij IDEA、STS)

3、通过改造一个普通的maven工程来实现



### 一、在线创建

spring boot官网提供了在线创建的方式，官网地址：https://start.spring.io/，创建如下图：



![img](https://img-blog.csdnimg.cn/20190829103929860.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3piZl96YmY=,size_16,color_FFFFFF,t_70)

创建完spring boot工程会下载一个后缀.zip的文件，解压以后，直接在IDE工具打开就可直接使用



### 二、IDEA创建（这里只演示IDEA创建）

1、点击 spring initializr ， 右边选择Default 

![img](https://img-blog.csdnimg.cn/20190829104557441.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3piZl96YmY=,size_16,color_FFFFFF,t_70)



2、这里可以发现IDEA创建spring boot工程，选择Spring Initializr与spring boot官网上在线创建大同小异

![img](https://img-blog.csdnimg.cn/20190829105036853.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3piZl96YmY=,size_16,color_FFFFFF,t_70)



3、与spring boot官网基本一样，接下来next，最后选择项目路径，点击finish即可。

![img](https://img-blog.csdnimg.cn/20190829105232839.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3piZl96YmY=,size_16,color_FFFFFF,t_70)



### 三、正常创建maven工程改造成spring boot项目

项目结构：

![](https://img-blog.csdnimg.cn/bf6910a7ae07457fa2b298b5d0c15ad3.png)



1、pom文件，引入依赖parent和spring-boot-start-web

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.4.9</version>
    </parent>

    <groupId>org.linhuaming</groupId>
    <artifactId>spring-security-oauth2-demo</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>8</maven.compiler.source>
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>

    </dependencies>

</project>
```



2、创建spring boot的配置文件，application.yml

```yml
server:
  port: 8082 #配置tomcat端口
```



3、创建启动类

```java
package org.linhuaming;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthApplication.class, args);
    }

}
```



4、创建控制器类

```java
package org.linhuaming.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @GetMapping(value = "/demo")
    public String demo(){
        String msg = "this is demo";
        return msg;
    }
}
```



最后，双击启动类OauthApplication，看到如下信息，即启动成功！！！

![](https://img-blog.csdnimg.cn/a3bdec7552cd41b09c9aa8ac0a792587.png)



打开浏览器，访问下链接    http://localhost:8082/demo

![](https://img-blog.csdnimg.cn/2e7ed6063fda4ce0928a06cbd425276e.png)







