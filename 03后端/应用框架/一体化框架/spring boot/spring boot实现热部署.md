## spring boot 实现热部署

[TOC]

### 一、热部署/热加载

**热部署（Hot Deploy）：**

​		热部署针对的是容器或者是整个应用，部署了新的资源或者修改了一些代码，需要在不停机的情况下的重新加载整个应用。



**热加载（Hot Swap）：**

​		热加载针对的是单个[字节码](https://so.csdn.net/so/search?q=字节码&spm=1001.2101.3001.7020)文件，指的是重新编译后，不需要停机，应用程序就可以加载使用新的class文件。



### 二、spring boot 热部署原理

​		 springBoot热部署原理是：当我们使用编译器启动项目后，在编译器上修改了代码后，编译器会将最新的代码编译成新的.class文件放到classpath下;而引入的spring-boot-devtools插件,插件会监控classpath下的资源，当classpath下的资源改变后，插件会触发重启；

**而重启为什么速度快于我们自己启动呢？**

​		我们自己启动的时候，是加载项目中所有的文件（自己编写的文件 + 所有项目依赖的jar）

​		而加入了spring-boot-devtools插件依赖后，我们自己编写的文件的类加载器为org.springframework.boot.devtools.restart.classloader.RestartClassLoader,是这个工具包自定义的类加载器, 项目依赖的jar使用的是JDK中的类加载器(AppClassLoader\ExtClassLoader\引导类加载器)

​		在插件触发的重启中，只会使用RestartClassLoader来进行加载(即：只加载我们自己编写的文件部分)



### 三、spring boot devtool 实现热部署

#### 1、pom.xml 文件添加依赖

```xml
  <!--快重启-->
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-devtools</artifactId>
      <optional>true</optional>  <!-- 不能被其它模块继承，如果多个子模块可以去掉  不必须 -->
      <scope>runtime</scope>     <!-- 只在运行时起作用  打包时不打进去 -->
</dependency>
  
  <build>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <configuration>
          <fork>true</fork>
          <addResources>true</addResources>
        </configuration>
      </plugin>
    </plugins>
  </build>

```



#### 2、IDEA 中设置

- 在Settings→Build→Compiler中将Build project automatically勾选。
- 最后按ctrl+shift+alt+/ 选择registy，将compiler.automake.allow.when.app.running勾选。



### 四、 Devtools 配置

application.yml中配置一下devtools

```yaml
spring:
  devtools:
    restart:
      enabled: true  #设置开启热部署
      additional-paths: src/main/java #重启目录
      exclude: WEB-INF/**  #排除文件(不重启项目)
  freemarker:
    cache: false    #页面不加载缓存，修改即时生效
```



参考：

https://blog.csdn.net/chachapaofan/article/details/88697452/