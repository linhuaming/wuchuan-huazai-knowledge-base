## 计算机编程中的编码-简介



### 0、编码由来的历史

https://blog.csdn.net/qq_39204474/article/details/77971644



### 1、基本概念

**字符：**

字符是指计算机中使用的字母、数字、字和符号，包括：1、2、3、A、B、C、~！·#￥%……—*（）——+等等。



**字符集：**

一组抽象字符的集合。字符集常常和一种具体的语言文字对应起来， 该文字中的所有字符或者大部分常用字符就构成了该文字的字符集。比如：英文字符集，中文字符集等。



**编码字符集：**

将字符内的字符排序，然后和二进制数字对应起来，这个字符集叫做被编码过的字符集（编码字符集），说白了就是字符集为每个字符分配了一个唯一的编号，通过这个编号就能找到对应的字符。如：ASCII字符集、GB2312字符集、Unicode字符集等，这是字符集的另一种含义。我们通常所说的字符集是这个含义。

<img src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fw5.sanwen8.cn%2Fmmbiz_jpg%2FFJalMJu8HqMJutO1EvCf1aTYlx6VAyiaiclCS5dSEYDzrlDmzvib9gmLk4SBpvmYI9r8Dm5iaicy1kpCmIJJWu5FYbA%2F0%3Fwx_fmt%3Djpeg&amp;refer=http%3A%2F%2Fw5.sanwen8.cn&amp;app=2002&amp;size=f9999,10000&amp;q=a80&amp;n=0&amp;g=0n&amp;fmt=jpeg?sec=1623515187&amp;t=593ad3524989bab1852bf1cc31d8854b" alt="img" style="zoom:50%;" />





**字符编码：**

其实就是**编码字符**在计算机上的存储方式。一些字符集，如：ASCII字符集、GB2312字符集等不仅代表编码的字符集，也是默认的编码方式；而Unicode字符集有UTF-32、UTF-16、UTF-8等编码方式。

字符集为每个字符分配了一个唯一的编号，通过这个编号就能找到对应的字符。在编程过程中我们经常会使用字符，而使用字符的前提就是把字符放入内存中，毫无疑问，放入内存中的仅仅是字符的编号，而不是真正的字符实体。



### 2、编码分类

https://blog.csdn.net/u013978512/article/details/88682772

编码主要有两类，一类是非Unicode编码，一类是Unicode编码。下面主要介绍下

####  非Unicode编码

包括ASCII、ISO8859-1、windows-1252、GB2312、GBK、GB18030、Big5等。 



#### Unicode编码

- Unicode，全称：Universal Code，是一个由“Unicode学术学会”机构制定的字符编码系统，开始于上世纪80年代

- Unicode 可以使用的编码有三种，分别是：

  UFT-8：一种变长的编码方案，使用 1~6 个字节来存储；
  UFT-32：一种固定长度的编码方案，不管字符编号大小，始终使用 4 个字节来存储；
  UTF-16：介于 UTF-8 和 UTF-32 之间，使用 2 个或者 4 个字节来存储，长度既固定又可变。













参考：

https://blog.csdn.net/qq_39204474/article/details/77971644

https://blog.csdn.net/zrj000za/article/details/54942825

https://blog.csdn.net/u013978512/article/details/88682772

