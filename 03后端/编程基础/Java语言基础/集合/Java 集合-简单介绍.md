## Java 集合-简单介绍

[TOC]

### 一、为什么需要使用集合

​		当我们需要保存一组类型相同的数据的时候，我们应该是用一个容器来保存，这个容器就是数组，但是，使用数组存储对象具有一定的弊端， 因为我们在实际开发中，存储的数据的类型是多种多样的，于是，就出现了“集合”，集合同样也是用来存储多个数据的。

​		数组的缺点是一旦声明之后，长度就不可变了；同时，声明数组时的数据类型也决定了该数组存储的数据的类型；而且，数组存储的数据是有序的、可重复的，特点单一。 但是集合提高了数据存储的灵活性，Java 集合不仅可以用来存储不同类型不同数量的对象，还可以保存具有映射关系的数据。



### 二、集合的介绍

Java 集合， 也叫作容器，主要是由两大接口派生而来：一个是 `Collection`接口，主要用于存放单一元素；另一个是 `Map` 接口，主要用于存放键值对。对于`Collection` 接口，下面又有三个主要的子接口：`List`、`Set` 和 `Queue`。

Java 集合框架如下图所示：

![](https://javaguide.cn/assets/java-collection-hierarchy.1727461b.png)



**分析下集合的特点、数据结构、线程安全问题：**

![](https://img-blog.csdn.net/20150501232625059?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvaGFvdmlwMTIz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)



#### List接口

特点：存储的元素是有序的、可重复的。

- Vector：有序的、可重复的、查询快、增删除慢，数据结构是数组，线程安全。
- ArrayList：有序的、可重复的、查询快、增删除慢，数据结构是数组，线程不安全。
- LinkedList：有序的、可重复的、查询慢、增删快，数据结构是双向链表，线程不安全。



#### Set接口

特点：存储的元素是无序的、不可重复的（唯一的）。

- HashSet：无序的、不可重复的，数据结构是哈希表，线程不安全
- LinkedHashSet：有序的（顺序满足 FIFO）、不可重复的，数据结构是链表+哈希表，线程不安全
- TreeSet：有序的（自然排序、自定义排序）、不可重复的，数据结构是二叉树，线程不安全



#### Queue接口

特点：按特定的排队规则来确定先后顺序，存储的元素是有序的、可重复的。

- PriorityQueue：有序的、可重复的，数据结构是二叉堆，线程不安全。

- ArrayDeque：有序的（优先级相关的）、可重复的，数据结构是数组+双指针，线程不安全。



#### Map接口

特点：使用键值对（key-value）存储，key 是不可重复的，value 是可重复的。

- Hashtable：key和value不支持null值、效率慢，数据结构是数组+链表，线程安全
- HashMap：key和value支持null值、效率快，数据结构是数组+链表->红黑树，线程不安全
- LinkedHashMap：支持顺序，数据结构是数组+链表，线程不安全
- TreeMap：支持排序，数据结构是红黑树，线程不安全

map详细介绍，请参考：https://javaguide.cn/java/collection/java-collection-questions-02.html



### 三、如何选用集合

​		主要根据集合的特点来选用，比如我们需要根据键值获取到元素值时就选用 `Map` 接口下的集合，需要排序时选择 `TreeMap`,不需要排序时就选择 `HashMap`,需要保证线程安全就选用 `ConcurrentHashMap`，其实用Hashtable也可以保证线程安全问题，但是效率慢，不适合并发高的情况下用。

​		当我们只需要存放元素值时，就选择实现`Collection` 接口的集合，需要保证元素唯一时选择实现 `Set` 接口的集合比如 `TreeSet` 或 `HashSet`。

​		不需要就选择实现 `List` 接口的比如 `ArrayList` 或 `LinkedList`，然后再根据实现这些接口的集合的特点来选用。



参考：

https://javaguide.cn/java/collection/java-collection-questions-01.html







