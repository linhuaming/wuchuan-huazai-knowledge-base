## Stream API 简单介绍-入门使用



[TOC]

### 一、为什么要使用Stream API

​		实际开发中，项目中多数数据源都来自于Mysql，Oracle等。但现在数据源可以更多了，有MongDB，Radis等，而这些NoSQL的数据就需要Java层面去处理。

​		Stream 和 Collection 集合的区别：Collection 是一种静态的内存数据结构，而 Stream 是有关计算的。前者是主要面向内存，存储在内存中，后者主要是面向 CPU，通过 CPU 实现计算。



### 二、什么是Stream

Stream（流）是一个来自数据源的元素队列并支持聚合操作

- 元素是特定类型的对象，形成一个队列。 Java中的Stream并不会存储元素，而是按需计算。
- **数据源** 流的来源。 可以是集合，数组，I/O channel， 产生器generator 等。
- **聚合操作** 类似SQL语句一样的操作， 比如filter, map, reduce, find, match, sorted等。

和以前的Collection操作不同， Stream操作还有两个基础的特征：

- **Pipelining**: 中间操作都会返回流对象本身。 这样多个操作可以串联成一个管道， 如同流式风格（fluent style）。 这样做可以对操作进行优化， 比如延迟执行(laziness)和短路( short-circuiting)。
- **内部迭代**： 以前对集合遍历都是通过Iterator或者For-Each的方式, 显式的在集合外部进行迭代， 这叫做外部迭代。 Stream提供了内部迭代的方式， 通过访问者模式(Visitor)实现。



**集合讲的是数据，Stream讲的是计算！**

**注意：**

- Stream 自己不会存储元素。
- Stream 不会改变源对象。相反，他们会返回一个持有结果的新Stream。
- Stream 操作是延迟执行的。这意味着他们会等到需要结果的时候才执行。



### 三、Stream操作的三个步骤

步骤：

- 创建Stream
- 中间操作
- 终止操作



#### 1、创建Stream

在 Java 8 中, 集合接口有两个方法来生成流：

- stream()  为集合创建串行流。
- parallelStream() 为集合创建并行流。
- Arrays 的静态方法stream()
- Stream的of()



#### 2、Stream的中间操作

多个中间操作可以连接起来形成一个流水线。

##### 2.1、筛选与切片

![](https://img2020.cnblogs.com/blog/2196407/202105/2196407-20210515175046123-375153206.png)



##### 2.2、映射

![](https://img2020.cnblogs.com/blog/2196407/202105/2196407-20210515175201376-624122434.png)



##### 2.3、排序

![](https://img2020.cnblogs.com/blog/2196407/202105/2196407-20210515175233950-430492633.png)



#### 3、Stream的终止操作

终端操作会从流的流水线生成结果。其结果可以是任何值，但不是流的值，例如：List、Integer，甚至是 void 。



流进行了终止操作后，不能再次使用。

- `count()` 返回流中元素总数
- `max(Comparator c)`返回流中最大值
- `min(Comparator c)` 返回流中最小值
- `forEach(Consumer c)`内部迭代(使用 Collection 接口需要用户去做迭代，称为外部迭代。相反，Stream API 使用内部迭代——它帮你把迭代做了)



**归约和收集**

- `reduce(T iden, BinaryOperator b) `可以将流中元素反复结合起来，得到一个值。返回 T
- `collect(Collector c)`将流转换为其他形式。接收一个 Collector接口的实现，用于给Stream中元素做汇总的方法



### 四、Collectors 工具类操作

Collectors类作为Collector接口对应的工具类，除提供了对应的实现类（CollectorImpl)以外,还提供了各种快速生成Collector实例的工具方法。

![](https://img2020.cnblogs.com/blog/2196407/202105/2196407-20210515180308066-903923255.png)

![](https://img2020.cnblogs.com/blog/2196407/202105/2196407-20210515180329606-185894131.png)



参考：

https://www.cnblogs.com/birdy-silhouette/p/14772188.html