## Java 多线程-基础知识

[TOC]

### 一、基础知识

程序（软件）：数据和指令的集合。软件架构：B/S、C/S ，软件分类：系统软件，应用软件。

进程：正在运行的程序，会在内存中分配空间。

线程：进程中的多条路径，多条线程并发的执行。

<img src="https://img-blog.csdnimg.cn/31b0b3b4125d4778905c19db10e6c2f3.png" style="zoom:50%;" />

并发：多条线程在同一时间段内交替执行。

并行：多条线程同时执行。

串行：每个线程一个一个地执行



![](https://img-blog.csdnimg.cn/490fe4e86ec64608b86c8e54bfb68ee3.png)





### 二、为什么要使用多线程

​		就例如一个网络爬虫的功能，有10亿数据，假如一条请求花费0.1秒，因为时间有限，所以你要尽量提升开发效率，你就需要好多的服务器，大家也都知道，每台服务器的价钱都是成本，这么庞大的成本显然不切实际。我们只能想办法从自身提升效率。

​		

​		线程，一般就是在解决CPU浪费资源时才会用到。这就是为什么要用到线程，合理利用CPU资源，可以提升处理效率。



因为有以下几个优势：

- 充分利用多核CPU
- 提高运行效率
- 防止阻塞



### 三、Java中线程的实现方式

#### 1、继承Thread类

```java
package com.linhuaming.demo.thread;

/**
 * @author linhuaming
 * @date 2019-02-16
 */
public class ThreadDemo extends Thread {
    private String name ;

    public ThreadDemo(String name){
        this.name = name;
    }

    @Override
    public void run(){
        for(int i=0 ;i < 100 ;i++){
            System.out.println(this.name);
        }
    }
    
}
```

运行测试：

```java
package com.linhuaming.demo.thread;

public class MainTest {
    
    public static void main(String[] args) {
          ThreadDemo t1 = new ThreadDemo("线程A");
          ThreadDemo t2 = new ThreadDemo("线程B");
          t1.start();
          t2.start();
          int priority = t1.getPriority();
          System.out.println("优先级别:"+priority);
    }
    
}
```



#### 2、实现Runnable接口

```java
package com.linhuaming.demo.thread;

/**
 * @author linhuaming
 * @date 2019-02-16
 */
public class RunnableDemo implements Runnable{
    private String name;

    public RunnableDemo(String name){
        this.name = name;
    }

    @Override
    public void run() {
        for(int i = 0;i<1000;i++){
            System.out.println(this.name);
        }
    }

}
```

运行测试：

```java
package com.linhuaming.demo.thread;

public class RunnableMainTest {

    public static void main(String[] args) {
        RunnableDemo runnableA = new RunnableDemo("线程A");
        RunnableDemo runnableB = new RunnableDemo("线程B");

        Thread t1 = new Thread(runnableA);
        Thread t2 = new Thread(runnableB);

        t1.start();
        t2.start();
    }

}
```



#### 3、使用Callable接口

// TODO

#### 4、使用线程池

 // TODO



### 四、线程的生命周期

如下图：

![](https://images2018.cnblogs.com/blog/1262677/201803/1262677-20180305090943799-77682186.png)

线程的五种状态：

　　1）新建状态（New）：线程对象实例化后就进入了新建状态。

　　2）就绪状态（Runnable）：线程对象实例化后，其他线程调用了该对象的start()方法，虚拟机便会启

　　　 动该线程，处于就绪状态的线程随时可能被调度执行。

　　3）运行状态（Running）：线程获得了时间片，开始执行。只能从就绪状态进入运行状态。

　　4）阻塞状态（Blocked）：线程因为某个原因暂停执行，并让出CPU的使用权后便进入了阻塞状态。

　　　　等待阻塞：调用运行线程的wait()方法，虚拟机会把该线程放入等待池。

　　　　同步阻塞：运行线程获取对象的同步锁时，该锁已被其他线程获得，虚拟机会把该线程放入锁定池。

　　　　其他线程：调用运行线程的sleep()方法或join()方法，或线程发出I/O请求时，进入阻塞状态。

　　5）结束状态（Dead）：线程正常执行完成或异常退出时，进入了结束状态。



### 五、线程常用方法

**1）Thread类**

​		getName（）返回线程的名称。

​		setName （） 设置线程的名称。

​		getpriority（）/setPriority（） 返回线程的优先级/设置线程的优先级。

​		isDaemon（）/setDeamon（true） 判断该线程是否为守护线程/设置线程为守护线程，参数值为true。

​		start（）开启线程的方法，会默认调用run（）方法，进行线程的开启。

​		sleep（ms） 休眠，毫秒值。

　　Thread()：用于构造一个新的Thread。

　　Thread(Runnable target)：用于构造一个新的Thread，该线程使用了指定target的run方法。

　　Thread(ThreadGroup group,Runnable target)：用于在指定的线程组中构造一个新的Thread，该

　　线程使用了指定target的run方法。

　　currentThread()：获得当前运行线程的对象引用。

　　interrupt()：将当前线程置为中断状态。

　　sleep(long millis)：使当前运行的线程进入睡眠状态，睡眠时间至少为指定毫秒数。

　　join()：等待这个other线程结束，即在一个线程中调用other.join()，将等待other线程结束后才继续本线程。

　　yield()：当前执行的线程让出CPU的使用权，从运行状态进入就绪状态，让其他就绪线程执行。



**2）Object类**

　　wait()：让当前线程进入等待阻塞状态，直到其他线程调用了此对象的notify()或notifyAll()方法后，当

　　前线程才被唤醒进入就绪状态。

　　notify()：唤醒在此对象监控器上等待的单个线程。

　　notifyAll()：唤醒在此对象监控器上等待的所以线程。

​    	注：wait()、notify()、notifyAll()都依赖于同步锁，而同步锁是对象持有的，且每个对象只有一个，所以

　　　 这些方法定义在Object类中，而不是Thread类中。



**3）yield()、sleep()、wait()比较**

　　　wait()：让线程从运行状态进入等待阻塞状态，并且会释放它所持有的同步锁。

　　　yield()：让线程从运行状态进入就绪状态，不会释放它锁持有的同步锁。

　　　sleep()：让线程从运行状态进入阻塞状态，不会释放它锁持有的同步锁。



### 六、线程安全问题

**1、会引发的问题**

先看一个多线程模拟卖票的例子，总票数7张，两个线程同时卖票：

```java
/**
 *  线程演示
 */
public class ThreadTest{

    public static void main(String[] args){
        Runnable r = new MyThread();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        t1.setName("线程t1");
        t2.setName("线程t2");
        t1.start();
        t2.start();
    }

}

/**
 * 卖票线程
 */
public class MyThread implements Runnable{
    private int tickets = 7;   //票数

    @Override
    public void run(){
        //获取当前线程，线程的名字
        Thread currentThread = Thread.currentThread();
        String currentThreadName = currentThread.getName();

        while(tickets > 0){
            System.out.println(currentThreadName+" tickets:"+tickets);
            try{
                if("线程t1".equals(currentThreadName)){
                    System.out.println("睡眠: "+currentThreadName);
                    Thread.sleep(3000); // 睡眠3秒
                    System.out.println("激活: "+currentThreadName+" tickets:"+(--tickets));
                }
                tickets--;

            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }

}
```

 

运行结果：

![image-20220619173221358](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220619173221358.png)

>  运行结果不符合我们的预期，因为两个线程使用共享变量tickets，由于存在着交叉操作而破坏数据的可能性，这种潜在的干扰被称为临界区，通过同步对临界区的访问可以避免这种干扰。



**2、解决办法，使用同步机制**

在Java语言中，每个对象都有与之关联的同步锁，并且可以通过使用synchronized方法或语句来获取或释放。这个锁，在多线程协作时，如果涉及到对共享对象的访问，在访问对象之前，线程必须获取到该对象的同步锁，获取到同步锁后可以阻止其他线程获得这个锁，直到持有锁的线程释放掉锁为止。

```java
public class ThreadTest{
    public static void main(String[] args){
        Runnable r = new MyThread();
        Thread t1 = new Thread(r);
        Thread t2 = new Thread(r);
        t1.start();
        t2.start();
    }
}
public class MyThread implements Runnable{
    private int tickets = 7;
    
    @Override
    public void run(){
        //获取当前线程，线程的名字
        Thread currentThread = Thread.currentThread();
        String currentThreadName = currentThread.getName();

        while(tickets>0){
            synchronized(this){  //获取当前对象的同步锁
                if(tickets>0){
                    System.out.println(currentThreadName+" tickets:"+tickets);
                    tickets--;
                    try{
                        Thread.sleep(100);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
}
```



运行结果：

![image-20220619175824994](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220619175824994.png)





**3、synchronized用法：**

1）synchronized普通方法：如果一个线程要在某个对象上调用synchronized方法，那么它必须先获取这个对象的

锁，然后执行方法体，最后释放这个对象上的锁，而与此同时，在同一个对象上调用synchronized方法的其他

线程将阻塞，直到这个对象的锁被释放为止。

```java
public synchronized void show(){
    System.out.println("hello world");
}
```



2）synchronized静态方法：静态方法也可以被声明为synchronized的，每个类都有与之相关联的Class对象，

而静态同步方法获取的就是它所属类的Class对象上的锁，两个线程不能同时执行同一个类的静态同步方法，如

果静态数据是在线程之间共享的，那么对它的访问就必须利用静态同步方法来进行保护。



3）synchronized语句：静态语句可以使我们获取任何对象上的锁而不仅仅是当前对象上的锁，也能够让我们定

义比方法还要小的同步代码区，这样可以让线程持有锁的时间尽可能短，从而提高性能。

```java
private final Object lock = new Object();
public void show(){
     synchronized(lock){
         System.out.println("hello world");
     }
}
```



 参考：

https://www.cnblogs.com/ywzbky/p/10719721.html

https://www.cnblogs.com/brave7/p/8505354.html

https://www.cnblogs.com/rosiness/p/14135503.html

https://blog.csdn.net/u010698072/article/details/53242960
