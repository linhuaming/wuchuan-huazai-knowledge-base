## JUC 学习总结-高级篇

[toc]

### 一、线程池

#### 1、线程池是什么

**池化技术**

​		程序的运行，其本质上，是对系统资源(CPU、内存、磁盘、网络等等)的使用。如何高效的使用这些资源 是我们编程优化演进的一个方向。今天说的线程池就是一种对CPU利用的优化手段。 通过学习线程池原理，明白所有池化技术的基本设计思路。遇到其他相似问题可以解决。

​		前面提到一个名词——池化技术，那么到底什么是池化技术呢 ? 池化技术简单点来说，就是提前保存大量的资源，以备不时之需。在机器资源有限的情况下，使用池化 技术可以大大的提高资源的利用率，提升性能等。 在编程领域，比较典型的池化技术有： 线程池、连接池、内存池、对象池等。



**线程池原理**

​		主要来介绍一下其中比较简单的线程池的实现原理，希望读者们可以举一反三，通过对线程池的理解， 学习并掌握所有编程中池化技术的底层原理。 

​		我们通过创建一个线程对象，并且实现Runnable接口就可以实现一个简单的线程。可以利用上多核 CPU。当一个任务结束，当前线程就接收。 

​		但很多时候，我们不止会执行一个任务。如果每次都是如此的创建线程->执行任务->销毁线程，会造成 很大的性能开销。

​		 那能否一个线程创建后，执行完一个任务后，又去执行另一个任务，而不是销毁。这就是线程池。 这也就是池化技术的思想，通过预先创建好多个线程，放在池中，这样可以在需要使用线程的时候直接 获取，避免多次重复创建、销毁带来的开销。



**为什么使用线程池**

​		10 年前单核CPU电脑，假的多线程，像马戏团小丑玩多个球 ，CPU 需要来回切换。 现在是多核电脑，多个线程各自跑在独立的CPU上，不用切换效率高。



**线程池的优势** 

​		线程池做的工作主要是：控制运行的线程数量，处理过程中将任务放入队列，然后在线程创建后启动这 些任务，如果线程数量超过了最大数量，超出数量的线程排队等候，等其他线程执行完毕，再从队列中 取出任务来执行。 它的主要特点为：线程复用，控制最大并发数，管理线程。 

第一：降低资源消耗，通过重复利用已创建的线程降低线程创建和销毁造成的消耗。

 第二：提高响应速度。当任务到达时，任务可以不需要等待线程创建就能立即执行。

 第三：提高线程的可管理性，线程是稀缺资源，如果无限制的创建，不仅会消耗系统资源，还会降低系 统的稳定性，使用线程池可以进行统一分配，调优和监控。



#### 2、线程池的三大方法

​		Java中的线程池是通过 Executor 框架实现的，该框架中用到了 Executor ，Executors， ExecutorService，ThreadPoolExecutor 这几个类。



**三大方法说明：**

- Executors.newFixedThreadPool(int)

执行长期任务性能好，创建一个线程池，一池有N个固定的线程，有固定线程数的线程。

```java
package com.linhuaming.juc.demo.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 固定线程池
 */
public class NewFixedThreadPoolDemo {

    public static void main(String[] args) {
        // 池子大小 5
        ExecutorService threadPool = Executors.newFixedThreadPool(5);

        try{
            // 模拟有10个顾客过来银行办理业务，池子中只有5个工作人员受理业务
            for (int i = 1; i <= 10; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+" 办理业务");
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            threadPool.shutdown(); // 用完记得关闭
        }

    }

}
```



- Executors.newSingleThreadExecutor()

只有一个线程。

```java
package com.linhuaming.juc.demo.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 单个线程池
 */
public class NewSingleThreadExecutorDemo {

    public static void main(String[] args) {
        // 有且只有一个固定的线程
        ExecutorService threadPool = Executors.newSingleThreadExecutor();
        try{
            // 模拟有10个顾客过来银行办理业务，池子中只有1个工作人员受理业务
            for (int i = 1; i <= 10; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+" 办理业务");
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            threadPool.shutdown(); // 用完记得关闭
        }
    }

}
```



- Executors.newCachedThreadPool()

执行很多短期异步任务，线程池根据需要创建新线程，但在先构建的线程可用时将重用他们。 可扩容，遇强则强

```java
package com.linhuaming.juc.demo.thread.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 缓存线程池
 */
public class NewCachedThreadPoolDemo {

    public static void main(String[] args) {
        // 一池N线程，可扩容伸缩
        ExecutorService threadPool = Executors.newCachedThreadPool();

        try{
            for (int i = 1; i <= 30; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+" 办理业务");
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            threadPool.shutdown(); // 用完记得关闭
        }

    }

}
```



#### 3、ThreadPoolExecutor 七大参数

操作：查看三大方法的底层源码，发现本质都是调用了 new ThreadPoolExecutor ( 7 大参数 )

```java
	// 源码
    public ThreadPoolExecutor(int corePoolSize,
                              int maximumPoolSize,
                              long keepAliveTime,
                              TimeUnit unit,
                              BlockingQueue<Runnable> workQueue,
                              ThreadFactory threadFactory,
                              RejectedExecutionHandler handler) {
        if (corePoolSize < 0 ||
                maximumPoolSize <= 0 ||
                maximumPoolSize < corePoolSize ||
                keepAliveTime < 0)
            throw new IllegalArgumentException();
        if (workQueue == null || threadFactory == null || handler == null)
            throw new NullPointerException();
        this.acc = System.getSecurityManager() == null ?
                null :
                AccessController.getContext();
        this.corePoolSize = corePoolSize;
        this.maximumPoolSize = maximumPoolSize;
        this.workQueue = workQueue;
        this.keepAliveTime = unit.toNanos(keepAliveTime);
        this.threadFactory = threadFactory;
        this.handler = handler;
    }
```



**参数理解：**

corePollSize ：核心线程数。在创建了线程池后，线程中没有任何线程，等到有任务到来时才创建 线程去执行任务。默认情况下，在创建了线程池后，线程池中的线程数为0，当有任务来之后，就会创建 一个线程去执行任务，当线程池中的线程数目达到corePoolSize后，就会把到达的任务放到缓存队列当 中。

maximumPoolSize ：最大线程数。表明线程中最多能够创建的线程数量，此值必须大于等于1。 

keepAliveTime ：空闲的线程保留的时间。 TimeUnit ：空闲线程的保留时间单位。

TimeUnit ：空闲线程的保留时间单位。

BlockingQueue< Runnable> ：阻塞队列，存储等待执行的任务。参数有ArrayBlockingQueue、 LinkedBlockingQueue、SynchronousQueue可选。

ThreadFactory ：线程工厂，用来创建线程，一般默认即可。

RejectedExecutionHandler ：队列已满，而且任务量大于最大线程的异常处理策略。有以下取值：

```java
ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务
（重复此过程）。
ThreadPoolExecutor.CallerRunsPolicy：由调用线程处理该任务。
```



**ThreadPoolExecutor 底层工作原理**

![image-20220715231122187](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220715231122187.png)



举例：8个人进银行办理业务 

1~2人被受理（核心大小core） 

3~5人进入队列（Queue） 

6~8人到最大线程池（扩容大小max） 

再有人进来就要被拒绝策略接受了



#### 4、四大拒绝策略

```java
RejectedExecutionHandler rejected = null;
rejected = new ThreadPoolExecutor.AbortPolicy();//默认，队列满了丢任务，抛出异常。
rejected = new ThreadPoolExecutor.DiscardPolicy();//队列满了丢任务，不抛出异常【如
果允许任务丢失这是最好的】。
rejected = new ThreadPoolExecutor.DiscardOldestPolicy();//将最早进入队列的任务
删，之后再尝试加入队列。
rejected = new ThreadPoolExecutor.CallerRunsPolicy();//如果添加到线程池失败，那么
主线程会自己去执行该任务，回退。
```



**测试代码：**

```java
package com.linhuaming.juc.demo.thread.pool;

import java.util.concurrent.*;

/**
 * 创建线程池
 */
public class ThreadPoolExecutorDemo {

    public static void main(String[] args) {
        // 获得CPU的内核数
        System.out.println("获得CPU的内 "+Runtime.getRuntime().availableProcessors());

        // 自定义 ThreadPoolExecutor
        ExecutorService threadPool = new ThreadPoolExecutor(
                2,
                5,
                2L,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                new ThreadPoolExecutor.DiscardPolicy());

        try{
            // 模拟有6,7,8,9,10个顾客过来银行办理业务，观察结果情况
            // 最大容量为：maximumPoolSize + workQueue = 最大容量数
            for (int i = 1; i <= 19; i++) {
                threadPool.execute(()->{
                    System.out.println(Thread.currentThread().getName()+" 办理业务");
                });
            }
        } catch(Exception e){
            e.printStackTrace();
        } finally{

        }

    }

}
```



**思考题：最大线程到底该如何定义?**

CPU密集型：CPU几核就是几 可以使用Runtime.getRuntime().availableProcessors()方法检测出电脑是几核的。

IO密集型：判断程序中十分消耗资源的IO有多少个最大线程数大于它就好了(最好是它的两倍)。



### 二、四大函数式接口

java.util.function , Java 内置核心四大函数式接口，可以使用lambda表达式。

| 函数式接口               | 参数类型 | 返回类型 | 用途                                                         |
| ------------------------ | -------- | -------- | ------------------------------------------------------------ |
| Consumer<T>  消费型接口  | T        | void     | 对类型为T的对象应用操作，包含方法：void accept(Tt);          |
| Supplier<T>  生产型接口  | 无       | T        | 返回类型为T的对象，包含方法：T get();                        |
| Function<T>  函数型接口  | T        | R        | 对类型为T的对象应用操作，并返回结果。结果是R类型的对象。包含方法：R apply(Tt): |
| Predicate<T>  断定型接口 | T        | boolean  | 确定类型为T的对象是否满足某约束，并返回boolean值。包含方法boolean test(Tt); |



#### 1、消费型接口，有一个输入参数，没有返回值

```java
 public static void main(String[] args) {
        // Consumer<String> consumer = new Consumer<String>() {
        // @Override
        // public void accept(String s) {
        //
        // }
        // };
        // 简写
        Consumer<String> consumer = s -> { System.out.println(s);};
        consumer.accept("abc");
 }
```



#### 2、生产型接口，没有输入参数，只有返回参数

```java
public static void main(String[] args) {
    // Supplier<String> supplier = new Supplier<String>() {
    // @Override
    // public String get() {
    // return null;
    // }
    // };
    Supplier<String> supplier = ()->{return "abc";};
    System.out.println(supplier.get());
}
```



#### 3、函数型接口，有一个输入参数，有返回值

```java
public static void main(String[] args) {
    // 函数式接口，可以改为 lambda 表达式
    //Function<String,Integer> function = new Function<String, Integer>() {
    // @Override
    // public Integer apply(String s) {
    // return 1024;
    // }
    //};
    
    // 简写
    Function<String,Integer> function = s->{return s.length();};
    System.out.println(function.apply("abc"));
}
```



#### 4、断定型接口，有一个输入参数，返回值是布尔值。

```java
public static void main(String[] args) {
    //Predicate<String> predicate = new Predicate<String>() {
    // @Override
    // public boolean test(String s) {
    // return false;
    // }
    //};
    
    // 简写
    Predicate<String> predicate = s -> {return s.isEmpty();};
    System.out.println(predicate.test("abc"));
}
```



### 三、Stream 流式计算

**链式编程、流式计算、lambda表达式，现在的 Java程序员必会！**



> 流（Stream）到底是什么呢？

是数据渠道，用于操作数据源（集合、数组等）所生成的元素序列。

集合讲的是数据，流讲的是计算！



> 特点

- Stream 自己不会存储元素。

- Stream 不会改变源对象，相反，他们会返回一个持有结果的新Stream。 

- Stream 操作是延迟执行的。这意味着他们会等到需要结果的时候才执行。

![image-20220717215729028](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220717215729028.png)



> 代码测试

**User实体类**

```java
public class User {
    private int id;
    private String userName;
    private int age;
    //get、set、有参/无参构造器、toString
}
```



**Stream算法题**

```java
package com.linhuaming.juc.demo.thread.jdk8;

import java.util.Arrays;
import java.util.List;

public class StreamDemo {

    public static void main(String[] args) {
        User u1 = new User(11, "a", 23);
        User u2 = new User(12, "b", 24);
        User u3 = new User(13, "c", 22);
        User u4 = new User(14, "d", 28);
        User u5 = new User(16, "e", 26);

        List<User> list = Arrays.asList(u1, u2, u3, u4, u5);

        list.stream()
                .filter(u -> {return u.getId()%2==0;})
                .filter(u -> {return u.getAge()>24;})
                .map(u -> {return u.getUserName().toUpperCase();})
                //.sorted() //默认正排序 自己用 compareTo 比较
                .sorted((o1,o2)->{return o2.compareTo(o1);})
                .limit(1)
                .forEach(System.out::println);

        /**
         // map解释
         List<Integer> list2 = Arrays.asList(1,2,3);
         list2 = list2.stream().map(x -> {return
         x*2;}).collect(Collectors.toList());
         for (Integer element : list2) {
         System.out.println(element);
         */
    }

}
```



### 四、ForkJoin 分支合并

**什么是ForkJoin：**

从JDK1.7开始，Java提供Fork/Join框架用于并行执行任务，它的思想就是讲一个大任务分割成若干小任 务，最终汇总每个小任务的结果得到这个大任务的结果。



主要有两步： 

- 第一、任务切分； 

- 第二、结果合并；

![image-20220717222446092](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220717222446092.png)

> 它的模型大致是这样的：线程池中的每个线程都有自己的工作队列（PS：这一点和ThreadPoolExecutor 不同，ThreadPoolExecutor是所有线程公用一个工作队列，所有线程都从这个工作队列中取任务），当 自己队列中的任务都完成以后，会从其它线程的工作队列中偷一个任务执行，这样可以充分利用资源。



**工作窃取：**

另外，forkjoin有一个工作窃取的概念。简单理解，就是一个工作线程下会维护一个包含多个子任务的双 端队列。而对于每个工作线程来说，会从头部到尾部依次执行任务。这时，总会有一些线程执行的速度 较快，很快就把所有任务消耗完了。那这个时候怎么办呢，总不能空等着吧，多浪费资源啊。

工作窃取（work-stealing）算法是指某个线程从其他队列里窃取任务来执行。工作窃取的运行流程图如 下：

![image-20220717223253326](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220717223253326.png)

> 核心类

**ForkJoinPool**

WorkQueue是一个ForkJoinPool中的内部类，它是线程池中线程的工作队列的一个封装，支持任务窃取。

成存在A,B两个个 WorkQueue在执行任务，A的任务执行完了，B的任务没执行完，那么A的WorkQueue就从B的 WorkQueue的ForkJoinTask数组中拿走了一部分尾部的任务来执行，可以合理的提高运行和计算效率。

每个线程都有一个WorkQueue，而WorkQueue中有执行任务的线程（ForkJoinWorkerThread owner），还有这个线程需要处理的任务（ForkJoinTask[] array）。那么这个新提交的任务就是加 到array中。



**ForkJoinTask**

ForkJoinTask代表运行在ForkJoinPool中的任务。

主要方法：

- fork() 在当前线程运行的线程池中安排一个异步执行。简单的理解就是再创建一个子任务。
- join() 当任务完成的时候返回计算结果。
- invoke() 开始执行任务，如果必要，等待计算完成。



**Recursive 递归**

Recursive  是 ForkJoinTask 的子类

- RecursiveAction 一个递归无结果的ForkJoinTask（没有返回值）
- RecursiveTask 一个递归有结果的ForkJoinTask（有返回值）



> 代码测试

**ForkJoinWork**

```java
package com.linhuaming.juc.demo.thread.forkjoin;

import java.util.concurrent.RecursiveTask;

public class ForkJoinWork extends RecursiveTask<Long> {

    private Long start;//起始值

    private Long end;//结束值

    public static final Long critical = 10000L;//临界值

    public ForkJoinWork(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    @Override
    protected Long compute() {
        //判断是否是拆分完毕
        Long lenth = end - start;
        if(lenth<=critical){
            //如果拆分完毕就相加
            Long sum = 0L;
            for (Long i = start;i<=end;i++){
                sum += i;
            }
            return sum;
        }else {
            //没有拆分完毕就开始拆分
            Long middle = (end + start)/2;//计算的两个值的中间值
            ForkJoinWork right = new ForkJoinWork(start,middle);
            right.fork();//拆分，并压入线程队列
            ForkJoinWork left = new ForkJoinWork(middle+1,end);
            left.fork();//拆分，并压入线程队列
            //合并
            return right.join() + left.join();
        }
    }
    
}
```



**三种方式测试**

```java
package com.linhuaming.juc.demo.thread.forkjoin;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.LongStream;

public class ForkJoinWorkDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        test1();  //15756
        test2();; // 14414
        test3();  // 203
    }

    /**
     * ForkJoin 测试
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public static void test1() throws ExecutionException, InterruptedException {
        //ForkJoin实现
        long l = System.currentTimeMillis();
        ForkJoinPool forkJoinPool = new ForkJoinPool();//实现ForkJoin 就必须有
        // ForkJoinPool的支持
        ForkJoinTask<Long> task = new ForkJoinWork(0L,2000000000L);//参数为起
        // 始值与结束值
        ForkJoinTask<Long> result = forkJoinPool.submit(task);
        Long aLong = result.get();
        long l1 = System.currentTimeMillis();
        System.out.println("invoke = " + aLong +" time: " + (l1-l));
    }

    /**
     * 普通测试
     */
    public static void test2(){
        //普通线程实现
        Long x = 0L;
        Long y = 2000000000L;
        long l = System.currentTimeMillis();
        for (Long i = 0L; i <= y; i++) {
            x+=i;
        }
        long l1 = System.currentTimeMillis();
        System.out.println("invoke = " + x+" time: " + (l1-l));

    }

    /**
     * Stream测试
     */
    public static void test3(){
        //Java 8 并行流的实现
        long l = System.currentTimeMillis();
        long reduce = LongStream.rangeClosed(0, 2000000000L).parallel().reduce(0, Long::sum);
        long l1 = System.currentTimeMillis();
        System.out.println("invoke = " + reduce+" time: " + (l1-l));
    }
}
```

> 打个比方，假设一个酒店有400个房间，一共有4名清洁工，每个工人每天可以打扫100个房间，这样，4 个工人满负荷工作时，400个房间全部打扫完正好需要1天。
>
> Fork/Join的工作模式就像这样：首先，工人甲被分配了400个房间的任务，他一看任务太多了自己一个 人不行，所以先把400个房间拆成两个200，然后叫来乙，把其中一个200分给乙。
>
> 紧接着，甲和乙再发现200也是个大任务，于是甲继续把200分成两个100，并把其中一个100分给丙， 类似的，乙会把其中一个100分给丁，这样，最终4个人每人分到100个房间，并发执行正好是1天。



### 五、异步回调

> **概述**

当我们需要调用一个函数方法时。如果这个函数执行很慢,那么我们就要进行等待。但有时候,我们可能并 不急着要结果。

因此,我们可以让被调用者立即返回,让他在后台慢慢处理这个请求。对于调用者来说,则可以先处理一些 其他任务,在真正需要数据的场合再去尝试获取需要的数据。

它建模了一种异步计算，返回一个执行运算结果的引用，当运算结束后，这个引用被返回给调用方。在 Future中出发那些潜在耗时的操作把调用线程解放出来，让它能继续执行其他有价值的工作，不再需要 等待耗时的操作完成。

Future的优点：比更底层的Thread更易用。要使用Future，通常只需要将耗时的操作封装在一个 Callable对象中，再将它提交给ExecutorService。

为了让程序更加高效，让CPU最大效率的工作，我们会采用异步编程。首先想到的是开启一个新的线程 去做某项工作。再进一步，为了让新线程可以返回一个值，告诉主线程事情做完了，于是乎Future粉墨 登场。然而Future提供的方式是主线程主动问询新线程，要是有个回调函数就爽了。所以，为了满足 Future的某些遗憾，强大的CompletableFuture随着Java8一起来了。



> **Future 工作原理**

![image-20220718221623132](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220718221623132.png)



> **实例**

```java
package com.linhuaming.juc.demo.thread.async;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * 异步回调
 */
public class CompletableFutureDemo {

    public static void main(String[] args) throws Exception{
        /** 1. 没有返回值的 runAsync 异步调用 **/
        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(()->{
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "没有返回值，update mysql ok");
        });
        System.out.println("111111"); // 先执行
        completableFuture.get(); // 获取结果

        /** 2. 有返回值的 供给型参数接口 **/
        CompletableFuture<Integer> completableFuture2 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread().getName() + " completableFuture2");
            int i = 10 / 0;
            return 1024;
        });

        CompletableFuture<Integer> exceptionally = completableFuture2.whenComplete((t, u) -> {
            System.out.println("===t:" + t); //正常结果
            System.out.println("===u:" + u); //报错的信息
        }).exceptionally(e -> {
            System.out.println("=======exception:" + e.getMessage());
            return 555;
        });
        
        System.out.println("获取结果: "+exceptionally.get());
    }

}
```



### 六、JMM（java 内存模型）



> **什么是JMM**

​		JMM是java内存模型，一种抽象的概念，并不真实存在，它描述的是一组规则或者规范。

​		JMM即为JAVA 内存模型（java memory model）。因为在不同的硬件生产商和不同的操作系统下，内 存的访问逻辑有一定的差异，结果就是当你的代码在某个系统环境下运行良好，并且线程安全，但是换 了个系统就出现各种问题。Java内存模型，就是为了屏蔽系统和硬件的差异，让一套代码在不同平台下 能到达相同的访问结果。JMM从java 5开始的JSR-133发布后，已经成熟和完善起来。

​		JMM规定了内存主要划分为主内存和工作内存两种。此处的主内存和工作内存跟JVM内存划分（堆、 栈、方法区）是在不同的层次上进行的，如果非要对应起来，主内存对应的是Java堆中的对象实例部 分，工作内存对应的是栈中的部分区域，从更底层的来说，主内存对应的是硬件的物理内存，工作内存 对应的是寄存器和高速缓存。



**JMM 带来问题：**

​		JVM在设计时候考虑到，如果JAVA线程每次读取和写入变量都直接操作主内存，对性能影响比较大，所 以每条线程拥有各自的工作内存，工作内存中的变量是主内存中的一份拷贝，线程对变量的读取和写 入，直接在工作内存中操作，而不能直接去操作主内存中的变量。但是这样就会出现一个问题，当一个 线程修改了自己工作内存中变量，对其他线程是不可见的，会导致线程不安全的问题。因为JMM制定了 一套标准来保证开发者在编写多线程程序的时候，能够控制什么时候内存会被同步给其他线程。



**JMM 关于同步的规定：**

1. 线程解锁前，必须把共享变量的值刷新回主内存 

2. 线程加锁前，必须读取主内存的最新值到自己的工作内存 

3. 加锁解锁是同一把锁



**JMM的内存模型-图解**

![](https://img-blog.csdnimg.cn/025f07a54b6a4c9d83cb1fa9feb7fc6a.png)

**内存交互操作：**

​		内存交互操作有8种，虚拟机实现必须保证每一个操作都是原子的，不可在分的（对于double和long类 型的变量来说，load、store、read和write操作在某些平台上允许例外）

- lock （锁定）：作用于主内存的变量，把一个变量标识为线程独占状态

- unlock （解锁）：作用于主内存的变量，它把一个处于锁定状态的变量释放出来，释放后的变量 才可以被其他线程锁定

- read （读取）：作用于主内存变量，它把一个变量的值从主内存传输到线程的工作内存中，以便 随后的load动作使用

- load （载入）：作用于工作内存的变量，它把read操作从主存中变量放入工作内存中
- use （使用）：作用于工作内存中的变量，它把工作内存中的变量传输给执行引擎，每当虚拟机 遇到一个需要使用到变量的值，就会使用到这个指令
- assign （赋值）：作用于工作内存中的变量，它把一个从执行引擎中接受到的值放入工作内存的 变量副本中
- write （写入）：作用于主内存中的变量，它把store操作从工作内存中得到的变量的值放入主内 存的变量中
- store （存储）：作用于主内存中的变量，它把一个从工作内存中一个变量的值传送到主内存 中，以便后续的write使用



### 七、volatile

> **问题：请你谈谈你对volatile的理解**

**volitile 是 Java 虚拟机提供的轻量级的同步机制，三大特性：**

1. 保证可见性

2. 不保证原子性 

3. 禁止指令重排



> 代码验证可见性

```java
package com.linhuaming.juc.demo.thread.volatile_demo;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

/**
 * volatile 用来保证数据的同步，也就是可见性
 */
public class JMMVolatileDemo01 {

    // volatile关键字 不加volatile没有可见性
    // 不加 volatile 就会死循环，这里给大家将主要是为了面试，可以避免指令重排
    // private static int num = 0;
    private volatile static int num = 0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            while (num==0){ //此处不要编写代码,让计算机忙的不可开交
            }
        }).start();
        Thread.sleep(1000);
        num = 1;
        System.out.println(num);
    }

}
```



>  验证 volatile 不保证原子性

原子性理解： 不可分割，完整性，也就是某个线程正在做某个具体的业务的时候，中间不可以被加塞或者被分割，需 要整体完整，要么同时成功，要么同时失败。

```java
package com.linhuaming.juc.demo.thread.volatile_demo;

/**
 * volatile 不保证原子性 测试
 */
public class JMMVolatileDemo02 {

    private volatile static int num = 0;

    public  static void add(){
        num++;
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 1; i <= 20; i++) {
            new Thread(()->{
                for (int j = 1; j <= 1000; j++) {
                    add();
                }
            },String.valueOf(i)).start();
        }

        // 需要等待上面2万个线程都全部计算完毕，看最终结果
        while (Thread.activeCount()>2){ // 默认一个 main线程 一个 gc 线程
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+" "+num);
    }

}
```

**说明：**

因为我们的 add 方法没有加锁，但是加了 volatile ，说明 volatile 不能保证原子性。

命令行查看底层字节码代码实现： `javap -c JMMVolatileDemo02.class`

![image-20220725002048661](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220725002048661.png)



num++ 在多线程下是非线程安全的，如果不用 synchronized解决，是否可以用原子包的类解决。

![image-20220725002349893](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220725002349893.png)

```java
package com.linhuaming.juc.demo.thread.volatile_demo;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * volatile 不保证原子性 测试
 * 使用原子类解决,非原子性问题
 */
public class JMMVolatileDemo03 {

    private volatile static AtomicInteger num = new AtomicInteger();

    public  static void add(){
        num.getAndIncrement();
    }

    public static void main(String[] args) throws InterruptedException {
        for (int i = 1; i <= 20; i++) {
            new Thread(()->{
                for (int j = 1; j <= 1000; j++) {
                    add();
                }
            },String.valueOf(i)).start();
        }

        // 需要等待上面2万个线程都全部计算完毕，看最终结果
        while (Thread.activeCount()>2){ // 默认一个 main线程 一个 gc 线程
            Thread.yield();
        }
        System.out.println(Thread.currentThread().getName()+" "+num);
    }

}
```



> 指令重排讲解

计算机在执行程序时，为了提高性能，编译器和处理器的常常会对指令做重排，一般分以下3种：

![image-20220725215331951](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220725215331951.png)

单线程环境里面确保程序最终执行结果和代码顺序执行的结果一致。

处理器在进行重排序时必须要考虑指令之间的数据依赖性。 

多线程环境中线程交替执行，由于编译器优化重排的存在，两个线程中使用的变量能否保证一致性是无 法确定的，结果无法预测。



**重排理解测试1：**

```java
package com.linhuaming.juc.demo.thread.volatile_demo;

/**
 * 指令重排 测试
 */
public class CommandResetDemo01 {

    public static void main(String[] args) {
        int x = 11; // 语句1
        int y = 12; // 语句2
        x = x + 5; // 语句3
        y = x * x; // 语句4
    }
    // 指令顺序预测: 1234 2134 1324
    // 问题：请问语句4可以重排后变成第一条吗？ 答案：不可以
    
}
```



**重排理解测试2：**

![image-20220725220454906](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220725220454906.png)



**指令重排小结：**

volatile 实现了禁止指令重排优化，从而避免 多线程环境下程序出现乱序执行的现象。

先了解一个概念，内存屏障（Memory Barrier）又称内存栅栏，是一个CPU 指令，它的作用有两个：

- 保证特定操作的执行顺序。 

- 保证某些变量的内存可见性（利用该特性实现volatile的内存可见性）。



​        由于编译器和处理器都能执行指令重排优化。如果在指令间插入一条 Memory Barrier 则会告诉编译器 和CPU，不管什么指令都不能和这条 Memory Barrier 指令重排序，也就是说，通过插入内存屏障禁止 在内存屏障前后的指令执行重排序优化。内存屏障另外一个作用是强制刷出各种CPU的缓存数据，因此 任何CPU上的线程都能读取到这些数据的最新版本。

![](https://img-blog.csdnimg.cn/ab93adb9ba1048a9a601b3f0fa088406.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAMTIz5bCP5q2l,size_16,color_FFFFFF,t_70,g_se,x_16)



**经过，可见性，原子性，指令重排的话，线程安全性获得保证：**

工作内存与主内存同步延迟现象导致的可见性问题，可以使用 synchronized 或 volatile 关键字解决，它 们都可以使一个线程修改后的变量立即对其他线程可见。

对于指令重排导致的可见性问题 和 有序性问题，可以利用 volatile 关键字解决，因为 volatile 的另外一 个作用就是禁止重排序优化。



### 八、深入单例模式

​		单例模式可以说只要是一个合格的开发都会写，但是如果要深究，小小的单例模式可以牵扯到很多东 西，比如 多线程是否安全，是否懒加载，性能等等。还有你知道几种单例模式的写法呢？如何防止反射 破坏单例模式？今天，我们来探究单例模式。



#### 1、饿汉式

```java
package com.linhuaming.juc.demo.thread.design_mode;

/**
 * 饿汉式
 */
public class Hungry {
    private Hungry() {
    }

    private final static Hungry hungry = new Hungry();

    public static Hungry getInstance() {
        return hungry;
    }

}
```

>  饿汉式是最简单的单例模式的写法，保证了线程的安全，在很长的时间里，我都是饿汉模式来完成单例 的，因为够简单，后来才知道饿汉式会有一点小问题，看下面的代码：



```java
package com.linhuaming.juc.demo.thread.design_mode;

/**
 * 饿汉式
 */
public class Hungry {
    private byte[] data1 = new byte[1024];
    private byte[] data2 = new byte[1024];
    private byte[] data3 = new byte[1024];
    private byte[] data4 = new byte[1024];
    
    private Hungry() {
    }

    private final static Hungry hungry = new Hungry();

    public static Hungry getInstance() {
        return hungry;
    }

}
```

> 在Hungry类中，我定义了四个byte数组，当代码一运行，这四个数组就被初始化，并且放入内存了，如 果长时间没有用到getInstance方法，不需要Hungry类的对象，这不是一种浪费吗？我希望的是 只有用 到了 getInstance方法，才会去初始化单例类，才会加载单例类中的数据。所以就有了 第二种单例模 式：懒汉式。



#### 2、懒汉式

```java
package com.linhuaming.juc.demo.thread.design_mode;

/**
 * 懒汉式
 */
public class LazyMan {
    private static LazyMan lazyMan;

    private LazyMan() {
        System.out.println(Thread.currentThread().getName()+" Start");
    }

    public static LazyMan getInstance() {
        if (lazyMan == null) {
            lazyMan = new LazyMan();
        }
        return lazyMan;
    }

    // 测试并发环境，发现单例失效
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                LazyMan.getInstance();
            }).start();
        }
    }
}
```

测试并发环境，发现单例模式失效了，如下图所示：

![image-20220725224448223](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220725224448223.png)



多加一层检测可以避免问题，也就是DCL懒汉式！

```java
package com.linhuaming.juc.demo.thread.design_mode;

/**
 * 懒汉式
 */
public class LazyMan {
    private static LazyMan lazyMan;

    private LazyMan() {
        System.out.println(Thread.currentThread().getName()+" Start");
    }

    public static LazyMan getInstance() {
        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                if (lazyMan == null) {
                    lazyMan = new LazyMan();
                }
            }
        }
        return lazyMan;
    }

    // 测试并发环境，发现单例失效
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                LazyMan.getInstance();
            }).start();
        }
    }
}
```

DCL懒汉式的单例，保证了线程的安全性，又符合了懒加载，只有在用到的时候，才会去初始化，调用 效率也比较高，但是这种写法在极端情况还是可能会有一定的问题。



因为  lazyMan = new LazyMan();  不是原子性操作，至少会经过三个步骤：

- 分配对象内存空间

- 执行构造方法初始化对象

- 设置instance指向刚分配的内存地址，此时instance ！=null；

​    由于指令重排，导致A线程执行 lazyMan = new LazyMan();的时候，可能先执行了第三步（还没执行第 二步），此时线程B又进来了，发现lazyMan已经不为空了，直接返回了lazyMan，并且后面使用了返回 的lazyMan，由于线程A还没有执行第二步，导致此时lazyMan还不完整，可能会有一些意想不到的错 误，所以就有了下面一种单例模式。



这种单例模式只是在上面DCL单例模式增加一个volatile关键字来避免指令重排：

```java
package com.linhuaming.juc.demo.thread.design_mode;

/**
 * 懒汉式
 */
public class LazyMan {
    private volatile static LazyMan lazyMan;

    private LazyMan() {
        System.out.println(Thread.currentThread().getName()+" Start");
    }

    public static LazyMan getInstance() {
        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                if (lazyMan == null) {
                    lazyMan = new LazyMan();
                }
            }
        }
        return lazyMan;
    }

    // 测试并发环境，发现单例失效
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(()->{
                LazyMan.getInstance();
            }).start();
        }
    }
}

```



#### 3、静态内部类

还有这种方式是第一种饿汉式的改进版本，同样也是在类中定义static变量的对象，并且直接初始化，不 过是移到了静态内部类中，十分巧妙。既保证了线程的安全性，同时又满足了懒加载。

```java

public class Holder {
    private Holder() {
    }
    public static Holder getInstance() {
        return InnerClass.holder;
    }
    private static class InnerClass {
        private static final Holder holder = new Holder();
    }
}
```



#### 4、万恶的反射

万恶的反射登场了，反射是一个比较霸道的东西，无视private修饰的构造方法，可以直接在外面 newInstance，破坏我们辛辛苦苦写的单例模式。

```java
public static void main(String[] args) {
    try {
        LazyMan lazyMan1 = LazyMan.getInstance();
        Constructor<LazyMan> declaredConstructor =
                LazyMan.class.getDeclaredConstructor(null);
        declaredConstructor.setAccessible(true);
        LazyMan lazyMan2 = declaredConstructor.newInstance();
        System.out.println(lazyMan1.hashCode());
        System.out.println(lazyMan2.hashCode());
        System.out.println(lazyMan1 == lazyMan2);
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```

> 我们分别打印出lazyMan1，lazyMan2的hashcode，lazyMan1是否相等lazyMan2，结果显而易见不相等；



那么，怎么解决这种问题呢？

```java
public class LazyMan {
    private LazyMan() {
        synchronized (LazyMan.class) {
            if (lazyMan != null) {
                throw new RuntimeException("不要试图用反射破坏单例模式");
            }
        }
    }
    private volatile static LazyMan lazyMan;
    
    public static LazyMan getInstance() {
        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                if (lazyMan == null) {
                    lazyMan = new LazyMan();
                }
            }
        }
        return lazyMan;
    }
}

```

>  在私有的构造函数中做一个判断，如果lazyMan不为空，说明lazyMan已经被创建过了，如果正常调用 getInstance方法，是不会出现这种事情的，所以直接抛出异常！



但是这种写法还是有问题：

​		上面我们是先正常的调用了getInstance方法，创建了LazyMan对象，所以第二次用反射创建对象，私有 构造函数里面的判断起作用了，反射破坏单例模式失败。但是如果破坏者干脆不先调用getInstance方 法，一上来就直接用反射创建对象，我们的判断就不生效了

```java
public static void main(String[] args) {
    try {
        Constructor<LazyMan> declaredConstructor =
                LazyMan.class.getDeclaredConstructor(null);
        declaredConstructor.setAccessible(true);
        LazyMan lazyMan1 = declaredConstructor.newInstance();
        LazyMan lazyMan2 = declaredConstructor.newInstance();
        System.out.println(lazyMan1.hashCode());
        System.out.println(lazyMan2.hashCode());
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```



那么如何防止这种反射破坏呢？

```java
public class LazyMan {
    private static boolean flag = false;
    private LazyMan() {
        synchronized (LazyMan.class) {
            if (flag == false) {
                flag = true;
            } else {
                throw new RuntimeException("不要试图用反射破坏单例模式");
            }
        }
    }
    private volatile static LazyMan lazyMan;
    public static LazyMan getInstance() {
        if (lazyMan == null) {
            synchronized (LazyMan.class) {
                if (lazyMan == null) {
                    lazyMan = new LazyMan();
                }
            }
        }
        return lazyMan;
    }
}
```

> 在这里，我定义了一个boolean变量flag，初始值是false，私有构造函数里面做了一个判断，如果 flag=false，就把flag改为true，但是如果flag等于true，就说明有问题了，因为正常的调用是不会第二 次跑到私有构造方法的，所以抛出异常。



看起来很美好，但是还是不能完全防止反射破坏单例模式，因为可以利用反射修改flag的值。

```java
public static void main(String[] args) {
    try {
        // 通过反射创建对象
        Constructor<LazyMan> declaredConstructor =
                LazyMan.class.getDeclaredConstructor(null);
        Field field = LazyMan.class.getDeclaredField("flag");
        field.setAccessible(true);
        
        // 通过反射实例化对象
        declaredConstructor.setAccessible(true);
        LazyMan lazyMan1 = declaredConstructor.newInstance();
        System.out.println(field.get(lazyMan1));
        System.out.println(lazyMan1.hashCode());
        
        //通过反射，修改字段的值！
        field.set(lazyMan1,false);
        LazyMan lazyMan2 = declaredConstructor.newInstance();
        System.out.println(field.get(lazyMan2));
        System.out.println(lazyMan2.hashCode());
    } catch (Exception e) {
        e.printStackTrace();
    }
}
```

> 并没有一个很好的方案去避免反射破坏单例模式，所以轮到我们的枚举登场了。



#### 5、枚举

​		枚举类型是Java 5中新增特性的一部分，它是一种特殊的数据类型，之所以特殊是因为它既是一种类 (class)类型却又比类类型多了些特殊的约束，但是这些约束的存在也造就了枚举类型的简洁性、安全性 以及便捷性。

```java
public enum EnumSingleton {
    INSTANCE;
    public EnumSingleton getInstance(){
        return INSTANCE;
    }
}

class Demo04{
    public static void main(String[] args) {
        EnumSingleton singleton1=EnumSingleton.INSTANCE;
        EnumSingleton singleton2=EnumSingleton.INSTANCE;
        System.out.println("正常情况下，实例化两个实例是否相同："+
                (singleton1==singleton2));
    }
}
```

> 枚举是目前最推荐的单例模式的写法，因为足够简单，不需要开发自己保证线程的安全，同时又可以有 效的防止反射破坏我们的单例模式，我们可以看下newInstance的源码：



我们可以下 jad 进行反编译，进行编译：

```shell
jad -sjava EnumSingleton.class
# 会生成一个java文件
Parsing EnumSingleton.class... Generating EnumSingleton.java
```



查看源码：

```java
// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3)
// Source File Name: EnumSingleton.java
package 53554F8B6A215F0F;
public final class EnumSingleton extends Enum
{
    public static EnumSingleton[] values()
    {
        return (EnumSingleton[])$VALUES.clone();
    }
    public static EnumSingleton valueOf(String name)
    {
        return (EnumSingleton)Enum.valueOf(53554F8B6A215F0F/EnumSingleton,
                name);
    }
    private EnumSingleton(String s, int i)
    {
        super(s, i);
    }
    public EnumSingleton getInstance()
    {
        return INSTANCE;
    }
    public static final EnumSingleton INSTANCE;
    private static final EnumSingleton $VALUES[];
    static
    {
        INSTANCE = new EnumSingleton("INSTANCE", 0);
        $VALUES = (new EnumSingleton[] {
                INSTANCE
        });
    }
}
```



用反射机制，再次尝试破坏看一下！

```java
public enum EnumSingleton {
    INSTANCE;
    public EnumSingleton getInstance(){
        return INSTANCE;
    }
}

class Demo04{
    public static void main(String[] args) throws Exception {
        EnumSingleton singleton1=EnumSingleton.INSTANCE;
        EnumSingleton singleton2=EnumSingleton.INSTANCE;
        System.out.println("正常情况下，实例化两个实例是否相同："+
                (singleton1==singleton2));
        //Constructor<EnumSingleton> constructor =EnumSingleton.class.getDeclaredConstructor(); //自身的类没有无参构造方法
        Constructor<EnumSingleton> constructor =
                EnumSingleton.class.getDeclaredConstructor(String.class,int.class);
        constructor.setAccessible(true);
        EnumSingleton enumSingleton = constructor.newInstance();
        System.out.println(enumSingleton);
    }
}
```

> 试图破坏，真的破坏不了！ 假如有人问你单例模式，再也不用害怕了。



### 九、深入理解CAS

#### 1、什么是CAS

CAS全称 Compare And Swap（比较与交换），是一种`无锁算法`。在不使用锁（没有线程被阻塞）的情况下实现多线程之间的变量同步。

- `java.util.concurrent`包中的原子类就是通过CAS来实现了乐观锁。



**CAS算法涉及到三个操作数：**

- 需要读写的内存值 V。
- 进行比较的值 A。
- 要写入的新值 B。

当且仅当 `V 的值等于 A` 时，`CAS`通过原子方式用`新值B来更新V的值`（“比较+更新”整体是一个原子操作），否则不会执行任何操作。一般情况下，“更新”是一个不断重试的操作。



CAS机制当中使用了3个基本操作数：`内存地址V`，`旧的预期值A`，`计算后要修改后的新值B`。

- （1）初始状态：在内存地址V中存储着变量值为 1

![](https://img-blog.csdnimg.cn/743e088496704b0b90821b895b1f5bb2.png)



- （2）线程1想要把内存地址为 V 的变量值增加1。这个时候对线程1来说，旧的预期值A=1，要修改的新值B=2。

![](https://img-blog.csdnimg.cn/d3576872b0814fd1a068b8abbb2642f5.png)



- （3）在线程1要提交更新之前，线程2捷足先登了，已经把内存地址V中的变量值率先更新成了2。

![](https://img-blog.csdnimg.cn/d5578964e171469c8cb7f5acd7cce3fb.png)



- （4）线程1开始提交更新，首先将`预期值A`和`内存地址V的实际值`比较（Compare），发现`A不等于V`的实际值，提交失败。

![](https://img-blog.csdnimg.cn/7720366b09494f31bd94db1eca7ee837.png)



- （5）线程1重新获取内存地址 V的当前值，并重新计算想要修改的新值。此时对线程1来说，A=2，B=3。这个重新尝试的过程被称为自旋。如果多次失败会有多次自旋。

![](https://img-blog.csdnimg.cn/1ab8d8cfd170424ab5469cc0491fe728.png)



- （6）线程 1 再次提交更新，这一次没有其他线程改变地址 V 的值。线程1进行`Compare`，发现`预期值 A 和内存地址V的实际值是相等`的，进行 `Swap` 操作，将内存地址 V 的实际值修改为 B。

![](https://img-blog.csdnimg.cn/a6a0d6c3084d4d35b4864655b836d3ed.png)



> - 总结：更新一个变量的时候，只有当变量的预期值 A 和内存地址 V 中的实际值相同时，才会将内存地址 V 对应的值修改为B，这整个操作就是CAS。



#### 2、CAS 基本原理

- CAS 主要包括两个操作：`Compare`和`Swap`，有人可能要问了：两个操作能保证是原子性吗？可以的。

CAS 是一种系统原语，原语属于操作系统用语，原语由若干指令组成，用于完成某个功能的一个过程，并且原语的执行必须是连续的，在执行过程中不允许被中断，也就是说 CAS 是一条 CPU 的原子指令，由操作系统硬件来保证。

回到 Java 语言，JDK 是在 1.5 版本后才引入 CAS 操作，在`sun.misc.Unsafe`这个类中定义了 CAS 相关的方法。

```java
public final native boolean compareAndSwapObject(Object o, long offset, Object expected, Object x);

public final native boolean compareAndSwapInt(Object o, long offset, int expected, int x);

public final native boolean compareAndSwapLong(Object o, long offset, long expected, long x);

```



用代码理解下什么是CAS： 

```java
package com.linhuaming.juc.demo.thread.cas;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * CAS : 比较并交换 compareAndSet
 *
 */
public class CASDemo01 {

    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(5);
        // main do somethings...

        // 期望的是5，后面改为 2020 ， 所以结果为 true，2020
        System.out.println(atomicInteger.compareAndSet(5, 2020)+"=>"+atomicInteger.get());

        // 期望的是5，后面改为 1024 ， 所以结果为 false，2020
        System.out.println(atomicInteger.compareAndSet(5, 1024)+"=>"+atomicInteger.get());
    }

}

/**
 * this：当前对象
 * valueOffSet：当前对象的内存地址偏移量，就是this的内存地址
*/
public final boolean compareAndSet(int expect, int update) {   
    return unsafe.compareAndSwapInt(this, valueOffset, expect, update);
}

```

> 一句话：真实值和期望值相同，就修改成功，真实值和期望值不同，就修改失败！



#### 3、CAS 缺点

- ABA 问题

CAS需要在操作值的时候检查内存值是否发生变化，没有发生变化才会更新内存值。但是如果内存值原来是A，后来变成了B，然后又变成了A，那么CAS进行检查时会发现值没有发生变化，但是实际上是有变化的。

ABA问题的解决思路就是在`变量前面添加版本号`，每次变量更新的时候都把版本号加一，这样变化过程就从`“A－B－A”`变成了`“1A－2B－3A”`。

![](https://img-blog.csdnimg.cn/9b9772f6461b40eca1c6a04f6e59b3e4.png)

>  JDK从1.5开始提供了`AtomicStampedReference`类来解决ABA问题，具体操作封装在`compareAndSet`()中。`compareAndSet`()首先检查`当前引用和当前标`志与`预期引用和预期标志`是否相等，如果都相等，则以原子方式将引用值和标志的值设置为给定的更新值。



- 循环时间长开销大

CAS操作如果长时间不成功，会导致其一直自旋，给CPU带来非常大的开销。

> 解决方案：可以考虑限制自旋的次数，避免过度消耗 CPU；另外还可以考虑延迟执行。



- 只能保证一个共享变量的原子操作

当对一个共享变量执行操作时，可以使用 CAS 来保证原子性，但是如果要对多个共享变量进行操作时，CAS 是无法保证原子性的，比如需要将 i和 j 同时加 1：

```java
i++；j++；
```

> 1、这个时候可以使用 `synchronized` 进行加锁。
>
> 2、有没有其他办法呢？有，将多个变量操作合成一个变量操作。从 JDK1.5 开始提供了`AtomicReference` 类来保证引用对象之间的`原子性`，你可以把多个变量放在一个对象里来进行CAS操作。



**总结：**

​		核心：`compare and swap`。需要3个操作数，分别是内存位置V，旧的预期值A，准备设置的新值B。CAS执行时，当地址V对应的旧值是A时，处理器才会将V对应的值更新为B，否则就不执行更新。该操作为原子操作，不会被其他线程中断；

​		Java的实现：`引入Unsafe类`，其通过本地`native`方法直接操作特定的内存数据。通过对内存的`偏移地址`去获取值和循环修改数据直至成功。JVM会编译成CAS的字节码指令，通过`硬件功能`保证指令执行过程中是连续的，原子性的。



### 十、原子引用

ABA问题怎么产生的？CAS会导致 “ABA问题”。狸猫换太子

CAS算法实现一个重要前提：需要取出内存中某时刻的数据并在当下时刻比较并交换，那么在这个时间 差内会导致数据的变化

比如说一个线程one从内存位置V中取出A，这个时候另一个线程two也从内存中取出A，并且线程two进 行了一些操作将值变成了B，然后线程two又将 V位置的数据变成A，这时候线程one进行CAS操作发现内 存中仍然是A，然后线程one操作成功。

尽管线程one的CAS操作成功，但是不代表这个过程就是没有问题的。



> 原子引用 AtomicReference

```java
public class AtomicReferenceDemo {
    
    public static void main(String[] args) {
        User zhangsan = new User("zhangsan", 22);
        User lisi = new User("lisi", 25);
        
        AtomicReference<User> atomicReference = new AtomicReference<>();
        atomicReference.set(zhangsan); // 设置
        
        System.out.print(atomicReference.compareAndSet(zhangsan,lisi));
        System.out.println(atomicReference.get().toString());
        
        System.out.print(atomicReference.compareAndSet(zhangsan,lisi));
        System.out.println(atomicReference.get().toString());
	}
    
}

class User{
    String username;
    int age;
    
    public User(String username, int age) {
        this.username = username;
        this.age = age;
    }
    
    @Override
    public String toString() {
        return "User{" +
        "username='" + username + '\'' +
        ", age=" + age +
        '}';
    }
}
```



要解决ABA问题，我们就需要加一个版本号

> 原子引用+版本号，类似乐观锁

演示ABA问题：

```java
/**
 * ABA 问题的解决 AtomicStampedReference
 */
public class ABADemo {
    static AtomicReference<Integer> atomicReference = new AtomicReference<>(100);
    public static void main(String[] args) {
        new Thread(()->{
            atomicReference.compareAndSet(100,101);
            atomicReference.compareAndSet(101,100);
        },"T1").start();

        new Thread(()->{
            // 暂停一秒钟，保证上面线程先执行
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(atomicReference.compareAndSet(100, 2019)); //修改成功！
            System.out.println(atomicReference.get());
        },"T2").start();
    }
}
```



解决方案：

```java
public class ABADemo {
    static AtomicStampedReference<Integer> atomicStampedReference = new AtomicStampedReference<>(100,1);

    public static void main(String[] args) {
        new Thread(()->{
            int stamp = atomicStampedReference.getStamp(); // 获得版本号
            System.out.println("T1 stamp 01=>"+stamp);

            // 暂停2秒钟，保证下面线程获得初始版本号
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            atomicStampedReference.compareAndSet(100, 101, atomicStampedReference.getStamp(),atomicStampedReference.getStamp()+1);
            System.out.println("T1 stamp 02=>"+atomicStampedReference.getStamp());

            atomicStampedReference.compareAndSet(101, 100, atomicStampedReference.getStamp(),atomicStampedReference.getStamp()+1);
            System.out.println("T1 stamp 03=>"+atomicStampedReference.getStamp());
        },"T1").start();


        new Thread(()->{
            int stamp = atomicStampedReference.getStamp(); // 获得版本号
            System.out.println("T2 stamp 01=>"+stamp); // 获得版本号

            // 暂停3秒钟，保证上面线程先执行
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            boolean result = atomicStampedReference.compareAndSet(100, 2019, stamp, stamp + 1);
            System.out.println("T2 是否修改成功 =>"+ result);
            System.out.println("T2 最新stamp =>"+atomicStampedReference.getStamp());
            System.out.println("T2 当前的最新值 =>"+atomicStampedReference.getReference());
        },"T2").start();
    }

}
```



### 十一、JAVA 各种锁（简单了解）

**锁的种类：**

![](https://img-blog.csdnimg.cn/2fb7d646659e4cecbae96058a060b2d0.png)

##### 1、公平锁与非公平锁

- 公平锁：是指多个线程按照申请锁的顺序来获取锁，类似排队打饭，先来后到。

- 非公平锁：是指多个线程获取锁的顺序并不是按照申请锁的顺序，有可能后申请的线程比现申请的线程 优先获取锁，在高并发的情况下，有可能会造成优先级反转或者饥饿现象。

```java
// 无参
public ReentrantLock() {
	sync = new NonfairSync();
}
// 有参
public ReentrantLock(boolean fair) {
	sync = fair ? new FairSync() : new NonfairSync();
}
```

> 两者区别

- 并发包中的 ReentrantLock 的创建可以指定构造函数 的 boolean类型来得到公平锁或者非公平锁，默认 是非公平锁！
- 公平锁：就是很公平，在并发环境中，每个线程在获取到锁时会先查看此锁维护的等待队列，如果为 空，或者当前线程是等待队列的第一个，就占有锁，否则就会加入到等待队列中，以后会按照FIFO的规 则从队列中取到自己。
- 非公平锁：非公平锁比较粗鲁，上来就直接尝试占有锁，如果尝试失败，就会采用类似公平锁那种方 式。

- Java ReentrantLock 而言，通过构造函数指定该锁是否是公平锁，默认是非公平锁。非公平锁的优点在 于吞吐量比公平锁大。Synchronized也是一种非公平锁。



##### 2、可重入锁

可重入锁（也叫递归锁），指的是同一线程外层函数获得锁之后，内层递归函数仍然能获取该锁的代码，在同一个线程在外层方法 获取锁的时候，在进入内层方法会自动获取锁。

也就是说，线程可以进入任何一个它已经拥有的锁，所同步着的代码块。 好比家里进入大门之后，就可 以进入里面的房间了。

ReentrantLock、Synchronized 就是一个典型的可重入锁，可重入锁最大的作用就是避免死锁。



测试一：Synchronized 版本

```java
/**
 * 可重入锁（也叫递归锁）
 * 指的是同一线程外层函数获得锁之后，内层递归函数仍然能获取该锁的代码
 * 在同一个线程在外层方法获取锁的时候，在进入内层方法会自动获取锁。
 */
public class ReentrantLockDemo {

    public static void main(String[] args) throws Exception {
        Phone phone = new Phone();

        // T1 线程在外层获取锁时，也会自动获取里面的锁
        new Thread(()->{
            phone.sendSMS();
        },"T1").start();

        new Thread(()->{
            phone.sendSMS();
        },"T2").start();

    }
}

class Phone{
    public synchronized void sendSMS(){
        System.out.println(Thread.currentThread().getName()+" sendSMS");
        sendEmail();
    }
    public synchronized void sendEmail(){
        System.out.println(Thread.currentThread().getName()+" sendEmail");
    }
}
```



测试二：Lock 版本

```java
/**
 * 可重入锁（也叫递归锁）
 * 指的是同一线程外层函数获得锁之后，内层递归函数仍然能获取该锁的代码
 * 在同一个线程在外层方法获取锁的时候，在进入内层方法会自动获取锁。
 */
public class ReentrantLockDemo {
    public static void main(String[] args) throws Exception {
        Phone phone = new Phone();
// T1 线程在外层获取锁时，也会自动获取里面的锁
        new Thread(phone,"T1").start();
        new Thread(phone,"T2").start();
    }
}

class Phone implements Runnable{
    Lock lock = new ReentrantLock();

    @Override
    public void run() {
        get();
    }

    public void get(){
        lock.lock();
        // lock.lock(); 锁必须匹配，如果两个锁，只有一个解锁就会失败
        try {
            System.out.println(Thread.currentThread().getName()+" get()");
            set();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
            // lock.lock();
        }
    }

    public void set(){
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName()+" set()");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

}
```



##### 3、自旋锁

是指尝试获取锁的线程不会立即阻塞，而是采用循环的方式去尝试获取锁，这样的好处是减少线程上下 文切换的消耗，缺点是循环会消耗CPU资源。

```java
//  Unfafe类，unsafe.getAndAddInt() ,
public final int getAndAddInt(Object var1, long var2, int var4) {
    int var5;
    do {
// 获取传入对象的地址
        var5 = this.getIntVolatile(var1, var2);
// 比较并交换，如果var1，var2 还是原来的 var5，就执行内存偏移+1； var5 +
        var4
    } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));
    return var5;
}
```



测试代码：

```java

/**
 * 自旋锁的实现
 */
public class SpinLockDemo {
    // 原子引用线程, 没写参数，引用类型默认为null
    AtomicReference<Thread> atomicReference = new AtomicReference<>();

    //上锁
    public void myLock(){
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName()+"==>mylock");
        // 自旋
        while (!atomicReference.compareAndSet(null,thread)){
        }
    }

    //解锁
    public void myUnlock(){
        Thread thread = Thread.currentThread();
        atomicReference.compareAndSet(thread,null);
        System.out.println(Thread.currentThread().getName()+"==>myUnlock");
    }

    // 测试
    public static void main(String[] args) {
        SpinLockDemo spinLockDemo = new SpinLockDemo();
        new Thread(()->{
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            spinLockDemo.myUnlock();
        },"T1").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            spinLockDemo.myUnlock();
        },"T2").start();
    }

}
```





##### 4、死锁

​		死锁是指两个或两个以上的进程在执行过程中，因争夺资源而造成的一种互相等待的现象，若无外力干 涉那它们都将无法推进下去，如果系统资源充足，进程的资源请求都能够得到满足，死锁出现的可能性 就很低，否者就会因为争夺有限的资源而陷入死锁。



**死锁图解：**

![image-20220728233357718](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220728233357718.png)



**客人与商家-举例说明：**

> 在上面的例子中，客人对应线程A，钱对锁A。商家对应线程B，物品对锁B。

![image-20220728233908272](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220728233908272.png)

- 客人对商家说：先给物品，我才给钱你。
- 商家对客人说：先给钱我，我才给物品你。



> 代码测试

```java

/**
 * 死锁演示
 */
public class DeadLockDemo {
    public static void main(String[] args) {
        String lockA = "lockA";
        String lockB = "lockB";
        new Thread(new HoldLockThread(lockA,lockB),"T1").start();
        new Thread(new HoldLockThread(lockB,lockA),"T2").start();
    }
}


class HoldLockThread implements Runnable{
    private String lockA;
    private String lockB;

    public HoldLockThread(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    public void run() {
        synchronized (lockA){
            System.out.println(Thread.currentThread().getName()+"lock:"+lockA+"=>get"+lockB);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockB){
                System.out.println(Thread.currentThread().getName()+"lock:"+lockB+"=>get"+lockA);
            }
        }
    }

}
```



**死锁解决办法：**

1、查看JDK目录的bin目录 

2、使用 jps -l 命令定位进程号 

3、使用 jstack 进程号 找到死锁查看

```java
Java stack information for the threads listed above:
===================================================
"T2":
at com.kuang.HoldLockThread.run(DeadLockDemo.java:43)
- waiting to lock <0x00000000d5b87298> (a java.lang.String)
- locked <0x00000000d5b872d0> (a java.lang.String)
at java.lang.Thread.run(Thread.java:748)
"T1":
at com.kuang.HoldLockThread.run(DeadLockDemo.java:43)
- waiting to lock <0x00000000d5b872d0> (a java.lang.String)
- locked <0x00000000d5b87298> (a java.lang.String)
at java.lang.Thread.run(Thread.java:748)
Found 1 deadlock.
```

> 问10个人，9个说看日志，还有一个分析堆栈信息，这一步，他就已经赢了！



参考文章：  https://blog.csdn.net/abc98526/article/details/123938633

参考视频：  https://www.bilibili.com/video/BV1B7411L7tE

演示代码：