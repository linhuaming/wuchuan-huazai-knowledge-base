## Java 锁机制-简单介绍

[TOC]

### 一、锁的概述

#### 1、背景

**问题的产生：**

Java允许多线程并发控制，当多个线程同时操作一个可共享的资源变量时（如数据的增删改查），将会导致数据不准确，相互之间产生冲突。



卖票例子：

```java
/**
 * 卖票任务
 */
public class Ticket implements  Runnable{

    // 当前票数
    private  int num = 10;

    @Override
    public void run() {
        while(true){
            if(num > 0){
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName()+" ...sale... " + num--);
            }
        }
    }
}


public class Nothing {
    public static void main(String[] args) {
        // 任务对象
        Ticket ticket = new Ticket();

        // 创建4个线程,同时执行卖票任务
        Thread t1 = new Thread(ticket);
        Thread t2 = new Thread(ticket);
        Thread t3 = new Thread(ticket);
        Thread t4 = new Thread(ticket);

        // 启动线程
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}
```



输出部分结果：

```java
Thread-2 ...sale... 10
Thread-0 ...sale... 9
Thread-3 ...sale... 8
Thread-1 ...sale... 7
Thread-1 ...sale... 6
Thread-2 ...sale... 4
Thread-0 ...sale... 5
Thread-3 ...sale... 5
Thread-2 ...sale... 3
Thread-1 ...sale... 2
Thread-3 ...sale... 1
Thread-0 ...sale... 0
Thread-1 ...sale... -1
Thread-2 ...sale... -2

```

> 显然上述结果是不合理的，对于同一张票进行了多次售出。



这就是多线程情况下， 出现了数据`“脏读”`情况。

- 即多个线程访问余票num时，当一个线程获得余票的数 量，要在此基础上进行-1的操作之前，其他线程可能已经卖出多张票，导致获得的
  num不是最新的，然后-1后更新的数据就会有误。这就需要线程同步的实现了。



**问题的解决：**

因此加入同步锁以避免在该线程没有完成操作之前，被其他线程的调用，从而保证 了该变量的唯一性和准确性。



#### 2、锁的定义

**锁的由来-图解：**

![image-20220729232132347](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220729232132347.png)

**步骤说明：**

- 为了处理任务效率加快，加入了多线程机制。
- 多个线程同时访问同一资源时，会引发数据不一致问题，也就是线程安全。
- 线程安全问题，可以使用同步机制方式解决。
- 同步机制的实现有两种方式，分别是synchronized 关键字和 Lock 锁



**什么是锁：**

在计算机科学中，锁(lock)或互斥(mutex)是一种同步机制，用于在有许多执行线程的环境中强制对资源的访问限制。



### 二、锁的种类

**java 中的各种锁：**

![](https://img-blog.csdnimg.cn/2fb7d646659e4cecbae96058a060b2d0.png)

#### 1、乐观锁VS悲观锁

乐观锁与悲观锁是一种广义上的概念，体现了看待线程同步的不同角度。在Java和数据库中都有此概念对应的实际应用。

![](https://img-blog.csdnimg.cn/dd01de91e7524bf2a83a87b74cd3d03e.png)



**悲观锁：**

![](https://img-blog.csdnimg.cn/621a2bcfbe134e7680a3f12bb8df785b.png)

对于同一个数据的并发操作，悲观锁认为自己在使用数据的时候一定有别的线程来修改数据，因此在获取数据的时候会先加锁，确保数据不会被别的线程修改。

- Java 语言中 `synchronized` 和 `ReentrantLock`等就是典型的悲观锁
- 使用了 synchronized 关键字的容器类如 HashTable 等也是悲观锁的应用



举个生活中的例子，假设厕所只有一个坑位了，悲观锁上厕所会第一时间把门反锁上，这样其他人上厕所只能在门外等候，这种状态就是「阻塞」了。

回到代码世界中，一个共享数据加了悲观锁，那线程每次想操作这个数据前都会假设其他线程也可能会操作这个数据，所以每次操作前都会上锁，这样其他线程想操作这个数据拿不到锁只能阻塞了。



>  代码演示

```java
// ------------------------- 悲观锁的调用方式 -------------------------
// synchronized 方式
public synchronized void testMethod() {
	// 操作同步资源
}

// ReentrantLock 方式
private ReentrantLock lock = new ReentrantLock(); // 需要保证多个线程使用的是同一个锁
public void modifyPublicResources() {
	lock.lock();
	// 操作同步资源
	lock.unlock();
}
```



**乐观锁：**

![](https://img-blog.csdnimg.cn/0f593d15a29a4afc9d3f7fe56a565ab1.png)

乐观锁认为自己在使用数据时不会有别的线程修改数据，所以不会添加锁，`只是在更新数据`的时候去判断之前有没有别的线程更新了这个数据。

- 如果这个数据没有被更新，当前线程将自己修改的数据成功写入
- 如果数据已经被其他线程更新，则根据不同的实现方式执行不同的操作（例如报错或者自动重试）
- 乐观锁在Java中是通过使用无锁编程来实现，最常采用的是`CAS算法`，Java原子类中的递增操作就通过`CAS`自旋实现的



> 代码演示

```java
// ------------------------- 乐观锁的调用方式 -------------------------
private AtomicInteger atomicInteger = new AtomicInteger();  // 需要保证多个线程使用的是同一个AtomicInteger
atomicInteger.incrementAndGet(); //执行自增1
```



通过调用方式示例，我们可以发现悲观锁基本都是在显式的锁定之后再操作同步资源，而乐观锁则直接去操作同步资源。

那么，为何乐观锁能够做到不锁定同步资源也可以正确的实现线程同步呢？乐观锁的主要实现方式 `“CAS”` 的技术原理。



#### 2、自旋锁VS适应性自旋锁

**为什么会出现自旋锁？**

阻塞或唤醒一个Java线程需要操作系统切换CPU状态来完成，这种状态转换需要耗费处理器时间。如果同步代码块中的内容过于简单，状态转换消耗的时间有可能比用户代码执行的时间还要长。

在许多场景中，同步资源的锁定时间很短，为了这一小段时间去切换线程，线程挂起和恢复现场的花费可能会让系统得不偿失。如果物理机器有多个处理器，能够让两个或以上的线程同时并行执行，我们就可以让后面那个请求锁的线程不放弃CPU的执行时间，看看持有锁的线程是否很快就会释放锁。

为了让当前线程“稍等一下”，我们需让当前线程进行自旋，如果在自旋完成后前面锁定同步资源的线程已经释放了锁，那么当前线程就可以不必阻塞而是直接获取同步资源，从而避免切换线程的开销。这就是自旋锁。



**自旋锁：**

![](https://img-blog.csdnimg.cn/4bfb47178fa44535837eac4e450f335c.png)

自旋锁是指线程在没有获得锁时不是被直接挂起，而是执行一个忙循环，这个忙循环就是所谓的自旋。

自旋锁本身是有缺点的，`它不能代替阻塞`。自旋等待虽然避免了线程切换的开销，但它要`占用处理器时间`。如果锁被占用的时间很短，自旋等待的效果就会非常好。

- 反之，如果锁被占用的时间很长，那么自旋的线程只会白浪费处理器资源。

- 所以，自旋等待的时间必须要有一定的限度，如果自旋超过了限定次数（默认是10次，可以使用-XX:PreBlockSpin来更改）没有成功获得锁，就应当挂起线程。



自旋锁的实现原理同样也是CAS，`AtomicInteger`中调用`unsafe`进行自增操作的源码中的do-while循环就是一个自旋操作，如果修改数值失败则通过循环来执行自旋，直至修改成功。

![](https://img-blog.csdnimg.cn/a961dc462ea64e259c8be34ae9dd77a2.png)

> 代码演示

```java

/**
 * 自旋锁的实现
 */
public class SpinLockDemo {
    // 原子引用线程, 没写参数，引用类型默认为null
    AtomicReference<Thread> atomicReference = new AtomicReference<>();

    //上锁
    public void myLock(){
        Thread thread = Thread.currentThread();
        System.out.println(Thread.currentThread().getName()+"==>mylock");
        // 自旋
        while (!atomicReference.compareAndSet(null,thread)){
        }
    }

    //解锁
    public void myUnlock(){
        Thread thread = Thread.currentThread();
        atomicReference.compareAndSet(thread,null);
        System.out.println(Thread.currentThread().getName()+"==>myUnlock");
    }

    // 测试
    public static void main(String[] args) {
        SpinLockDemo spinLockDemo = new SpinLockDemo();
        new Thread(()->{
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            spinLockDemo.myUnlock();
        },"T1").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(()->{
            spinLockDemo.myLock();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            spinLockDemo.myUnlock();
        },"T2").start();
    }

}
```





**适应性自旋锁：**

自旋锁在JDK1.4.2中引入，使用-XX:+UseSpinning来开启。JDK 6中变为默认开启，并且引入了自适应的自旋锁（适应性自旋锁）。

自适应意味着自旋的时间（次数）不再固定，而是由前一次在同一个锁上的自旋时间及锁的拥有者的状态来决定。如果在同一个锁对象上，自旋等待刚刚成功获得过锁，并且持有锁的线程正在运行中，那么虚拟机就会认为这次自旋也是很有可能再次成功，进而它将允许自旋等待持续相对更长的时间。如果对于某个锁，自旋很少成功获得过，那在以后尝试获取这个锁时将可能省略掉自旋过程，直接阻塞线程，避免浪费处理器资源。



**总结：**

尝试获取锁的线程不会立即阻塞，而是采用`循环`的方式去尝试获取锁，这样的好处是`减少线程上下文切换的消耗`，缺点是`循环会消耗CPU`。



#### 3、无锁VS偏向锁VS轻量级锁VS重量级锁

**Java的对象头和对象组成详解：**

https://blog.csdn.net/lkforce/article/details/81128115



**java monitor是什么意思：**

https://blog.csdn.net/weixin_42104906/article/details/115780296



**java对象结构与Monitor工作原理：**

https://blog.csdn.net/qq_44819486/article/details/109519314



**无锁：**

无锁没有对资源进行锁定，所有的线程都能访问并修改同一个资源，但同时只有一个线程能修改成功。

无锁的特点就是修改操作在循环内进行，线程会不断的尝试修改共享资源。如果没有冲突就修改成功并退出，否则就会继续循环尝试。如果有多个线程修改同一个值，必定会有一个线程能修改成功，而其他修改失败的线程会不断重试直到修改成功。上面我们介绍的CAS原理及应用即是无锁的实现。无锁无法全面代替有锁，但无锁在某些场合下的性能是非常高的。



**偏向锁：**

偏向锁是指一段同步代码一直被一个线程所访问，那么该线程会自动获取锁，降低获取锁的代价。

在大多数情况下，锁总是由同一线程多次获得，不存在多线程竞争，所以出现了偏向锁。其目标就是在只有一个线程执行同步代码块时能够提高性能。

当一个线程访问同步代码块并获取锁时，会在Mark Word里存储锁偏向的线程ID。在线程进入和退出同步块时不再通过CAS操作来加锁和解锁，而是检测Mark Word里是否存储着指向当前线程的偏向锁。引入偏向锁是为了在无多线程竞争的情况下尽量减少不必要的轻量级锁执行路径，因为轻量级锁的获取及释放依赖多次CAS原子指令，而偏向锁只需要在置换ThreadID的时候依赖一次CAS原子指令即可。

偏向锁只有遇到其他线程尝试竞争偏向锁时，持有偏向锁的线程才会释放锁，线程不会主动释放偏向锁。偏向锁的撤销，需要等待全局安全点（在这个时间点上没有字节码正在执行），它会首先暂停拥有偏向锁的线程，判断锁对象是否处于被锁定状态。撤销偏向锁后恢复到无锁（标志位为“01”）或轻量级锁（标志位为“00”）的状态。

偏向锁在JDK 6及以后的JVM里是默认启用的。可以通过JVM参数关闭偏向锁：-XX:-UseBiasedLocking=false，关闭之后程序默认会进入轻量级锁状态。




**轻量级锁：**

是指当锁是偏向锁的时候，被另外的线程所访问，偏向锁就会升级为轻量级锁，其他线程会通过自旋的形式尝试获取锁，不会阻塞，从而提高性能。

在代码进入同步块的时候，如果同步对象锁状态为无锁状态（锁标志位为“01”状态，是否为偏向锁为“0”），虚拟机首先将在当前线程的栈帧中建立一个名为锁记录（Lock Record）的空间，用于存储锁对象目前的Mark Word的拷贝，然后拷贝对象头中的Mark Word复制到锁记录中。

拷贝成功后，虚拟机将使用CAS操作尝试将对象的Mark Word更新为指向Lock Record的指针，并将Lock Record里的owner指针指向对象的Mark Word。

如果这个更新动作成功了，那么这个线程就拥有了该对象的锁，并且对象Mark Word的锁标志位设置为“00”，表示此对象处于轻量级锁定状态。

如果轻量级锁的更新操作失败了，虚拟机首先会检查对象的Mark Word是否指向当前线程的栈帧，如果是就说明当前线程已经拥有了这个对象的锁，那就可以直接进入同步块继续执行，否则说明多个线程竞争锁。

若当前只有一个等待线程，则该线程通过自旋进行等待。但是当自旋超过一定的次数，或者一个线程在持有锁，一个在自旋，又有第三个来访时，轻量级锁升级为重量级锁。




**重量级锁：**

升级为重量级锁时，锁标志的状态值变为“10”，此时Mark Word中存储的是指向重量级锁的指针，此时等待锁的线程都会进入阻塞状态。



**总结：**

整体的锁状态升级流程如下：
![在这里插入图片描述](https://img-blog.csdnimg.cn/938dfb7e78ee40b89addf413f06d6728.png)

> 综上，偏向锁通过对比Mark Word解决加锁问题，避免执行CAS操作。而轻量级锁是通过用CAS操作和自旋来解决加锁问题，避免线程阻塞和唤醒而影响性能。重量级锁是将除了拥有锁的线程以外的线程都阻塞。



> **偏向锁、轻量级锁和重量级锁的区别？**

- 偏向锁的优点是加解锁不需要额外消耗，和执行非同步方法比仅存在纳秒级差距，缺点是如果存在锁竞争会带来额外锁撤销的消耗，适用只有一个线程访问同步代码块的场景。

- 轻量级锁的优点是竞争线程不阻塞，程序响应速度快，缺点是如果线程始终得不到锁会自旋消耗 CPU，适用追求响应时间、同步代码块执行快的场景。
- 重量级锁的优点是线程竞争不使用自旋不消耗CPU，缺点是线程会阻塞，响应时间慢，适应追求吞吐量、同步代码块执行慢的场景。



#### 4、公平锁VS非公平锁

**公平锁：**

![](https://img-blog.csdnimg.cn/1e2cfea7f3064acf88bfffa593235efc.png)

公平锁是指多个线程按照申请锁的顺序来获取锁，线程直接进入队列中排队，队列中的第一个线程才能获得锁。

- 公平锁的优点是等待锁的线程不会饿死。
- 缺点是整体吞吐效率相对非公平锁要低，等待队列中除第一个线程以外的所有线程都会阻塞，CPU唤醒阻塞线程的开销比非公平锁大。



**非公平锁：**

![](https://img-blog.csdnimg.cn/4824de30ea5749478fea1d58df928b72.png)

非公平锁是多个线程加锁时直接尝试获取锁，获取不到才会到等待队列的队尾等待。但如果此时锁刚好可用，那么这个线程可以无需阻塞直接获取到锁，所以非公平锁有可能出现后申请锁的线程先获取锁的场景。



- 非公平锁的优点是可以减少唤起线程的开销，整体的吞吐效率高，因为线程有几率不阻塞直接获得锁，CPU不必唤醒所有线程。
- 缺点是处于等待队列中的线程可能会饿死，或者等很久才会获得锁。



**对比：**

接下来我们通过ReentrantLock的源码来讲解公平锁和非公平锁。

![](https://img-blog.csdnimg.cn/63cb59e89de94c88abc04724eee393cd.png)

> 根据代码可知，ReentrantLock里面有一个内部类Sync，Sync继承AQS（AbstractQueuedSynchronizer），添加锁和释放锁的大部分操作实际上都是在Sync中实现的。它有公平锁FairSync和非公平锁NonfairSync两个子类。ReentrantLock默认使用非公平锁，也可以通过构造器来显示的指定使用公平锁。



下面我们来看一下公平锁与非公平锁的加锁方法的源码:

![](https://img-blog.csdnimg.cn/f11519c0d7b249c68bbcd5ccae54798c.png)

> 通过上图中的源代码对比，我们可以明显的看出公平锁与非公平锁的lock()方法唯一的区别就在于公平锁在获取同步状态时多了一个限制条件：hasQueuedPredecessors()。



来看下 hasQueuedPredecessors() 方法：

![](https://img-blog.csdnimg.cn/4240d5fa3e03433ab86caa3b74e9aa5c.png)

> 再进入hasQueuedPredecessors()，可以看到该方法主要做一件事情：主要是判断当前线程是否位于同步队列中的第一个。如果是则返回true，否则返回false。



**总结：**

- `公平锁`：在并发环境中，每个线程在获取锁时会先查看此锁维护的等待队列，如果空，或者是队列首个就占有锁，否则就加入等待队列中，按照FIFO的规则获取锁；
- `非公平锁`：先抢先得，否则就排队等待。 优点吞吐量大。synchronized也是非公平的。



#### 5、可重入锁VS不可重入锁

**可重入锁：**

![](https://img-blog.csdnimg.cn/420d124dabaa432b9a0804f62f5c67ff.png)

可重入锁又名递归锁，是指在同一个线程在外层方法获取锁的时候，再进入该线程的内层方法会自动获取锁（前提锁对象得是同一个对象或者class），不会因为之前已经获取过还没释放而阻塞。

- 可重入锁又称之为递归锁，是指同一个线程在外层方法获取了锁，在进入内层方法会自动获取锁。



Java中ReentrantLock和synchronized都是可重入锁，可重入锁的一个优点是可一定程度避免死锁。下面用示例代码来进行分析：

```java
public class Widget {
    public synchronized void doSomething() {
        System.out.println("方法1执行...");
        doOthers();
    }

    public synchronized void doOthers() {
        System.out.println("方法2执行...");
    }
}
```

> 在上面的代码中，类中的两个方法都是被内置锁synchronized修饰的，doSomething()方法中调用doOthers()方法。因为内置锁是可重入的，所以同一个线程在调用doOthers()时可以直接获得当前对象的锁，进入doOthers()进行操作。
>
> 如果是一个不可重入锁，那么当前线程在调用doOthers()之前需要将执行doSomething()时获取当前对象的锁释放掉，实际上该对象锁已被当前线程所持有，且无法释放。所以此时会出现死锁。



**而为什么可重入锁就可以在嵌套调用时可以自动获得锁呢？**

我们通过图示和源码来分别解析一下。

还是打水的例子，有多个人在排队打水，此时管理员允许锁和同一个人的多个水桶绑定。这个人用多个水桶打水时，第一个水桶和锁绑定并打完水之后，第二个水桶也可以直接和锁绑定并开始打水，所有的水桶都打完水之后打水人才会将锁还给管理员。这个人的所有打水流程都能够成功执行，后续等待的人也能够打到水。这就是可重入锁。
![](https://img-blog.csdnimg.cn/93476ccbe8fa4e619dc4b2eff092d1f1.png)



**不可重入锁：**

但如果是非可重入锁的话，此时管理员只允许锁和同一个人的一个水桶绑定。第一个水桶和锁绑定打完水之后并不会释放锁，导致第二个水桶不能和锁绑定也无法打水。当前线程出现死锁，整个等待队列中的所有线程都无法被唤醒。

![](https://img-blog.csdnimg.cn/8da44236c0064e14bc5d35412fe6d421.png)



**对比：**

之前我们说过ReentrantLock和synchronized都是可重入锁，那么我们通过可重入锁ReentrantLock以及不可重入锁NonReentrantLock的源码来对比分析一下为什么非可重入锁在重复调用同步资源时会出现死锁。

首先ReentrantLock和NonReentrantLock都继承父类AQS，其父类AQS中维护了一个同步状态status来计数重入次数，status初始值为0。

当线程尝试获取锁时，可重入锁先尝试获取并更新status值，

- 如果status == 0表示没有其他线程在执行同步代码，则把status置为1，当前线程开始执行。
- 如果status != 0，则判断当前线程是否是获取到这个锁的线程，如果是的话执行status+1，且当前线程可以再次获取锁。



而不可重入锁是直接去获取并尝试更新当前status的值，如果status != 0的话会导致其获取锁失败，当前线程阻塞。

释放锁时，可重入锁同样先获取当前status的值，在当前线程是持有锁的线程的前提下。如果status-1 == 0，则表示当前线程所有重复获取锁的操作都已经执行完毕，然后该线程才会真正释放锁。而不可重入锁则是在确定当前线程是持有锁的线程之后，直接将status置为0，将锁释放。

![](https://img-blog.csdnimg.cn/7caadaf7c2ff447eac2e2dd6a98a33fc.png)



#### 6、独享锁VS共享锁（写锁VS读锁）

**独享锁 VS 共享锁：**

https://juejin.cn/post/6844903715099377671



**独享锁：**

独享锁又称为写锁，是指锁一次只能被一个线程所持有。如果一个线程对数据加上排他锁后，那么其他线程不能再对该数据加任何类型的锁。获得独占锁的线程即能读数据又能修改数据。

![](https://img-blog.csdnimg.cn/b07d6ce1a93345e2a76ada2e9ccb2d52.png)

> JDK中的`synchronized`和`java.util.concurrent(JUC)`包中`Lock`的实现类就是独占锁。



**共享锁：**

共享锁又称为读锁，是指锁可被多个线程所持有。如果一个线程对数据加上共享锁后，那么其他线程只能对数据再加共享锁，不能加独占锁。获得共享锁的线程只能读数据，不能修改数据。

![](https://img-blog.csdnimg.cn/0973f22fdd594188b5f77f74098d9f6d.png)



参考：

https://blog.csdn.net/JMW1407/article/details/122312349
