##   JUC 学习总结之基础篇

[toc]

### 一、什么是JUC

JUC是java.util.concurrent包的简称，在Java5.0添加，目的就是为了更好的支持高并发任务。让开发者进行多线程编程时减少竞争条件和死锁的问题，如下图：

![](https://img-blog.csdnimg.cn/f6036b69bd154835abba57c63bb5aa8c.png)



### 二、进程和线程

#### 1、程序、进程、线程

**程序**

是一个静态的概念，一般对应于操作系统中的一个可执行的文件，比如：我们要启动酷狗听音乐，则对应酷狗可执

行程序。当我们双击酷狗，则加载程序到内存中，开始执行该程序，于是产生了"进程"。



**进程**

执行中的程序叫做进程(Process)，是一个动态的概念。现代的操作系统都可以同时启动多个进程。比如：我们在

用酷狗听音乐，也可以使用wps写文档，也可以同时用浏览器查看网页。可以通过任务管理器查看当前的进程。



**线程**

是进程的一个执行路径，一个进程中至少有一个线程，进程中的多个线程共享进程的资源。



>  java默认有两个线程:main(主)现场和GC(垃圾回收)线程。
> java是没有权限去开启线程、操作硬件的，这是一个native的一个本地方法，它调用的底层的C++代码。

**图解：**

<img src="https://img-blog.csdnimg.cn/31b0b3b4125d4778905c19db10e6c2f3.png" style="zoom:50%;" />





#### 2、并发、并行、串行

**并发： 多线程操作同一个资源。**

CPU 只有一核，模拟出来多条线程，天下武功，唯快不破。那么我们就可以使用CPU快速交替，来模拟多线程。



**并行： 多个人(CPU内核)一起行走(运行)。**

CPU多核，多个线程可以同时执行。 我们可以使用线程池！



**串行：只能是一条条线程的执行。**



> 并发编程的本质：**充分利用CPU的资源！**

**图解：**

![](https://img-blog.csdnimg.cn/490fe4e86ec64608b86c8e54bfb68ee3.png)



### 三、Lock（重点）

>  传统的 synchronized

```java
package com.linhuaming.juc.demo.thread;

/**
 * 三个售票员 卖出 30张票
 */
public class SaleTicketTest1 {

    public static void main(String[] args) throws Exception {
        Ticket ticket = new Ticket();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <=40; i++) {
                    ticket.saleTicket();
                }
            }
        }, "A").start();  // 线程A

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <=40; i++) {
                    ticket.saleTicket();
                }
            }
        }, "B").start();  // 线程B

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 1; i <=40; i++) {
                    ticket.saleTicket();
                }
            }
        }, "C").start();  // 线程C


    }

}

/**
 * 资源类
 */
class Ticket{
    private int number = 30;

    // synchronized 同步方法, 加锁
    public synchronized void saleTicket(){
        if (number>0){
            System.out.println(Thread.currentThread().getName() + "卖出第 " + (number--) + "票,还剩下:"+number);
        }
    }

}
```



> 使用 juc.locks 包下的类操作 Lock 锁 + Lambda 表达式

```java
package com.linhuaming.juc.demo.thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 三个售票员 卖出 30张票
 */
public class SaleTicketTest2 {

    public static void main(String[] args) {
        Ticket2 ticket = new Ticket2();
        new Thread(()->{for (int i = 1; i <=1000; i++) ticket.saleTicket();}, "A").start();
        new Thread(()->{for (int i = 1; i <=40; i++) ticket.saleTicket();}, "B").start();
        new Thread(()->{for (int i = 1; i <=40; i++) ticket.saleTicket();}, "C").start();
    }

}


/**
 * 资源类
 */
class Ticket2{
    // 特点： 可重入锁，非公平锁
    private Lock lock = new ReentrantLock();
    private int number = 1000;

    // lock 方法, 加锁
    public void saleTicket(){
        // 加锁
        lock.lock();
        try{
            if (number>0){
                System.out.println(Thread.currentThread().getName() + "卖出第 " + (number--) + "票,还剩下:"+number);
            }
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 解锁
            lock.unlock();
        }
    }

}
```



> 总结：synchronized 和 lock 区别

1. 首先synchronized是java内置关键字，在jvm层面，Lock是个java类；
2. synchronized无法判断是否获取锁的状态，Lock可以判断是否获取到锁；
3. synchronized会自动释放锁(a 线程执行完同步代码会释放锁 ；b 线程执行过程中发生异常会释放
锁)，Lock需在finally中手工释放锁（unlock()方法释放锁），否则容易造成线程死锁；
4. 用synchronized关键字的两个线程1和线程2，如果当前线程1获得锁，线程2等待。如果线程1
阻塞，线程2则会一直等待下去，而Lock锁就不一定会等待下去，如果尝试获取不到锁，线程可以
不用一直等待就结束了；
5. synchronized的锁可重入、不可中断、非公平，而Lock锁可重入、可判断、可公平（两者皆可）
6. Lock锁适合大量同步的代码的同步问题，synchronized锁适合代码少量的同步问题。



### 四、生产者和消费者

线程间的通信 , 线程之间要协调和调度

> 生产者和消费者 synchroinzed 版

```java
package com.linhuaming.juc.demo.thread;


/**
 * 题目：现在两个线程，可以操作初始值为0的一个变量
 * 实现一个线程对该变量 + 1，一个线程对该变量 -1
 * 实现交替10次
 *
 * 诀窍：
 * 1. 高内聚 低耦合的前提下，线程操作资源类
 * 2. 判断 、干活、通知
 */
public class A {

    public static void main(String[] args) {
        Data data = new Data();

        new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    data.increment();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"A").start();
 
        new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    data.decrement();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"B").start();

    }

}


/**
 * 资源类
 */
class Data {
    private int number = 0;

    // 生产 +1
    public synchronized void increment() throws InterruptedException {
        // 判断该不该这个线程做
        if (number != 0) {
            this.wait();
        }
        // 干活
        number++;
        // 通知
        System.out.println(Thread.currentThread().getName()+"线程生产+1，目前总数"+number);
        this.notifyAll();
    }

    // 消费 -1
    public synchronized void decrement() throws InterruptedException {
        // 判断该不该这个线程做
        if (number == 0){
            this.wait();
        }
        // 干活
        number--;
        // 通知
        System.out.println(Thread.currentThread().getName()+"线程消费-1，目前总数"+number);
        this.notifyAll();
    }
}
```



> 问题升级：防止虚假唤醒，4个线程，两个加，两个减

【重点】if 和 while

![image-20220621000604202](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220621000604202.png)

```java
package com.linhuaming.juc.demo.thread;

public class B {
    public static void main(String[] args) {
        Data2 data = new Data2();

        new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    data.increment();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"A").start();

        new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    data.decrement();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"B").start();

        new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    data.increment();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"C").start();

        new Thread(()->{
            try {
                for (int i = 0; i < 10; i++) {
                    data.decrement();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"D").start();

    }
}

/**
 * 资源类
 */
class Data2{
    private int number = 0;

    // 生产 +1
    public synchronized void increment() throws InterruptedException {
        // 判断该不该这个线程做
        while (number!=0){
            this.wait();
        }
        // 干活
        number++;
        // 通知
        System.out.println(Thread.currentThread().getName()+"线程生产+1，目前总数"+number);
        this.notifyAll();
    }

    // 消费 -1
    public synchronized void decrement() throws InterruptedException {
        // 判断该不该这个线程做
        while (number==0){
            this.wait();
        }
        // 干活
        number--;
        // 通知
        System.out.println(Thread.currentThread().getName()+"线程消费-1，目前总数"+number);
        this.notifyAll();
    }

}
```

运行结果：

![image-20220621001334099](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220621001334099.png)

> 可以看到，很明显的有问题了。**解决方案:将if改为while即可**



> JUC 版 生产者和消费者问题

![](https://img-blog.csdnimg.cn/77f62922b16b42afa9143d0a34eb9818.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAMTIz5bCP5q2l,size_20,color_FFFFFF,t_70,g_se,x_16)

![](https://img-blog.csdnimg.cn/f6995efd199644ca9c4920367cd1faf0.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAMTIz5bCP5q2l,size_20,color_FFFFFF,t_70,g_se,x_16)



```java
package com.linhuaming.juc.demo.thread.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 题目：现在四个线程，可以操作初始值为0的一个变量
 * 实现两个线程对该变量 + 1，两个线程对该变量 -1
 * 实现交替10次
 * <p>
 * 诀窍：
 * 1. 高内聚低耦合的前提下，线程操作资源类
 * 2. 判断 、干活、通知
 * 3. 多线程交互中，必须要防止多线程的虚假唤醒，也即（判断不能用if，只能用while）
 */
public class A {

    public static void main(String[] args) {
        Data data = new Data();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    data.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "C").start();

        new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    data.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "D").start();
    }

}

/**
 * 资源类
 */
class Data {
    private int number = 0;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    // +1 操作
    public void increment() throws InterruptedException {
        lock.lock();
        try {
            // 判断该不该这个线程做
            while (number != 0) {
                condition.await();
            }
            // 干活
            number++;
            System.out.println(Thread.currentThread().getName() + " " + number);
            // 通知
            condition.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    // -1 操作
    public void decrement() throws InterruptedException {
        lock.lock();
        try {
            // 判断该不该这个线程做
            while (number == 0) {
                condition.await();
            }
            // 干活
            number--;
            System.out.println(Thread.currentThread().getName() + " " + number);
            // 通知
            condition.signalAll();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

}
```



> 精确通知顺序访问，**任何一个新技术,绝不仅仅只是覆盖了原来的技术,一定有它的优势和补充了原来的技术！**

可以定义多个监视器进行精准通知如：

```java
package com.linhuaming.juc.demo.thread.juc;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 题目：多线程之间按顺序调用，实现 A->B->C->D
 * 四个线程启动，要求如下：
 */

public class B {
    public static void main(String[] args) {
        Data2 data01 = new Data2();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data01.printA();
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data01.printB();
            }
        }, "B").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data01.printC();
            }
        }, "C").start();


        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                data01.printD();
            }
        }, "D").start();
    }

}

class Data2 {
    // 可重入锁
    private final Lock lock = new ReentrantLock();

    // 监视器
    private final Condition condition1 = lock.newCondition();
    private final Condition condition2 = lock.newCondition();
    private final Condition condition3 = lock.newCondition();
    private final Condition condition4 = lock.newCondition();

    // 物品数量
    private int count = 0;

    //生产
    public void printA() {
        lock.lock();
        try {
            while (count != 0) {
                //等待
                condition1.await();
            }
            count++;
            System.out.println(Thread.currentThread().getName() + "生产物品数量+1物品还剩" + count + "个,通知B消费");
            //唤醒B
            condition2.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }

    }

    //消费
    public void printB() {
        lock.lock();
        try {
            while (count <= 0) {
                condition2.await();
            }
            count--;
            System.out.println(Thread.currentThread().getName() + "消费物品数量-1物品还剩" + count + "个，通知C生产");
            //通知C
            condition3.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    //生产
    public void printC() {
        try {
            lock.lock();
            while (count != 0) {
                //等待
                condition3.await();
            }
            count++;
            System.out.println(Thread.currentThread().getName() + "生产物品数量+1物品还剩" + count + "个,通知D消费");
            //唤醒D
            condition4.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    //消费
    public void printD() {
        lock.lock();
        try {
            while (count <= 0) {
                condition4.await();
            }
            count--;
            System.out.println(Thread.currentThread().getName() + "消费物品数量-1物品还剩" + count + "个，通知A生产");
            //通知A
            condition1.signal();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

}
```



### 五、八锁现象

**1、标准访问，请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

/**
 * 多线程的8锁
 * 1、标准访问，请问先打印邮件还是短信？
 *
 * 结论：被synchronized修饰的方法，锁的对象是方法的调用者。因为两个方法的调用者是同一个，
 * 所以两个方法用的是同一个锁，先调用方法的先执行
 */
public class Lock1 {

    public static void main(String[] args) throws InterruptedException {
        Phone phone = new Phone();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();

    }

}

class Phone{
    public synchronized void sendEmail() throws Exception{
        System.out.println("sendEmail");
    }
    public synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }
}
```

> 结论：被synchronized修饰的方法，锁的对象是方法的调用者。因为两个方法的调用者是同一个，所以两个方法用的是同一个锁，先调用方法的先执行。



**2、邮件方法暂停4秒钟，请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

import java.util.concurrent.TimeUnit;

/**
 * 多线程的8锁
 * 1、标准访问，请问先打印邮件还是短信？
 * 2、邮件方法暂停4秒钟，请问先打印邮件还是短信？
 *
 * 结论：被synchronized修饰的方法，锁的对象是方法的调用者。因为两个方法的调用者是同一个，所以
 * 两个方法用的是同一个锁，先调用方法的先执行，第二个方法只有在第一个方法执行完释放锁之后才能执行。
 */
public class Lock2 {

    public static void main(String[] args) throws InterruptedException {
        Phone2 phone = new Phone2();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();

    }

}

class Phone2{
    public synchronized void sendEmail() throws Exception{
        TimeUnit.SECONDS.sleep(4);
        System.out.println("sendEmail");
    }
    public synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }
}
```



**3、新增一个普通方法hello()没有同步,请问先打印邮件还是hello？**

```java
package com.linhuaming.juc.demo.thread.lock8;

import java.util.concurrent.TimeUnit;

/**
 * 多线程的8锁
 * 3、新增一个普通方法hello()没有同步,请问先打印邮件还是hello？
 *
 * 结论：新增的方法没有被synchronized修饰，不是同步方法，不受锁的影响，所以不需要等待。其他线
 * 程共用了一把锁，所以还需要等待。
 */
public class Lock3 {

    public static void main(String[] args) throws InterruptedException {
        Phone3 phone = new Phone3();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                // phone.sendSMS();
                phone.hello();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();
    }

}

class Phone3 {
    public synchronized void sendEmail() throws Exception{
        TimeUnit.SECONDS.sleep(4);
        System.out.println("sendEmail");
    }

    public synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }

    public void hello(){
        System.out.println("Hello");
    }

}
```

> 结论：新增的方法没有被synchronized修饰，不是同步方法，不受锁的影响，所以不需要等待。其他线 程共用了一把锁，所以还需要等待。



**4、两部手机、请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

import java.util.concurrent.TimeUnit;

/**
 * 多线程的8锁
 * 4、两部手机、请问先打印邮件还是短信？
 *
 * 结论：被synchronized修饰的方法，锁的对象是方法的调用者。因为用了两个对象调用各自的方法，
 * 所 以两个方法的调用者不是同一个，所以两个方法用的不是同一个锁，后调用的方法不需要等待先调用的方法。
 */
public class Lock4 {

    public static void main(String[] args) throws InterruptedException {
        Phone4 phone1 = new Phone4();
        Phone4 phone2 = new Phone4();

        new Thread(()->{
            try {
                phone1.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone2.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();
    }

}

class Phone4 {
    public synchronized void sendEmail() throws Exception{
        TimeUnit.SECONDS.sleep(4);
        System.out.println("sendEmail");
    }

    public synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }

}
```

> 结论：被synchronized修饰的方法，锁的对象是方法的调用者。因为用了两个对象调用各自的方法，所 以两个方法的调用者不是同一个，所以两个方法用的不是同一个锁，后调用的方法不需要等待先调用的方法。



**5、两个静态同步方法，同一部手机，请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

/**
 * 多线程的8锁
 * 5、两个静态同步方法，同一部手机，请问先打印邮件还是短信？
 *
 * 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。
 * 因为两个同步方法都被static修 饰了，所以两个方法用的是同一个锁，后调用的方法需要等待先调用的方法。
 */
public class Lock5 {

    public static void main(String[] args) throws InterruptedException {
        Phone5 phone = new Phone5();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();

    }

}

class Phone5 {
    public static synchronized void sendEmail() throws Exception{
        System.out.println("sendEmail");
    }
    public static synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }
}
```

> 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。因为两个同步方法都被static修 饰了，所以两个方法用的是同一个锁，后调用的方法需要等待先调用的方法。



**6、两个静态同步方法，同一部手机，请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

/**
 * 多线程的8锁
 * 6、两个静态同步方法，2部手机，请问先打印邮件还是短信？
 *
 * 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。因为两个同步方法都被static修
 * 饰了，即便用了两个不同的对象调用方法，两个方法用的还是同一个锁，后调用的方法需要等待先调用
 * 的方法。
 */
public class Lock6 {

    public static void main(String[] args) throws InterruptedException {
        Phone5 phone = new Phone5();
        Phone5 phone2 = new Phone5();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone2.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();

    }

}

class Phone6 {
    public static synchronized void sendEmail() throws Exception{
        System.out.println("sendEmail");
    }

    public static synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }
}
```

> 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。因为两个同步方法都被static修 饰了，即便用了两个不同的对象调用方法，两个方法用的还是同一个锁，后调用的方法需要等待先调用 的方法。



**7、一个普通同步方法，一个静态同步方法，同一部手机，请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

/**
 * 多线程的8锁
 * 7、一个普通同步方法，一个静态同步方法，同一部手机，请问先打印邮件还是短信？
 *
 * 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。仅仅被synchronized修饰的方法，
 * 锁的对象是方法的调用者。因为两个方法锁的对象不是同一个，所以两个方法用的不是同一个锁， 后调用的方法不需要等待先调用的方法。
 */
public class Lock7 {

    public static void main(String[] args) throws InterruptedException {
        Phone7 phone = new Phone7();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();
    }

}

class Phone7 {
    public static synchronized void sendEmail() throws Exception{
        System.out.println("sendEmail");
    }

    public synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }
}
```

> 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。仅仅被synchronized修饰的方 法，锁的对象是方法的调用者。因为两个方法锁的对象不是同一个，所以两个方法用的不是同一个锁， 后调用的方法不需要等待先调用的方法。



**8、一个普通同步方法，一个静态同步方法，2部手机，请问先打印邮件还是短信？**

```java
package com.linhuaming.juc.demo.thread.lock8;

/**
 * 多线程的8锁
 * 8、一个普通同步方法，一个静态同步方法，2部手机，请问先打印邮件还是短信？
 *
 * 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。仅仅被synchronized修饰的方
 * 法，锁的对象是方法的调用者。即便是用同一个对象调用两个方法，锁的对象也不是同一个，所以两个
 * 方法用的不是同一个锁，后调用的方法不需要等待先调用的方法。
 */
public class Lock8 {

    public static void main(String[] args) throws InterruptedException {
        Phone8 phone = new Phone8();
        Phone8 phone2 = new Phone8();

        new Thread(()->{
            try {
                phone.sendEmail();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"A").start();

        Thread.sleep(200);

        new Thread(()->{
            try {
                phone2.sendSMS();
            } catch (Exception e) {
                e.printStackTrace();
            }
        },"B").start();
    }
}

class Phone8 {
    public static synchronized void sendEmail() throws Exception{
        System.out.println("sendEmail");
    }

    public synchronized void sendSMS() throws Exception{
        System.out.println("sendSMS");
    }
}
```

> 结论：被synchronized和static修饰的方法，锁的对象是类的class对象。仅仅被synchronized修饰的方
> 法，锁的对象是方法的调用者。即便是用同一个对象调用两个方法，锁的对象也不是同一个，所以两个
> 方法用的不是同一个锁，后调用的方法不需要等待先调用的方法。



### 六、集合类线程不安全

#### 1、List

演示ArrayList 集合 ，并发时引发的问题

```java
package com.linhuaming.juc.demo.thread.unsafe;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 1 故障现象：ConcurrentModificationException
 * 2 导致原因：add 方法没有加锁
 * 3 解决方案：换一个集合类
 * List<String> list = new Vector<>(); JDK1.0 就存在了！
 * List<String> list = Collections.synchronizedList(new ArrayList<>());
 * List<String> list = new CopyOnWriteArrayList<>();
 */

public class UnSafeList {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        for (int i = 1; i <= 30; i++) {
            new Thread(()->{
                list.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(list);
            }).start();
        }
    }

}
```



执行上面结果后，并发修改异常，如下图所示：

![image-20220705215357578](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220705215357578.png)



**解决办法：**

> 1 故障现象：ConcurrentModificationException
>
> 2 导致原因：add 方法没有加锁
>
> 3 解决方案：换一个集合类
>
> ​	List<String> list = new Vector<>(); JDK1.0 就存在了！
>
> ​	List<String> list = Collections.synchronizedList(new ArrayList<>());
>
> ​	List<String> list = new CopyOnWriteArrayList<>();



**写入时复制（CopyOnWrite）思想**

>  写入时复制（CopyOnWrite，简称COW）思想是计算机程序设计领域中的一种优化策略。其核心思想 是，如果有多个调用者（Callers）同时要求相同的资源（如内存或者是磁盘上的数据存储），他们会共 同获取相同的指针指向相同的资源，直到某个调用者视图修改资源内容时，系统才会真正复制一份专用 副本（private copy）给该调用者，而其他调用者所见到的最初的资源仍然保持不变。这过程对其他的 调用者都是透明的（transparently）。此做法主要的优点是如果调用者没有修改资源，就不会有副本 （private copy）被创建，因此多个调用者只是读取操作时可以共享同一份资源。 
>
> 读写分离，写时复制出一个新的数组，完成插入、修改或者移除操作后将新数组赋值给array 

**CopyOnWriteArrayList为什么并发安全且性能比Vector好** 

>  我知道Vector是增删改查方法都加了synchronized，保证同步，但是每个方法执行的时候都要去获得 锁，性能就会大大下降，而CopyOnWriteArrayList 只是在增删改上加锁，但是读不加锁，在读方面的性 能就好于Vector，CopyOnWriteArrayList支持读多写少的并发情况。



#### 2、Set

演示HashSet集合 ，并发时引发的问题

```java
package com.linhuaming.juc.demo.thread.unsafe;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * 1 故障现象：ConcurrentModificationException
 * 2 导致原因：add 方法没有加锁
 * 3 解决方案：换一个集合类
 * Set<String> set = Collections.synchronizedSet(new HashSet<>());
 * Set<String> set = new CopyOnWriteArraySet();
 * 优化建议：（同样的错误，不出现第2次）
 *
 */
public class UnSafeSet {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        for (int i = 1; i <= 30; i++) {
            new Thread(()->{
                set.add(UUID.randomUUID().toString().substring(0,8));
                System.out.println(set);
            }).start();
        }
    }

}
```



执行上面结果后，并发修改异常，如下图所示：

![image-20220705221227302](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220705221227302.png)



**解决办法：**

> 1 故障现象：ConcurrentModificationException
>
> 2 导致原因：add 方法没有加锁
>
> 3 解决方案：换一个集合类
>
> Set<String> set = Collections.synchronizedSet(new HashSet<>());
>
> Set<String> set = new CopyOnWriteArraySet();



**HashSet底层就是HashMap**

```java
Set<String> set = new HashSet<>();
// 点进去
public HashSet() {
map = new HashMap<>();
}
// add方法 就是map的put方法
public boolean add(E e) {
return map.put(e, PRESENT)==null;
}
//private static final Object PRESENT = new Object(); 不变得值
```



#### 3、Map

演示HashMap集合 ，并发时引发的问题

```java
package com.linhuaming.juc.demo.thread.unsafe;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 1 故障现象：ConcurrentModificationException
 * 2 导致原因：put 方法没有加锁
 * 3 解决方案：换一个集合类
 * Set<String> set = Collections.Collections.synchronizedMap(new HashMap<>());
 * Set<String> set = new ConcurrentHashMap<>();
 *
 */
public class UnSafeMap {

    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
        for (int i = 1; i < 30; i++) {
            new Thread(()->{
                map.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,8));
                System.out.println(map);
            },String.valueOf(i)).start();
        }
    }

}
```



执行上面结果后，并发修改异常，如下图所示：

![image-20220705222941424](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220705222941424.png)



**解决办法：**

> 1 故障现象：ConcurrentModificationException
>
> 2 导致原因：put 方法没有加锁
>
> 3 解决方案：换一个集合类
>
> Set<String> set = Collections.Collections.synchronizedMap(new HashMap<>());
>
> Set<String> set = new ConcurrentHashMap<>();



### 七、Callable

多线程中，第3种获得多线程的方式，Callable。它与Runnable有什么区别呢？

- 是否有返回值 

- 是否抛异常 

- 方法不一样，一个是call，一个是run



> 使用示例

```java
package com.linhuaming.juc.demo.thread.callable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class CallableDemo {

    public static void main(String[] args) throws ExecutionException,InterruptedException {
        MyThread myThread = new MyThread();
        FutureTask futureTask = new FutureTask(myThread); // 适配类
        Thread t1 = new Thread(futureTask,"A"); // 调用执行
        t1.start();
        Integer result = (Integer) futureTask.get(); // 获取返回值
        System.out.println(Thread.currentThread().getName()+" "+result);
    }

}

class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName()+" "+"call 被调用");
        return 1024;
    }

}
```



**Callable与Runnable 关系：**

![image-20220705224647947](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220705224647947.png)



### 八、常用辅助类

#### 1、CountDownLatch

```java
package com.linhuaming.juc.demo.thread.utils;

import java.util.concurrent.CountDownLatch;

/**
 * 减操作
 */
public class CountDownLatchDemo {

    public static void main(String[] args) throws InterruptedException {
        // 计数器
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+" Start");
                countDownLatch.countDown(); // 计数器-1
            },String.valueOf(i)).start();
        }

        // 阻塞等待计数器归零
        countDownLatch.await();
        System.out.println(Thread.currentThread().getName()+" End");
    }

}
```

> CountDownLatch 主要有两个方法，当一个或多个线程调用 await 方法时，这些线程会阻塞 其他线程调用CountDown方法会将计数器减1（调用CountDown方法的线程不会阻塞） 当计数器变为0时，await 方法阻塞的线程会被唤醒，继续执行



#### 2、CyclicBarrier

作用：和上面的减法相反，这里是加法，举例：好比集齐7个龙珠召唤神龙，或者人到齐了再开会！

```java
package com.linhuaming.juc.demo.thread.utils;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * 加操作
 * 这里是加法，好比集齐7个龙珠召唤神龙，或者人到齐了再开会！
 */
public class CyclicBarrierDemo {

    public static void main(String[] args) {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7,()->{
            System.out.println("召唤神龙成功");
        });

        for (int i = 1; i <= 7; i++) {
            final int tempInt = i;
            new Thread(()->{
                System.out.println(Thread.currentThread().getName()+"收集了第"+ tempInt +"颗龙珠");
                try {
                    cyclicBarrier.await(); // 等待
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

}
```



#### 3、Semaphore

翻译：Semaphore 信号量;信号灯;信号，作用举例：抢车位

```java
package com.linhuaming.juc.demo.thread.utils;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * 翻译：Semaphore 信号量;信号灯;信号
 * 作用举例：抢车位
 */
public class SemaphoreDemo {

    public static void main(String[] args) {
        // 模拟资源类，有3个空车位
        Semaphore semaphore = new Semaphore(3);

        // 模拟6个车
        for (int i = 1; i <= 6; i++) {
            new Thread(()->{
                try {
                    semaphore.acquire(); // acquire 得到
                    System.out.println(Thread.currentThread().getName()+" 抢到了车位");
                    TimeUnit.SECONDS.sleep(3); // 停3秒钟
                    System.out.println(Thread.currentThread().getName()+" 离开了车位");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    semaphore.release(); // 释放这个位置
                }
            },String.valueOf(i)).start();
        }
    }

}
```

>  原理： 
>
> 在信号量上我们定义两种操作： 
>
> ​	acquire（获取） 当一个线程调用 acquire 操作时，他要么通过成功获取信号量（信号量-1） 要么一直等下去，直到有线程释放信号量，或超时 
>
> ​	release （释放） 实际上会将信号量的值 + 1，然后唤醒等待的线程。 
>
> 信号量主要用于两个目的：一个是用于多个共享资源的互斥使用，另一个用于并发线程数的控制。



### 九、读写锁

独占锁（写锁）：指该锁一次只能被一个线程锁持有。对于ReentranrLock和 Synchronized 而言都是独 占锁。

共享锁（读锁）：该锁可被多个线程所持有。 对于ReentrantReadWriteLock其读锁时共享锁，写锁是独占锁，读

锁的共享锁可保证并发读是非常高效 的。

```java
package com.linhuaming.juc.demo.thread.lock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 多个线程同时读一个资源类没有任何问题，所以为了满足并发量，读取共享资源应该可以同时进行。
 * 但是，如果有一个线程想去写共享资源，就不应该再有其他线程可以对该资源进行读或写。
 * 1. 读-读 可以共存
 * 2. 读-写 不能共存
 * 3. 写-写 不能共存
 */
public class ReadWriteLockDemo {

    public static void main(String[] args) {
        MyCacheLock myCache = new MyCacheLock();
        // 写
        for (int i = 1; i <= 5; i++) {
            final int tempInt = i;
            new Thread(()->{
                myCache.put(tempInt+"",tempInt+"");
            },String.valueOf(i)).start();
        }

        //        for (int i = 1; i <= 5; i++) {
        //            final int tempInt = i;
        //            myCache.put(tempInt+"",tempInt+"");
        //        }

        // 读
        for (int i = 1; i <= 5; i++) {
            final int tempInt = i;
            new Thread(()->{
                myCache.get(tempInt+"");
            },String.valueOf(i)).start();
        }
    }

}

// 测试发现问题: 写入的时候，还没写入完成，会存在其他的写入！造成问题
class MyCache{
    private volatile Map<String,Object> map = new HashMap<>();
    public void put(String key,Object value){
        System.out.println(Thread.currentThread().getName()+" 写入"+key);
        map.put(key,value);
        System.out.println(Thread.currentThread().getName()+" 写入成功!");
    }
    public void get(String key){
        System.out.println(Thread.currentThread().getName()+" 读取"+key);
        Object result = map.get(key);
        System.out.println(Thread.currentThread().getName()+" 读取结果："+result);
    }
}

// 加锁
class MyCacheLock {
    private volatile Map<String, Object> map = new HashMap<>();
    private ReadWriteLock readWriteLock = new ReentrantReadWriteLock(); // 读写锁

    public void put(String key, Object value) {
        // 写锁
        readWriteLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + " 写入" + key);
            map.put(key, value);
            System.out.println(Thread.currentThread().getName() + " 写入成功!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void get(String key) {
        // 读锁
         readWriteLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName()+" 读取"+key);
            Object result = map.get(key);
            System.out.println(Thread.currentThread().getName()+" 读取结果："+result);
        }catch (Exception e){
            e.printStackTrace();
        }finally{
             readWriteLock.readLock().unlock();
        }
    }

}
```



### 十、阻塞队列

#### 1、阻塞队列

阻塞队列是一个队列，在数据结构中起的作用如下图：

![](https://img-blog.csdnimg.cn/c4434898fa6f49ea858040d4c9251151.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBAMTIz5bCP5q2l,size_20,color_FFFFFF,t_70,g_se,x_16)



**当队列是空的，从队列中获取元素的操作将会被阻塞：**

试图从空的队列中获取元素的线程将会被阻塞，直到其他线程往空的队列插入新的元素。



**当队列是满的，从队列中添加元素的操作将会被阻塞：**

试图向已满的队列中添加新元素的线程将会被阻塞，直到其他线程从队列中移除一个或多个元素或者完 全清空，使队列变得空闲起来并后续新增。



**阻塞队列的用处：**

在多线程领域：所谓阻塞，在某些情况下会挂起线程（即阻塞），一旦条件满足，被挂起的线程又会自 动被唤起。



**为什么需要 BlockingQueue？**

好处是我们不需要关心什么时候需要阻塞线程，什么时候需要唤醒线程，因为这一切BlockingQueue 都 给你一手包办了。

在 concurrent 包发布以前，在多线程环境下，我们每个程序员都必须自己去控制这些细节，尤其还要 兼顾效率和线程安全，而这会给我们的程序带来不小的复杂度。



**接口架构图：**

![](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220708235417579.png)

- ArrayBlockingQueue：由数组结构组成的有界阻塞队列。

- LinkedBlockingQueue：由链表结构组成的有界（默认值为：integer.MAX_VALUE）阻塞队列。

- PriorityBlockingQueue：支持优先级排序的无界阻塞队列。

- DelayQueue：使用优先级队列实现的延迟无界阻塞队列。

- SynchronousQueue：不存储元素的阻塞队列，也即单个元素的队列。

- LinkedTransferQueue：由链表组成的无界阻塞队列。

- LinkedBlockingDeque：由链表组成的双向阻塞队列。



#### 2、四组API

| **方式**         | **抛出异常** | **有返回值,不抛出异常** | **阻塞 等待** | **超时等待**                 |
| ---------------- | ------------ | ----------------------- | ------------- | ---------------------------- |
| **添加**         | **add**      | **offer**               | **put**       | **offer(等待时间,等待单位)** |
| **移除**         | **remove**   | **poll**                | **take**      | **poll(等待时间,等待单位)**  |
| **检测队首元素** | **element**  | **peek**                | -             | -                            |

**说明：**

![image-20220709001451753](C:/Users/linhuaming/AppData/Roaming/Typora/typora-user-images/image-20220709001451753.png)



**代码测试（抛出异常）：**

当阻塞队列满时

```java
package com.linhuaming.juc.demo.thread.block_queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 阻塞队列,抛出异常
 */
public class BlockingQueueExceptionDemo1 {

    public static void main(String[] args) {
        // 初始化队列大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
        System.out.println(blockingQueue.add("d")); // java.lang.IllegalStateException: Queue full

    }

}
```



当阻塞队列空时

```java
package com.linhuaming.juc.demo.thread.block_queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 阻塞队列,抛出异常
 */
public class BlockingQueueExceptionDemo2 {

    public static void main(String[] args) {
        // 队列大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));

        System.out.println(blockingQueue.element()); // 检测队列队首元素！

        // public E remove() 返回值E，就是移除的值
        System.out.println(blockingQueue.remove()); //a
        System.out.println(blockingQueue.remove()); //b
        System.out.println(blockingQueue.remove()); //c
        System.out.println(blockingQueue.remove()); // java.util.NoSuchElementException
    }

}
```



**代码测试（返回特殊值）：**

```java
package com.linhuaming.juc.demo.thread.block_queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 阻塞队列,有返回值,不抛出异常
 */
public class BlockingQueueReturnDemo1 {

    public static void main(String[] args) {
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        System.out.println(blockingQueue.offer("a")); // true
        System.out.println(blockingQueue.offer("b")); // true
        System.out.println(blockingQueue.offer("c")); // true
        System.out.println(blockingQueue.offer("d")); // false

        System.out.println("检测队列队首元素！ "+blockingQueue.peek()); // 检测队列队首元素！

        // public E poll()
        System.out.println(blockingQueue.poll()); // a
        System.out.println(blockingQueue.poll()); // b
        System.out.println(blockingQueue.poll()); // c
        System.out.println(blockingQueue.poll()); // null

    }
}
```



**代码测试（一直阻塞）：**

```java
package com.linhuaming.juc.demo.thread.block_queue;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 阻塞队列,一直阻塞
 */
public class BlockingQueueAwaitDemo {

    public static void main(String[] args) throws InterruptedException {
        // 队列大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        blockingQueue.put("a");
        blockingQueue.put("b");
        blockingQueue.put("c");
        // blockingQueue.put("d");  // 一直阻塞

        System.out.println(blockingQueue.take()); // a
        System.out.println(blockingQueue.take()); // b
        System.out.println(blockingQueue.take()); // c
        System.out.println(blockingQueue.take()); // 阻塞不停止等待
    }

}
```



**代码测试（超时退出）：**

```java
package com.linhuaming.juc.demo.thread.block_queue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 阻塞队列,超时退出
 */
public class BlockingQueueTimeoutDemo {

    public static void main(String[] args) throws InterruptedException {
        // 队列大小
        ArrayBlockingQueue blockingQueue = new ArrayBlockingQueue<>(3);

        blockingQueue.offer("a");
        blockingQueue.offer("b");
        blockingQueue.offer("c");
        //        blockingQueue.offer("d",3L, TimeUnit.SECONDS); // 一直阻塞,等待3秒超时退出

        System.out.println(blockingQueue.poll()); // a
        System.out.println(blockingQueue.poll()); // b
        System.out.println(blockingQueue.poll()); // c
        System.out.println(blockingQueue.poll(3L,TimeUnit.SECONDS)); // 一直阻塞,等待3秒超时退出
    }

}
```



#### 3、同步队列

SynchronousQueue 没有容量。 

与其他的 BlockingQueue 不同，SynchronousQueue是一个不存储元素的 BlockingQueue 。

 每一个put操作必须要等待一个take操作，否则不能继续添加元素，反之亦然。

```java
package com.linhuaming.juc.demo.thread.block_queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * 同步队列
 */
public class SynchronousQueueDemo {

    public static void main(String[] args) {
        BlockingQueue<String> blockingQueue = new SynchronousQueue<>();
        new Thread(()->{
            try {
                System.out.println(Thread.currentThread().getName()+" put 1");
                blockingQueue.put("1");
                System.out.println(Thread.currentThread().getName()+" put 2");
                blockingQueue.put("2");
                System.out.println(Thread.currentThread().getName()+" put 3");
                blockingQueue.put("3");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"T1").start();

        new Thread(()->{
            try {
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName()+blockingQueue.take());
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName()+blockingQueue.take());
                TimeUnit.SECONDS.sleep(3);
                System.out.println(Thread.currentThread().getName()+blockingQueue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        },"T1").start();
    }

}
```



